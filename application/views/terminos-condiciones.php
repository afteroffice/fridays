<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="es"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="es"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="es"> <!--<![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>T.G.I. Fridays&reg; Per&uacute; - restaurante comida casual Lima Arequipa Trujillo</title> 
				
	<!-- Styles -->
	<style>
	@font-face{
		font-family:csm-font;
		src: url('<?=base_url() ?>static/fonts/dinneuzeitgrotesk-boldcond-regular-webfont.ttf'),
		url('<?=base_url() ?>static/fonts/dinneuzeitgrotesk-boldcond-regular-webfont.eot'); /* IE9 ?#iefix */
	}
	html {
		font:"Helvetica Neue",Helvetica,Arial,sans-serif;
		font-family:csm-font;
		-webkit-text-size-adjust: 100%;
		-ms-text-size-adjust: 100%;
	}
	
	</style>
</head>
<body>

<h1>Política de privacidad</h1>
<p>Fridays valora y respeta tu privacidad, por eso te garantizamos que la información que nos proporciones será estrictamente confidencial. Es nuestra política no vender, alquilar, distribuir o proporcionar tu información personal a terceros. La información que proporciones será utilizada para conocer tus preferencias, servirte mejor y hacerte llegar novedades sobre nuestro menú, promociones especiales y servicios.</p>
<h1>Política de reservas</h1>
<p><b>Tolerancia</b></p>
<p>Las reservas realizadas tienen una tolerancia de 15 minutos. Después de los 15 minutos el local se reserva el derecho de disponer del espacio reservado.</p>
<p><b>Número de personas</b></p>
<p>Las reservas realizadas requieren un mínimo del 50% de la mesa para que el número de sitios sea garantizado después de la hora pactada. El local se reserva el derecho de uso de los sitios no utilizados 15 minutos después de la hora de tolerancia.</p>
<p>Los huéspedes podrán realizar modificaciones en sus reservas a través de la central de reservas, siempre que la disponibilidad de espacios y los tiempos de reserva del restaurante seleccionado en el sistema lo permitan. Todo cambio estará sujeto a la disponibilidad de espacios del restaurante.</p>
<p><b>Políticas del restaurante</b></p>
<p>Nuestros restaurantes se dedican a la venta de bebidas y comidas, por lo que está prohibido el ingreso con alimentos. Tenemos definida una excepción cuando los huéspedes quieren llevar una torta de cumpleaños. No tenemos políticas de corcho libre o cobro por descorche.</p>

</body>
</html>