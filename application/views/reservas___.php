<section>
	<div class="container bg-reservas clearfix">
		<div class="reservas-online">
			<form id="freservas" method="post">
				<div id="msg-error" class="fila" style="display: none;">
					Por favor llenar los campos obligatorios marcado en azul.
				</div>
				<div class="fila">
					<label>Nombre y Apellidos</label>
					<input type="text" name="r-nombre" id="r-nombre" placeholder="" data-validation="required" class="onlyLetter" data-validation-error-msg="Este campo es obligatorio">
				</div>
				<div class="fila tel">
					<label>Teléfono</label>
					<input type="tel" name="r-telefono" id="r-telefono" placeholder="" data-validation="required" class="onlyNumber" maxlength="9" data-validation-error-msg="Debe ingresar un numero valido">
				</div>
				<div class="fila correo">
					<label>E-mail</label>
					<input type="email" name="r-correo" id="r-correo" placeholder="" data-validation="required email" data-validation-error-msg="Debe ingresar un email valido">
				</div>
				<div class="fila local">
					<label>Local</label>
					<select id="r-local" name="r-local">
						<!--<option value="Asia">Asia</option>-->
						<option value="Jockey Plaza">Jockey Plaza</option>
						<option value="Larcomar">Larcomar</option>
						<option value="Óvalo Gutiérrez">Óvalo Gutiérrez</option>
						<option value="San Miguel">San Miguel</option>
						<option value="Salaverry">Salaverry</option>
						<option value="Primavera">Primavera</option>
					</select>
				</div>
				<div class="fila fecha">
					<label>Fecha</label>
					<input id="fecha-reserva" type="text" name="r-fecha" class="datepicker" style="width: 75%; float: left;" />
					<!--input type="text" name="r-dia" id="r-dia" placeholder="DD" class="fecha onlyNumber" maxlength="2" data-validation="number" data-validation-allowing="range[1;31]" data-validation-error-msg="Debe ingresar un dia valido">
					<span>/</span>
					<input type="text" name="r-mes" id="r-mes" placeholder="MM" class="fecha onlyNumber" maxlength="2" data-validation="number " data-validation-allowing="range[1;12]" data-validation-error-msg="Debe ingresar una fecha valida"-->
					<img src="<?=base_url() ?>assets/img/icono-calendario.png" alt="">
				</div>
				<div class="fila hora">
					<label>Hora</label>
					<select id="r-hora-h" name="r-hora-h" name="r-hora-h" style="width: 20%; float: left">
						<option rel="12" value="12">12</option>
					<?php for ($h=1;$h<12;$h++) : ?>
						<option rel="<?=12 + $h?>" value="<?=str_pad($h, 2, "0", STR_PAD_LEFT) ?>"><?=str_pad($h, 2, "0", STR_PAD_LEFT) ?></option>
					<?php endfor; ?>
					</select>
					<div style="float: left; padding: 6px 0px 0px 6px">:</div>
					<select id="r-hora-m" name="r-hora-m" name="r-hora-m" style="width: 20%; float: left; margin-left: 10px;">
					<?php for ($m=0;$m<4;$m++) : ?>
						<option value="<?=str_pad($m*15, 2, "0", STR_PAD_LEFT) ?>"><?=str_pad($m*15, 2, "0", STR_PAD_LEFT) ?></option>
					<?php endfor; ?>
					</select>
					<div style="float: left; padding: 10px 0px 0px 6px">PM</div>
				</div>
				<div class="fila personas">
					<label>Nº de personas</label>
					<input type="text" name="r-personas" id="r-personas" placeholder="" class="personas onlyNumber" data-validation="required" data-validation-error-msg="Este campo es obligatorio">
				</div>
				<div class="fila motivo">
					<label>Motivo de reserva</label>
					<input type="text" name="r-motivo" id="r-motivo" placeholder="">
				</div>
				<div class="fila recibir">
					<input type="checkbox" name="f-recibir" value="recibir" id="f-recibir" checked="checked">
					<label for="f-recibir" id="quiero-recibir">Quiero recibir promociones de Fridays</label>
				</div>
				<div class="fila tyc">
					<input type="checkbox" name="f-tyc" value="tyc" id="f-tyc" checked="checked">
					<label for="f-tyc" id="terminos-condiciones">
						Acepto los <a class="fancybox fancybox.iframe tyc" href="<?=base_url()?>reservas/terminoscondiciones">términos y condiciones</a>
					</label>
				</div>
				<input type="submit" name="r-enviar" id="r-enviar" value="Enviar">
			</form>
		</div>
	</div>
</section>
