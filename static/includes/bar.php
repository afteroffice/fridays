
<?php

  include_once("funciones-sesiones.php");
  include_once("../../configRD.php");



function cargaInicialArequipa(){

  CompruebaArequipa();
  
  cabeceras();
  
}

function cargaInicialLima(){

  CompruebaLima();
  
  cabeceras();
  
}

function cargaInicialTrujillo(){

  CompruebaTrujillo();
  
  cabeceras();
  
}



function cabeceras(){

	echo '<!DOCTYPE html>'."\n";
	echo '<!--[if IE 7 ]><html class="ie ie7" lang="es"> <![endif]-->'."\n";
	echo '<!--[if IE 8 ]><html class="ie ie8" lang="es"> <![endif]-->'."\n";
	echo '<!--[if (gte IE 9)|!(IE)]><!--><html lang="es"> <!--<![endif]-->'."\n";
	
	echo '<head>'."\n";
	echo '<meta charset="windows-1252">'."\n";
	$ciudadActual = $_SESSION['nCiudad'];
  //obtener el nombre de la p�gina
  $nombrePaginaActual = $_SERVER['SCRIPT_NAME'];
  
  if ( strpos($nombrePaginaActual, '/') !== FALSE ){
      $nombrePaginaActual = array_pop(explode('/', $nombrePaginaActual));
  }

  $filaCategoria = consultar_Categoria_Bar_Pagina_Ciudad($ciudadActual, $nombrePaginaActual);

  $row = mysql_fetch_array($filaCategoria );

  $title=$row["Title"];
  $description=$row["Description"];
  $keywords=$row["Keywords"];
  
    echo '<title>'.$title.'</title>'."\n";
	echo '<meta name="keywords" content="'.$description.'"/>'."\n";
	echo '<meta name="description" content="'.$keywords.'"/>'."\n";
  
	include_once("../../includes/etiquetas-comunes.php"); 

	if($ciudadActual=="Arequipa"){
	include_once("../../includes/etiquetas-arequipa.php"); 
	}
	if($ciudadActual=="Lima"){
	include_once("../../includes/etiquetas-lima.php"); 
	}
	if($ciudadActual=="Trujillo"){
	include_once("../../includes/etiquetas-trujillo.php"); 
	}
}

function cabeceras2(){

	echo '<!-- Styles -->'."\n";
	echo '<link rel="stylesheet" type="text/css" href="../../css/style.css">'."\n";
	
	include("../../includes/estilos-ie.php"); 
	include_once("../../includes/analytics.php"); 
	
	echo '</head>'."\n";
	echo '<body>'."\n";
	include("../../includes/header.php"); 
}


function PintaCuerpo(){

  $ciudadActual = $_SESSION['nCiudad'];
  
  //obtener el nombre de la p�gina
  $nombrePaginaActual = $_SERVER['SCRIPT_NAME'];
  
  if ( strpos($nombrePaginaActual, '/') !== FALSE ){
      $nombrePaginaActual = array_pop(explode('/', $nombrePaginaActual));
  }

  $filaCategoria = consultar_Categoria_Bar_Pagina_Ciudad($ciudadActual, $nombrePaginaActual);

  $row = mysql_fetch_array($filaCategoria );

  $idCategoria=$row["idCategoria"];
  $nombreCategoria=$row["nombreCategoria"];
  $titleCategoria=$row["titleCategoria"];
  $srcImgCategoria=$row["srcImgCategoria"];
  $H1Banner=$row["H1Banner"];
  $H1Banner=htmlspecialchars_decode($H1Banner, ENT_NOQUOTES);
  
  $H2Banner=$row["H2Banner"];
  $H2Banner=htmlspecialchars_decode($H2Banner, ENT_NOQUOTES);
  $paginaCategoria=$row["paginaCategoria"];
  $activo=$row["activo"];
  $orden=$row["orden"];
  
    //echo "<p>htmlentities($srcImgCategoria)</p>";
	echo '<section class="clearfix">'."\n";
	echo '<div class="container">'."\n";
	echo '<div class="menu-bar">'."\n";
	echo '<ul>'."\n";
	
	if($ciudadActual=="Arequipa"){
		    $ciudad="arequipa";
		}
		if($ciudadActual=="Lima"){
			$ciudad="lima";
		}
		if($ciudadActual=="Trujillo"){
			$ciudad="trujillo";
		}
		
        $filasCategorias = consulta_Categorias_Bar_Activas_Ciudad($ciudadActual);
        $filas = mysql_num_rows($filasCategorias);
        if($filas==0){  

        echo '<li><a href="#" >Sin Categorias.</a></li>'."\n"; 

        }else{
           while($rows = mysql_fetch_array($filasCategorias)){
              
			  $nombreCat=$rows["nombreCategoria"];
              $paginaCat=$rows["paginaCategoria"];
              $titleCat=$rows["titleCategoria"];
			  
              $extens = extension($paginaCat);
			  
             if($extens=="php"){
			  echo '<li><a href="/'.$ciudad.'/bar/'.$paginaCat.'" title="'.$titleCat.'" ';
				  if($paginaCat==$nombrePaginaActual){ echo 'class="activo" '; } 
			  echo '>'.$nombreCat.'</a></li>'."\n";
			  }
			   
            }
        }

		 echo '</ul>'."\n";
	     echo '<select onChange="location = this.options[this.selectedIndex].value;">'."\n";
	          $filasCategorias = consulta_Categorias_Bar_Activas_Ciudad($ciudadActual);
            $filas = mysql_num_rows($filasCategorias);
            if($filas==0){  
  
 		       echo '<option value="#" selected>SIN BEBIDAS...</option>'."\n";
      
            }else{
    
               echo '<option value="#" selected>MENU BAR...</option>'."\n";
  
               while($rows = mysql_fetch_array($filasCategorias)){
                  $nombreCat=$rows["nombreCategoria"];
                  $paginaCat=$rows["paginaCategoria"];
                  
				  $extens = extension($paginaCat);
			  
                   if($extens=="php"){
        		       echo '<option value="'.$paginaCat.'">- '.$nombreCat.'</option>'."\n";
					}

              }
          }

    	echo '</select>'."\n";
    	echo '</div>'."\n";
	    $tmp ='<div class="bar-tit" style="background:url('.$srcImgCategoria;
		$tmp.=');" >'."\n";
		echo $tmp;
	    //echo '<div class="bar-tit" >'."\n";
    	echo '<h1>'.$H1Banner.'</h1>'."\n";
       echo '<h2>'.$H2Banner.'</h2>'."\n";
       echo ' </div>'."\n";
	   echo '<div class="bebidas">'."\n";
	   
	    $filasDetalles = consulta_Items_Bar_Activos_Ciudad($idCategoria, $ciudadActual);
              $filasDetallessinImagen = consulta_Items_Bar_Activos_sinImagen_Ciudad($idCategoria, $ciudadActual);

              $filas = mysql_num_rows($filasDetalles);
              $filassinImagen = mysql_num_rows($filasDetallessinImagen);

              $totalfilas= $filas + $filassinImagen;
              if($totalfilas==0){

            	echo '<p>No hay bebidas para mostrar.</p>'."\n";

             }else{
                //if($filas>0){
                echo '<ul class="beb1 clearfix">'."\n";
 
				while ($row = mysql_fetch_array($filasDetalles)){
	
						$tmpImgsrc= $row["srcImg"];
						$tmpImgsrc210_210= $row["srcImg210_210"];
						$tmpImgalt= $row["altImg"];
						$tmpImgclass= $row["classImg"];
						$tmpImgclass210_210= $row["classImg210_210"];
						$tmpH2= $row["textoH2"];
						$tmpH2=htmlspecialchars_decode($tmpH2, ENT_NOQUOTES);
						$tmpP= $row["parrafo"];
						$tmpP=htmlspecialchars_decode($tmpP, ENT_NOQUOTES);
						$tmpMegusta=$row["iFrame_megusta"];
						

						echo '<li>'."\n"; 
						echo '<div class="me-gusta3">'."\n";
                        echo htmlspecialchars_decode($tmpMegusta)."\n";
                        echo '</div>'."\n";
						echo '<div class="img-comidas">'."\n";
						echo '<img src="'.$tmpImgsrc.'" alt="'.$tmpImgalt.'" class="'.$tmpImgclass.'">'."\n";
						echo '<img src="'.$tmpImgsrc210_210.'" alt="'.$tmpImgalt.'" class="'.$tmpImgclass210_210.'">'."\n";
						
						echo '</div>'."\n";
						echo '<div class="info-bebidas">'."\n";
						echo '<h2>'.$tmpH2.'</h2>'."\n";
						echo '<p>'.$tmpP.'</p>'."\n";
						echo '</div>'."\n";
						echo '</li>'."\n";
                  }
			   // echo '</ul>'."\n";
				//}
				//if($filassinImagen>0){
					//echo '<ul class="beb2 clearfix">'."\n";
				 	while ($row = mysql_fetch_array($filasDetallessinImagen)){
	
						$tmpH2= $row["textoH2"];
						$tmpH2=htmlspecialchars_decode($tmpH2, ENT_NOQUOTES);
						$tmpP= $row["parrafo"];
						$tmpP=htmlspecialchars_decode($tmpP, ENT_NOQUOTES);
						$tmpMegusta=$row["iFrame_megusta"];
	
						  echo '<li>'."\n";
						  echo '<div class="info-bebidas sin-img">'."\n";
						  echo '<h2>'.$tmpH2.'</h2>'."\n";
						  echo '<div class="me-gusta4">'."\n";
                          echo htmlspecialchars_decode($tmpMegusta)."\n";
                          echo '</div>'."\n";
						  echo '<p>'.$tmpP.'</p>'."\n";
						  echo '</div>'."\n";
						  echo '</li>'."\n";   
					}
				//}
                echo '</ul>'."\n";  
             }
	   
   echo '</div>'."\n"; 
   echo '</div>'."\n"; 
   echo '</section>'."\n"; 
   
   
include("../../includes/footer.php");

echo '</body>'."\n"; 
echo '</html>'."\n"; 
}



//funcion para obtener una categoria por el nombre de su pagian y su ciudad
function consultar_Categoria_Bar_Pagina_Ciudad($ciudadActual, $nombrePaginaActual){
    
     $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM barCategorias$ciudadActual WHERE paginaCategoria='$nombrePaginaActual'";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function consulta_Categorias_Bar_Activas_Ciudad($ciudadActual){
   
   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
	$vacio="";
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM barCategorias$ciudadActual WHERE activo=1 AND LENGTH(paginaCategoria) >=5";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function consulta_Items_Bar_Activos_Ciudad($idCategoria, $ciudadActual){

   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM itemBar$ciudadActual WHERE idCategoria='$idCategoria' AND activo='1' AND LENGTH(srcImg) >=5 ORDER BY orden ASC ";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}


function consulta_Items_Bar_Activos_sinImagen_Ciudad($idCategoria, $ciudadActual){

   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM itemBar$ciudadActual WHERE idCategoria='$idCategoria' AND activo='1' AND LENGTH(srcImg) < 5 ORDER BY orden ASC ";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function extension($str) {
		return end(explode(".", $str));
	}
?>