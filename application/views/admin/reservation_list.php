<div class="row">
	<p>
		<a href="<?=base_url() ?>admin/reservation/export" class="btn btn-lg btn-success"><i class="fa fa-file-excel-o"></i> Exportar</a>
	</p>
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header" data-original-title>
				<h2><i class="icon-user"></i><span class="break"></span>Reservaciones</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
						<tr>
							<th>Nombre</th>
							<th>E-mail</th>
							<th>Promociones</th>
							<th>Estado</th>
							<th>Acciones</th>
						</tr>
				  </thead>   
				  <tbody>
					<?php foreach ($contents as $c) : ?>
					<tr>
						<td><?=$c->name ?></td>
						<td><?=$c->email ?></td>
						<td><?=($c->promo_receive == 'recibir') ? 'si' : 'no' ?></td>
						<td><?=$arrayStatus[$c->status_reservation] ?></td>
						<td>
							<?php if ($c->status_reservation == 'pending') : ?>
							<a class="btn btn-success" href="<?=base_url() ?>admin/reservation/confirm/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Confirmar" >
								<i class="fa fa-check "></i>  
							</a>
							<a class="btn btn-success" href="<?=base_url() ?>admin/reservation/noconfirm/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="No Confirmar" >
								<i class="fa fa-minus "></i>  
							</a>
							<?php endif; ?>
							<?php if ($c->status_reservation != 'cancel') : ?>
							<a class="btn btn-success" href="<?=base_url() ?>admin/reservation/cancel/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Cancelar" >
								<i class="fa fa-power-off "></i>  
							</a>
							<?php endif; ?>
							<a class="btn btn-info" href="<?=base_url() ?>admin/reservation/edit/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Editar" >
								<i class="fa fa-edit "></i>  
							</a>
							<a class="btn btn-danger" href="<?=base_url() ?>admin/reservation/delete/<?=$c->id ?>" onclick="return confirm('Desea eliminar lal reserva?')" data-toggle="tooltip" data-placement="top" title="Eliminar" >
								<i class="fa fa-remove "></i> 
							</a>
						</td>
					</tr>
					<?php endforeach; ?>
				  </tbody>
			  </table>            
			</div>
		</div>
	</div>
</div>
