<footer>
    <div class="clearfix">
        <div class="pie">
        <div class="container fdo-pie">
          <nav>
            <div class="menu2">
              <ul>
                <li><a href="/conocenos.php" title="">conócenos</a></li>
                <li><a href="/contactanos.php" title="">contáctanos</a></li>
                <li><a href="/suscribete.php" title="">suscríbete</a></li>
              </ul>
            </div>
          </nav>
          <nav>
            <div class="menu3">
              <ul>
                <li><a href="/certificado-regalo.php" title="">certificado de regalo</a></li>
                <li><a href="/eventos.php" title="">eventos</a></li>
                <li><a href="/trabaja-nosotros.php" title="">trabaja con nosotros</a></li>
                <li><a href="/mapa-sitio.php" title="">mapa de sitio</a></li>
              </ul>
            </div>
          </nav>
          <div class="clear"></div>
        </div>
      </div>
    </div>
</footer>