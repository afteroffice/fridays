<section>
	<div class="container bg-transp clearfix">
		<div class="cont-suscribete">
			<div class="susc-msg">
				<b>¡Ya casi terminamos!<br/><br/>
				En breve te llegará un correo electrónico, debes hacer click en el mensaje para confirmar tu suscripción</b><br/><br/>
			</div>
			<div class="clear"></div>
			<p>Política de privacidad</p>
			<p>Friday's valora y respeta tu privacidad, por eso te garantizamos que la información que nos proporciones será estrictamente confidencial. Es nuestra política no vender, alquilar, distribuir o proporcionar tu información personal a terceros. La información que proporciones será utilizada para conocer tus preferencias, servirte mejor y hacerte llegar novedades sobre nuestro menú, promociones especiales y servicios.</p>
		</div>
	</div>
</section>
