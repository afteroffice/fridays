<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<title>Dashboard - Fridays</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/style.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/retina.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>
		<link href="<?=base_url() ?>assets/css/summernote.css" rel="stylesheet" type="text/css" />
		
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?=base_url() ?>assets/js/respond.min.js"></script>
		<![endif]-->
		<!--[if !IE]>-->
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<?=base_url() ?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
		<!--<![endif]-->
		
		<script src="<?=base_url() ?>assets/js/bootstrap.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.sparkline.min.js"></script>
		<script src="<?=base_url() ?>assets/js/fullcalendar.min.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?=base_url() ?>assets/js/excanvas.min.js"></script><![endif]-->
		<script src="<?=base_url() ?>assets/js/jquery.flot.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.flot.pie.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.flot.stack.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.flot.resize.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.flot.time.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.cleditor.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.autosize.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.placeholder.min.js"></script>
		<script src="<?=base_url() ?>assets/js/moment.min.js"></script>
		<script src="<?=base_url() ?>assets/js/daterangepicker.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="<?=base_url() ?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url() ?>assets/js/dataTables.bootstrap.min.js"></script>
		<script src="<?=base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>
		<script src="<?=base_url() ?>assets/js/custom.min.js"></script>
		<script src="<?=base_url() ?>assets/js/main.js?v=2"></script>
		<?php if (isset($javascript) && is_array($javascript)): ?>
		<?php foreach ($javascript as $j) : ?>
		<script src="<?=base_url() ?><?=$j ?>"></script>
		<?php endforeach; ?>
		<?php elseif(isset($javascript) && $javascript != "") : ?>
		<script src="<?=base_url() ?><?=$javascript ?>"></script>
		<?php endif; ?>
	</head>

	<body>
		<!-- start: Header -->
		<header class="navbar">
			<div class="container">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
				</button>
				<a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i></a>
				<a class="navbar-brand col-md-2 col-sm-1 col-xs-2" href="index.html"><span>Fridays</span></a>
				<div class="nav-no-collapse header-nav">
					<ul class="nav navbar-nav pull-right">
						<?php $user = $this->session->userdata('user') ?>
						<li class="dropdown">
							<a class="btn account dropdown-toggle" data-toggle="dropdown" href="index.html#">
								<div class="avatar"></div>
								<div class="user">
									<span class="hello">Bienvenido!</span>
									<span class="name"><?=$user->username ?></span>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>	
		</header>
		
		<div class="container">
			<div class="row">
				<div id="sidebar-left" class="col-lg-2 col-sm-1 ">
					<div class="sidebar-nav nav-collapse collapse navbar-collapse">
						<ul class="nav main-menu">
							<?php if ($user->username == 'admin') : ?>
							<li><a href="<?=base_url() ?>admin/dashboard"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm text"> Dashboard</span></a></li>
							<li><a href="<?=base_url() ?>admin/slider"><i class="fa fa-circle"></i><span class="hidden-sm text"> Slider</span></a></li>
							<li><a href="<?=base_url() ?>admin/city" class="dropmenu"><i class="fa fa-circle"></i><span class="hidden-sm text"> Ciudades</span></a></li>
							<?php endif; ?>
							<li><a href="<?=base_url() ?>admin/reservation" class="dropmenu"><i class="fa fa-circle"></i><span class="hidden-sm text"> Reservaciones</span></a></li>
							<li><a href="<?=base_url() ?>admin/login/logout" class="dropmenu"><i class="fa fa-power-off"></i><span class="hidden-sm text"> Salir</span></a></li>
						</ul>
					</div>
					<a href="#" id="main-menu-min" class="full visible-md visible-lg"><i class="fa fa-angle-double-left"></i></a>
				</div>
				<div id="content" class="col-lg-10 col-sm-11 ">
					 <?php $this->load->view($content_view); ?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<footer>
		</footer>
		<div class="modal fade" id="modalContent"  aria-labelledby="myModalLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="modalDeleteLabel">Modal title</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="button" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</body>
</html>
