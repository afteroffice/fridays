<?php
class Ciudad extends CI_Controller {
	
	public function index($slug)
	{
		$this->load->model('slider_model');
		$this->load->model('city_model');
		$city = $this->city_model->getCityBySlug($slug);
		$data = $this->slider_model->getAll(array('status' => 'active'));
		$views['data'] = $data;
		$views['city'] = $city;
		$views['c_food'] = $this->city_model->getCategoriesFoods($city->id);
		$views['c_bar'] = $this->city_model->getCategoriesBars($city->id);
		$views['content_view'] = 'ciudad';
		$views['section'] = 'ciudad';
		$this->load->view('template', $views);
	}
	
	public function comida($slugCity, $slug)
	{
		$this->load->model('foodcategory_model');
		$this->load->model('city_model');
		$city = $this->city_model->getCityBySlug($slugCity);
		$category = $this->foodcategory_model->getCategoryBySlug($slug, $city->id);
		$views['categories'] = $this->foodcategory_model->getAll(array('status' => 'active', 'city' => $city->id), true);
		$views['items'] = $this->foodcategory_model->getItemsByCategory($category->id);
		$views['category'] = $category;
		$views['city'] = $city;
		$views['c_food'] = $this->city_model->getCategoriesFoods($city->id);
		$views['c_bar'] = $this->city_model->getCategoriesBars($city->id);
		$views['content_view'] = 'comida';
		$views['section'] = 'comida';
		$this->load->view('template', $views);
	}
	
	public function bar($slugCity, $slug)
	{
		$this->load->model('barcategory_model');
		$this->load->model('city_model');
		$city = $this->city_model->getCityBySlug($slugCity);
		$category = $this->barcategory_model->getCategoryBySlug($slug, $city->id);
		$views['categories'] = $this->barcategory_model->getAll(array('status' => 'active', 'city' => $city->id), true);
		$views['items'] = $this->barcategory_model->getItemsByCategory($category->id);
		$views['category'] = $category;
		$views['city'] = $city;
		$views['c_food'] = $this->city_model->getCategoriesFoods($city->id);
		$views['c_bar'] = $this->city_model->getCategoriesBars($city->id);
		$views['content_view'] = 'bar';
		$views['section'] = 'bar';
		$this->load->view('template', $views);
	}
}
