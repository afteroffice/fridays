<?php
class Certificadoregalo extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'certificado-regalo';
		$views['section'] = 'certificado-regalo';
		$this->load->view('template', $views);
	}
}
