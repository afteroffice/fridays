<?php
class Promo_email_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "promo_email";
	}
	
	public function getDataByEmail($email)
	{
		$this->db->select();
		$this->db->where('email', $email);
		$query = $this->db->get($this->_table);
		$result = $query->result();
		if (isset($result[0])) {
			return $result[0];
		}
		return false;
	}
}
