<?php
class Promo extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('promo_model');
		$this->load->model('promoitem_model');
		$this->load->library('upload');
	}
	
	public function index($city)
	{
		$contents = $this->promo_model->getAll(array('status != ' => 'delete', 'city' => $city), true);
		$views['city'] = $city;
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/promo_list';
		$this->load->view('admin/template', $views);
	}
	
	public function add($city)
	{
		if ($this->input->post() && $this->_validate_submit()) {
			$idPromo = $this->promo_model->insert(
				array(
					'title' => $this->input->post('title'),
					'title_icon' => $this->input->post('title_icon'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'disclamer' => $this->input->post('disclamer'),
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords'),
					'city' => $city,
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->promo_model->update(
						array(
							'image' => $newName
						),
						$idPromo
					);
				}
			}
			if ($_FILES['image_icon']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_icon')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->promo_model->update(
						array(
							'image_icon' => $newName
						),
						$idPromo
					);
				}
			}
			redirect('/admin/promo/index/'.$city);
		}
		$views['city'] = $city;
		$views['content_view'] = 'admin/promo_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		$dataPromo = $this->promo_model->find($id);
		if ($this->input->post() && $this->_validate_submit()) {
			$this->promo_model->update(
				array(
					'title' => $this->input->post('title'),
					'title_icon' => $this->input->post('title_icon'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'disclamer' => $this->input->post('disclamer'),
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords')
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->promo_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			if ($_FILES['image_icon']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_icon')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->promo_model->update(
						array(
							'image_icon' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/promo/index/'.$dataPromo->city);
		}
		$views['city'] = $dataPromo->city;
		$views['content'] = $dataPromo;
		$views['content_view'] = 'admin/promo_add';
		$this->load->view('admin/template', $views);
	}
	
	public function enable($id)
	{
		$dataPromo = $this->promo_model->find($id);
		$this->promo_model->update(
			array(
				'status' => 'active',
			),
			$id
		);
		redirect('/admin/promo/index/'.$dataPromo->city);
	}
	
	public function disable($id)
	{
		$dataPromo = $this->promo_model->find($id);
		$this->promo_model->update(
			array(
				'status' => 'inactive',
			),
			$id
		);
		redirect('/admin/promo/index/'.$dataPromo->city);
	}
	
	public function delete($id)
	{
		$dataPromo = $this->promo_model->find($id);
		$this->promo_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/promo/index/'.$dataPromo->city);
	}
	
	public function duplicate($id)
	{
		$dataPromo = $this->promo_model->find($id);
		$idPromo = $this->promo_model->insert(
			array(
				'title' => $dataPromo->title,
				'title_icon' => $dataPromo->title_icon,
				'content' => $dataPromo->content,
				'slug' => $dataPromo->slug,
				'disclamer' => $dataPromo->disclamer,
				'city' => $dataPromo->city,
				'order' => '100',
				'date_register' => date('Y-m-d H:i:s')
			)
		);
		redirect('/admin/promo/index/'.$dataPromo->city);
	}
	
	private function _validate_submit()
	{
		$this->form_validation->set_rules('title_icon', 'Titulo','trim|required');
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		$this->form_validation->set_rules('slug', 'Slug','trim|required');
		return $this->form_validation->run();
	}
	
	public function order()
	{
		$ids = $this->input->post('id');
		foreach ($ids as $k => $id) {
			$this->promo_model->update(
				array('order' => $k),
				$id
			);
		}
	}
}
