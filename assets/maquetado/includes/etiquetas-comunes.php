<!-- Page Metas -->
<meta name="copyright" content="" /> 
<meta name="robots" content="index,follow,all" />

<meta name="DC.Language" scheme="RFC1766" content="Spanish"> 
<meta name="DC.language" scheme="ISO639-1" content="es" />

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<link href="/favicon.ico" rel="shortcut icon">
<link href="/images/icons/apple-touch-icon-iphone-57px.png" rel="apple-touch-icon" sizes="57x57">
<link href="/images/icons/apple-touch-icon-ipad-72px.png" rel="apple-touch-icon" sizes="72x72">
<link href="/images/icons/apple-touch-icon-iphoneretina-114px.png" rel="apple-touch-icon" sizes="114x114">
<link href="/images/icons/apple-touch-icon-ipadretina-144px.png" rel="apple-touch-icon" sizes="144x144">