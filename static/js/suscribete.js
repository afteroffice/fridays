$(document).ready(function() {
	$.extend(jQuery.validator.messages, {
		required: "Este campo es obligatorio.",
		remote: "Por favor, rellena este campo.",
		email: "Por favor, escribe una direcci&oacute;n de correo v&acute;lida",
		url: "Por favor, escribe una URL v&aacute;lida.",
		date: "Por favor, escribe una fecha v&aacute;lida.",
		dateISO: "Por favor, escribe una fecha (ISO) v&acute;lida.",
		number: "Por favor, escribe un n&uacute;mero entero v&aacute;lido.",
		digits: "Por favor, escribe s&oacute;lo d?gitos.",
		creditcard: "Por favor, escribe un n?mero de tarjeta v&aacute;lido.",
		equalTo: "Por favor, escribe el mismo valor de nuevo.",
		accept: "Por favor, escribe un valor con una extensi&oacute;n aceptada.",
		maxlength: jQuery.validator.format("Por favor, no escribas m&aacute;s de {0} caracteres."),
		minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
		rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
		range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
		max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
		min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
	});
	
	var options = {
		errorClass: 'mce_inline_error', 
		errorElement: 'div', 
		onkeyup: function(){}, 
		onfocusout:function(){}, 
		onblur:function(){}
	};
	var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
	$("#mc-embedded-subscribe-form").unbind('submit');
	options = {
		url: 'http://fridaysperu.us5.list-manage.com/subscribe/post-json?u=1afc9d75153efb95fafb40f3a&id=5add08e3ff&c=?', 
		type: 'GET', 
		dataType: 'json', 
		contentType: "application/json; charset=utf-8",
		beforeSubmit: function() {
			$('#FECNAC-fake-date').val($('#mce-FECNAC-month').val() + '/' + $('#mce-FECNAC-day').val() + '/' + $('#mce-FECNAC-year').val());
			$('#mce_tmp_error_msg').remove();
			$('.datefield','#mc_embed_signup').each(function() {
				var txt = 'filled';
				var fields = new Array();
				var i = 0;
				$(':text', this).each (function() {
					fields[i] = this;
					i++;
				});
				$(':hidden', this).each(function() {
					var bday = false;
					if (fields.length == 2) {
						bday = true;
						fields[2] = {'value':1970};//trick birthdays into having years
					}
					if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ) {
						this.value = '';
					} else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
						this.value = '';
					} else {
						if (/\[day\]/.test(fields[0].name)) {
							this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;
						} else {
							this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
						}
					}
				});
			});
			return mce_validator.form();
		}, 
		success: mce_success_cb
	};
	$('#mc-embedded-subscribe-form').ajaxForm(options);
});

function mce_success_cb(resp) {
	$('#mce-success-response').hide();
	$('#mce-error-response').hide();
	if (resp.result=="success") {
		$('#mce-'+resp.result+'-response').show();
		$('#mce-'+resp.result+'-response').html(resp.msg);
		$('#mc-embedded-subscribe-form').each(function(){
			this.reset();
		})	;
	} else {
		var index = -1;
		var msg;
		try {
			var parts = resp.msg.split(' - ',2);
			if (parts[1]==undefined){
				msg = resp.msg;
			} else {
				i = parseInt(parts[0]);
				if (i.toString() == parts[0]){
					index = parts[0];
					msg = parts[1];
				} else {
					index = -1;
					msg = resp.msg;
				}
			}
		} catch(e){
			index = -1;
			msg = resp.msg;
		}
		try {
			if (index== -1) {
				$('#mce-'+resp.result+'-response').show();
				$('#mce-'+resp.result+'-response').html(msg);            
			} else {
				err_id = 'mce_tmp_error_msg';
				html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
				var input_id = '#mc_embed_signup';
				var f = $(input_id);
				if (ftypes[index]=='address') {
					input_id = '#mce-'+fnames[index]+'-addr1';
					f = $(input_id).parent().parent().get(0);
				} else if (ftypes[index]=='date') {
					input_id = '#mce-'+fnames[index]+'-month';
					f = $(input_id).parent().parent().get(0);
				} else {
					input_id = '#mce-'+fnames[index];
					f = $().parent(input_id).get(0);
				}
				if (f) {
					$(f).append(html);
					$(input_id).focus();
				} else {
					$('#mce-'+resp.result+'-response').show();
					$('#mce-'+resp.result+'-response').html(msg);
				}
			}
		} catch(e){
			$('#mce-'+resp.result+'-response').show();
			$('#mce-'+resp.result+'-response').html(msg);
		}
	}
}
	
function annconf() {
	var mcedpto = document.getElementById('mce-DPTO').value;
	if (mcedpto == "Lima") {
		$(".mc-field-group-local").show(500);
		$('#mce-LOCAL').addClass('required');
	} else if (mcedpto == "Arequipa" || mcedpto == "Trujillo") {
		$(".mc-field-group-local").hide(500);
		$('#mce-LOCAL').removeClass('required');
		$("#mce-LOCAL").val("");
	}
}
