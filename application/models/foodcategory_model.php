<?php
class Foodcategory_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "food_category";
	}
	
	public function getCategoryBySlug($slug, $city)
	{
		$this->db->select();
		$this->db->where('slug', $slug);
		$this->db->where('city', $city);
		$this->db->where('status', 'active');
		$query = $this->db->get($this->_table);
		$result = $query->result();
		return $result[0];
	}
	
	public function getItemsByCategory($category)
	{
		$this->db->select();
		$this->db->where('category', $category);
		$this->db->where('status', 'active');
		$this->db->order_by("order", "asc");
		$query = $this->db->get('food_item');
		return $query->result();
	}
}
