<?php
class Giftcards extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'gift-cards';
		$views['section'] = 'gift-cards';
		$this->load->view('template', $views);
	}
}
