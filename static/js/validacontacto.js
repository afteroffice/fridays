
function trim(cadena)
{
	cadena = cadena.replace("'", "").replace("\"", "");
	for(i=0; i<cadena.length; )
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(i+1, cadena.length);
		else
			break;
	}

	for(i=cadena.length-1; i>=0; i=cadena.length-1)
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(0,i);
		else
			break;
	}
	
	return cadena;
}

function ValidaSuscripcion(){
		var Nombre = trim(document.getElementById("strNombre").value);
		if (Nombre.length < 5)
		{
			alert ("Error en el Nombre y Apellidos\nPor favor, verifique");
			document.getElementById("strNombre").focus();
			return false;	
		}
		
		document.getElementById("mc-embedded-subscribe-form").submit();
}

function ValidaForm(){
		var tmpError = false;
		document.getElementById("divAlerta").style.display="none";
		var elNombre = trim(document.getElementById("strNombres").value);
		if (elNombre.length < 3)
		{
			//alert ("Error en los nombres");
			document.getElementById("strNombres").focus();
			tmpError=true;
			//return false;	
		}
		var elApellidos = trim(document.getElementById("strApellidos").value);
		if (elApellidos.length < 1)
		{
			//alert ("Error en los Apellidos");
			document.getElementById("strApellidos").focus();
			tmpError=true;
			//return false;	
		}
		
		var tmpTeloCorreo = false;
		var x=document.getElementById("strEmail").value;
		if (trim(x).length > 0)
		{
			tmpTeloCorreo=true;
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
			{
			  //alert("e-mail no válido");
			  document.getElementById("strEmail").focus();
			  tmpError=true;
			  //return false;
			}
		}
		
		if (isNaN(trim(document.getElementById("strPhone").value)) && document.getElementById("strPhone").value.length >= 7)
		{
			//alert("Error en el número de teléfono. Deben ser sólo números\nPor favor, verifique");	
			document.getElementById("strPhone").focus();
			tmpTeloCorreo=true;
			tmpError=true;
			//return false;
		}
		
		if (!tmpTeloCorreo)
			document.getElementById("strEmail").focus();
		
		var elComentario = trim(document.getElementById("txtComentarios").value);
		if (elComentario.length < 10)
		{
			//alert ("Error en los comentarios");
			document.getElementById("txtComentarios").focus();
			tmpError=true;
			//return false;	
		}
		
		if (tmpError || !tmpTeloCorreo)
		{
			document.getElementById("divAlerta").style.display="block";
		} 
		else
		{
			document.getElementById("postContacto").submit();	
		}
}

function BorrarForm(){
	var frm_elements = document.getElementById("postContacto").elements;
	for(i=0; i<frm_elements.length; i++)
	{
		field_type = frm_elements[i].type.toLowerCase();
		//alert("field_type: " + field_type);
		if (field_type == "text" || field_type == "email" || field_type == "tel" || field_type == "textarea")
			frm_elements[i].value="";
		//alert("indexOf: " + field_type.indexOf("select"));
		if (field_type.indexOf("select") == 0)
			frm_elements[i].selectedIndex = 0;
	}
	
}
