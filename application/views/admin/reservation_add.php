<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h2><i class="icon-edit"></i>Reservas</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<fieldset class="col-sm-12">
						<div class="form-group">
							<label class="control-label" >Nombre:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->name ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Telefono:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->telephone ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Correo:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->email ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Lugar:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->place ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Fecha:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->date ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Hora:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->hour ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Motivo:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->reason ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Num. de Personas:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=$content->num_person ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label" >Promocion:</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?=($content->promo_receive != "" && $content->promo_receive != "0") ? $content->promo_receive : "no recibir"; ?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('title') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Observaciones</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'observation',
											'id'          => 'observation',
											'rows'         => 4,
											'value'       => @field($content->observation, set_value('observation')),
											'class'       => 'form-control',
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Guardar</button>
							<a href="<?=base_url() ?>admin/reservation" type="submit" class="btn btn-danger">Regresar</a>
						</div>
					</fieldset>
				</form>   
			</div>
		</div>
	</div>
</div>
