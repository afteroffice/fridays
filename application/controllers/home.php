<?php
class Home extends CI_Controller {
	
	public function index()
	{
		$this->load->model('slider_model');
		$this->load->model('city_model');
		$data = $this->slider_model->getAll(array('status' => 'active'));
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['data'] = $data;
		$views['cities'] = $cities;
		$views['content_view'] = 'slider';
		$views['section'] = 'slider';
		$this->load->view('template', $views);
	}
}
