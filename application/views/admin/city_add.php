<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h2><i class="icon-edit"></i>Ciudad</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<fieldset class="col-sm-12">
						<div class="form-group <?=(form_error('title') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Titulo</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'title',
											'id'          => 'title',
											'value'       => @field($content->title, set_value('title')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('slug') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Slug</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'slug',
											'id'          => 'slug',
											'value'       => @field($content->slug, set_value('slug')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Guardar</button>
							<a href="<?=base_url() ?>admin/city" type="submit" class="btn btn-danger">Regresar</a>
						</div>
					</fieldset>
				</form>   
			</div>
		</div>
	</div>
</div>
