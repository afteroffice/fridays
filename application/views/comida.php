<div class="container clearfix">
	<div class="com-tit" style="background:url(<?=base_url() ?>uploads/images/<?=$category->image ?>);" >
		<div class="com-tit1" >
			<h1><?=$category->title ?></h1>
			<h2><?=$category->subtitle ?></h2>
		</div>
	</div>
</div>
<div class="container com-promo">
	<h2><?=$category->content ?></h2>
</div>
<div class="container">
	<div class="menu-comidas">
		<ul>
			<?php foreach ($categories as $c) : ?>
			<li><a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/comida/<?=$c->slug ?>" title="" class="<?=($c->id == $category->id) ? 'activo' : '' ?>" ><?=$c->title ?></a></li>
			<?php endforeach; ?>
		</ul>
		<div class="com-select">
			<select onchange="location.href = this.options[this.selectedIndex].value;">
				<option value="#" selected>MENU COMIDAS...</option>
				<?php foreach ($categories as $c) : ?>
				<option value="<?=$c->slug ?>">- <?=$c->title ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="comidas">
		<ul class="com1 clearfix">
			<?php foreach ($items as $i) : ?>
			<li>
				<?php if ($i->image != "") : ?>
				<div class="img-comidas">
					<img src="<?=base_url() ?>uploads/images/<?=$i->image ?>" alt="<?=$i->title ?>" class="img-com1">
					<img src="<?=base_url() ?>uploads/images/<?=$i->image_3 ?>" alt="<?=$i->title ?>" class="img-com2">
					<img src="<?=base_url() ?>uploads/images/<?=$i->image_2 ?>" alt="<?=$i->title ?>" class="img-com3">
				</div>
				<?php endif; ?>
				<div class="info-comidas <?=($i->image == "") ? 'sin-img' : ''  ?>">
					<h2><?=$i->title ?></h2>
					<p><?=$i->content ?></p>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
