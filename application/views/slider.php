<script>
	$(function() {
		$('.pickadate').pickadate({
			selectMonths: true,
			selectYears: true,
			selectYears: 80,
			format: 'dd/mm/yyyy',
			monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
			weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
			today: '',
			clear: 'Limpiar',
			close: 'Cerrar',
			klass: {
				navPrev: 'hide',
				navNext: 'hide',
				selectMonth: 'picker_select_month',
				selectYear: 'picker_select_year',
			}
		});
		$( "#dialog" ).dialog({
			buttons: [{
				icons: {
					primary: "ui-icon-close"
				},
				click: function() {
					$( this ).dialog( "close" );
				}
			}]
		});
		
		jQuery('body').bind('click', function(e) {
			if(jQuery('#dialog').dialog('isOpen')
				&& !jQuery(e.target).is('.ui-dialog, a')
				&& !jQuery(e.target).closest('.ui-dialog').length
			) {
				jQuery('#dialog').dialog('close');
			}
		});
	}); 
</script>
<style type="text/css">
	#dialog{
		padding: 0;
	}
	
	.date-html5 {
		-webkit-appearance: none;
		-moz-appearance: none;
	}

	#mc_embed_signup{
		background: url(static/images/fondo/sitemod.jpg);
		border-radius: .4em;
		margin: 20px auto;
		text-align: center;
		width: 80%;
		padding-top: 20px;
	}

	#mc_embed_signup .button {
		font-family:csm-font;
		line-height:33px;
		text-transform:uppercase;
		margin:30px 0 60px 0;
		padding:5px 50px;
		color:#FFF;
		background:#CF0907;
		border-radius:0;
	}

	#mc_embed_signup h1{
		color: #AF1A25;
		font-family: csm-font;
		font-size:225%;
		font-weight: 700;
		padding-top: 40px;
		padding-bottom: 40px;
		line-height: 52px;
	}

	#mc_embed_signup h3{
		font-family: csm-font;
		font-weight: 700;
		padding-bottom: 10px;
	}

	#mc_embed_signup img{
		width: 150px;
	}

	#mc_embed_signup form{
		font-size: 18px;
		margin: 10px auto;
		width: 90%
	}
	
	.form-element {
		border: 1px solid #000;
		border-radius: 4px !important;
	}

	.mc-field-group{
		display: inline-block;
		width: 80%;
		margin: 10px 0;
	}
	.mc-field-group input .form-element{
		border: 1.5px solid #000;
		color: black;
		display: block;
		width: 90%;
		padding: 5px
	}

	.ui-dialog .ui-dialog-buttonpane{
		background: transparent;
		border: 0;
		left: -12%;
		/*left: -78px;*/
		margin: 0;
		top: -705px;
		padding: 0;
		
		position: relative;
	}
	.ui-button-text-icon-primary .ui-button-text, .ui-button-text-icons .ui-button-text{
		padding: .7em 1em .5em .8em;
	}

	.ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br{
		background: transparent;
		border: none;
		z-index: 2000;
	}


@media only screen and (max-width: 1199px) { /*970 iPad Horizontal*/

}

@media only screen and (max-width: 969px) { /*768 iPad Vertical*/
	.ui-dialog{
		left: 150px;
	}
}

@media only screen and (max-width: 767px) { /*640 Tablet Vert*/
	.ui-dialog .ui-dialog-buttonpane{
		left: -2%;
	}
	/*#dialog{*/
	.ui-widget-content{
		max-width:95%;
	}

	.ui-dialog{
		left: 20px;
		max-width: 100%;
	}
	#mc_embed_signup{
		margin: 0;
		width: 100%;
	}
	input::-webkit-input-placeholder { font-size: 14pt;}
	input::-moz-placeholder { font-size: 14pt;}
	input:-ms-input-placeholder { font-size: 14pt;}
	input:-moz-placeholder { font-size: 14pt;}
}

@media only screen and (max-width: 567px){ /*320*/
	.ui-dialog .ui-dialog-buttonpane{
		/*left: -400px;*/
		left: -1%;
		top: -500px;
	}
	/*#dialog{*/
	.ui-widget-content{
		max-width:100%;
	}

	.ui-dialog{
		left: 20px;
		max-width: 90%;
	}
	#mc_embed_signup{
		margin: 0;
		width: 100%;
	}
	#mc_embed_signup h1 {
		font-size: 145%;
		padding-top: 10px;
		padding-bottom: 10px;
		line-height: 42px;
	}
	
	#mc_embed_signup h3 {
		font-size: 60%;
		padding-bottom: 5px;
		line-height: 17px;
	}
	
	#mc_embed_signup form {
		margin-top: 0px;
	}
	
	#mc_embed_signup img {
		width: 100px;
	}
	input::-webkit-input-placeholder { font-size: 10pt;}
	input::-moz-placeholder { font-size: 10pt;}
	input:-ms-input-placeholder { font-size: 10pt;}
	input:-moz-placeholder { font-size: 10pt;}
}

@media only screen and (max-width: 320px){ 
	.ui-dialog .ui-dialog-buttonpane{
		top: -540px;
	}
}

@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 568px)
  and (-webkit-min-device-pixel-ratio: 2) {
	.mc-field-group{
		display: inline-block;
		width: 95%;
		margin: 10px 0;
		margin-top: 1px;
		margin-bottom: 1px;
	}
}
</style>
<div class="container">
	<div id="sequence">
		<img class="sequence-prev" src="<?=base_url() ?>static/images/bt-prev.png" alt="Anterior" />
		<img class="sequence-next" src="<?=base_url() ?>static/images/bt-next.png" alt="Siguiente" />
		<ul class="sequence-canvas">
			<?php foreach ($data as $i => $d) : ?>
			<li class="<?=($i== 0) ? 'animate-in' : '' ?>"><div class="info"><h2><?=$d->title ?></h2><h3><?=$d->subtitle ?></h3><?=$d->content ?></div><img class="sl-1" src="<?=base_url() ?>uploads/images/<?=$d->image ?>" alt="T.G.I. Friday's" />
			<?php endforeach; ?>
		</ul>
	</div>
</div>

<div id="dialog" title="suscribete">
	<div id="mc_embed_signup">
		<img src="<?=base_url() ?>static/images/logo-fridays.png" alt=" suscripción fridays" />
		<h1 id="title-modal">¡PROMOCIONES Y NOVEDADES EXCLUSIVAS PARA TI!</h1>
		<h3 class="subtitle-modal">¡Llena tus datos y listo!</h3>
		<form id="mc-embedded-subscribe-form" action="http://fridaysperu.us5.list-manage.com/subscribe/post?u=1afc9d75153efb95fafb40f3a&amp;id=5cfd4c73f0" method="post" class="validate" target="_blank">
			<div class="mc-field-group">
				<input type="text" value="" name="NOMBRES" placeholder="Nombres" class="form-element required" id="mce-NOMBRES" style="border: 1.5px solid #000;color: black;display: block;width: 90%;padding: 5px" data-validation="required" />
			</div>
			<div class="mc-field-group">
				<input type="text" value="" name="APELLIDOS" placeholder="Apellidos" class="form-element required" id="mce-APELLIDOS" style="border: 1.5px solid #000;color: black;display: block;width: 90%;padding: 5px" data-validation="required" />
			</div>
			<div class="mc-field-group">
				<input type="email" value="" name="EMAIL" placeholder="Correo Electrónico" class="form-element required email" id="mce-EMAIL" style="border: 1.5px solid #000;color: black;display: block;width: 90%;padding: 5px" data-validation="required" />
			</div>
			<div class="clear"></div>
<div id="mce-responses">
	<div class="response" id="mce-error-response" style="display:none"></div>
	<div class="response" id="mce-success-response" style="display:none"></div>
</div>	
<div class="clear"></div>
			<input type="submit" value="Suscribirme" name="subscribe" id="mc-embedded-subscribe" class="button">
		</form>
	</div>
</div>

<script type="text/javascript">
		var fnames = new Array();var ftypes = new Array();fnames[1]='NOMBRES';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';fnames[2]='DNI';ftypes[2]='text';fnames[3]='FECNAC-fake-date';ftypes[3]='date';
		try {
			var jqueryLoaded=jQuery;
			jqueryLoaded=true;
		} catch(err) {
			var jqueryLoaded=false;
		}
		var head= document.getElementsByTagName('head')[0];
		if (!jqueryLoaded) {
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
			head.appendChild(script);
			if (script.readyState && script.onload!==null){
				script.onreadystatechange= function () {
					if (this.readyState == 'complete') mce_preload_check();
				}    
			}
		}
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
		head.appendChild(script);
		var err_style = '';
		try{
			err_style = mc_custom_error_style;
		} catch(e){
			err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
		}
		var head= document.getElementsByTagName('head')[0];
		var style= document.createElement('style');
		style.type= 'text/css';
		if (style.styleSheet) {
			style.styleSheet.cssText = err_style;
		} else {
			style.appendChild(document.createTextNode(err_style));
		}
		head.appendChild(style);
		setTimeout('mce_preload_check();', 250);
		var mce_preload_checks = 0;
		function mce_preload_check(){
			if (mce_preload_checks>40) return;
			mce_preload_checks++;
			try {
				var jqueryLoaded=jQuery;
			} catch(err) {
				setTimeout('mce_preload_check();', 250);
				return;
			}
			try {
				var validatorLoaded=jQuery("#fake-form").validate({});
			} catch(err) {
				setTimeout('mce_preload_check();', 250);
				return;
			}
			mce_init_form();
		}
		function mce_init_form(){
			jQuery(document).ready( function($) {
				var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
				//var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
				//$("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
				options = { url: 'http://fridaysperu.us5.list-manage.com/subscribe/post-json?u=1afc9d75153efb95fafb40f3a&id=5cfd4c73f0&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
					success: mce_success_cb
				};
				$('#mc-embedded-subscribe-form').ajaxForm(options);
 
			jQuery.extend(jQuery.validator.messages, {
				required: "Este campo es obligatorio.",
				remote: "Por favor, rellena este campo.",
				email: "Por favor, escribe una direcci&oacute;n de correo v&acute;lida",
				url: "Por favor, escribe una URL v&aacute;lida.",
				date: "Por favor, escribe una fecha v&aacute;lida.",
				dateISO: "Por favor, escribe una fecha (ISO) v&acute;lida.",
				number: "Por favor, escribe un n&uacute;mero entero v&aacute;lido.",
				digits: "Por favor, escribe s&oacute;lo d?gitos.",
				creditcard: "Por favor, escribe un n?mero de tarjeta v&aacute;lido.",
				equalTo: "Por favor, escribe el mismo valor de nuevo.",
				accept: "Por favor, escribe un valor con una extensi&oacute;n aceptada.",
				maxlength: jQuery.validator.format("Por favor, no escribas m&aacute;s de {0} caracteres."),
				minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
				rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
				range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
				max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
				min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
			});
		});
	}
	/*function mce_success_cb(resp){
		setTimeout(function(){
			$( "#dialog" ).dialog('close');
		}, 2000);
		$('#title-modal').html('Gracias por Suscribirte');
		$('.subtitle-modal').hide();
		$('#mc-embedded-subscribe-form').hide();
		if (resp.result=="success"){
			$('#mce-'+resp.result+'-response').show();
			$('#mce-'+resp.result+'-response').html(resp.msg);
			$('#mc-embedded-subscribe-form').each(function(){
				this.reset();
			});
			$('#title-modal').html('Gracias por<br />Suscribirte');
			$('.subtitle-modal').hide();
			$('#mc-embedded-subscribe-form').hide();
		} else {
			$('#title-modal').html('Gracias por Suscribirte');
			$('.subtitle-modal').hide();
			$('#mc-embedded-subscribe-form').hide();
		}
	}*/
	function mce_success_cb(resp) {
	$('#mce-success-response').hide();
	$('#mce-error-response').hide();
	$('.subtitle-modal').hide();
	$('#mc-embedded-subscribe-form').hide();
	$('#title-modal').css({"line-height": "35px", "font-size": "30px", "padding": "10px"});
	$('#title-modal').html('¡Ya casi terminamos!<br/><br/>En breve te llegará un correo electrónico. Debes hacer click en el mensaje para confirmar tu suscripción.<br/>');
	if (resp.result=="success") {
		$('#mce-'+resp.result+'-response').show();
		$('#mce-'+resp.result+'-response').html(resp.msg);
		$('#mc-embedded-subscribe-form').each(function(){
			this.reset();
		})	;
	} else {
		var index = -1;
		var msg;
		try {
			var parts = resp.msg.split(' - ',2);
			if (parts[1]==undefined){
				msg = resp.msg;
			} else {
				i = parseInt(parts[0]);
				if (i.toString() == parts[0]){
					index = parts[0];
					msg = parts[1];
				} else {
					index = -1;
					msg = resp.msg;
				}
			}
		} catch(e){
			index = -1;
			msg = resp.msg;
		}
		msg = msg.replace("ya est\u00e1 suscrito a esta lista &quot;Novedades Fridays&quot;. <a href=\"http:\/\/feedback.us5.list-manage.com\/subscribe\/send-email?u=1afc9d75153efb95fafb40f3a&id=5cfd4c73f0&e=Y3NhbmNoZXpAYmFraW10ZWNoLmNvbQ==\">Haga clic aqu\u00ed para actualizar sus preferencias.<\/a>", " ya está suscrito a esta lista.");
		msg = msg.replace("csanchez@bakimtech.com ya está suscrito a esta lista &quot;Novedades Fridays&quot;. <a href=\"http://feedback.us5.list-manage2.com/subscribe/send-email?u=1afc9d75153efb95fafb40f3a&id=5cfd4c73f0&e=Y3NhbmNoZXpAYmFraW10ZWNoLmNvbQ==\">Haga clic aquí para actualizar sus preferencias.</a>", " ya está suscrito a esta lista.");
		try {
			if (index== -1) {
				$('#mce-'+resp.result+'-response').show();
				$('#mce-'+resp.result+'-response').html(msg);
				$('#title-modal').html(msg);
			} else {
				err_id = 'mce_tmp_error_msg';
				html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
				var input_id = '#mc_embed_signup';
				var f = $(input_id);
				if (ftypes[index]=='address') {
					input_id = '#mce-'+fnames[index]+'-addr1';
					f = $(input_id).parent().parent().get(0);
				} else if (ftypes[index]=='date') {
					input_id = '#mce-'+fnames[index]+'-month';
					f = $(input_id).parent().parent().get(0);
				} else {
					input_id = '#mce-'+fnames[index];
					f = $().parent(input_id).get(0);
				}
				if (f) {
					$(f).append(html);
					$(input_id).focus();
				} else {
					$('#mce-'+resp.result+'-response').show();
					$('#mce-'+resp.result+'-response').html(msg);
					$('#title-modal').html(msg);
				}
			}
		} catch(e){
			$('#mce-'+resp.result+'-response').show();
			$('#mce-'+resp.result+'-response').html(msg);
			$('#title-modal').html(msg);
		}
	}
	}

	</script>
