<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="es"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="es"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="es"> <!--<![endif]-->

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Fridays Per&uacute; - Restaurante de Comida Casual</title> 
		<?php if (isset($category) && isset($category->tag_keywords)) : ?>
		<meta name="keywords" content="<?=$category->tag_keywords ?>"/>
		<?php elseif (isset($data) && isset($data->tag_keywords)) : ?>
		<meta name="keywords" content="<?=$data->tag_keywords ?>"/>
		<?php else: ?>
		<meta name="keywords" content="Mejores costillas, ribs, wings, Hamburguesas, Fajitas, Quesadillas, Happy hour, Almuerzo ejecutivo"/>
		<?php endif; ?>

		<?php if (isset($category) && isset($category->tag_description)) : ?>
		<meta name="description" content="<?=$category->tag_description ?>"/>
		<?php elseif (isset($data) && isset($data->tag_description)) : ?>
		<meta name="description" content="<?=$data->tag_description ?>"/>
		<?php else: ?>
		<meta name="description" content="Fridays ofrece comida deliciosa y bebidas increíbles. Desde hamburguesas hasta costillas, cervezas y cócteles, conoce porque aquí siempre es Viernes."/>
		<?php endif; ?>

		<!-- Dublin Core -->
		<meta name="DC.title" content="T.G.I. Fridays&reg; Perú" />
		<meta name="DC.identifier" content="http://www.bakimtech.com/" />
		<meta name="DC.description" content="T.G.I. Fridays&reg; Perú en Lima" />
		<meta name="DC.subject" content="fridays" />

		<!-- Page Metas -->
		<meta name="copyright" content="" /> 
		<meta name="robots" content="index,follow,all" />

		<meta name="DC.Language" scheme="RFC1766" content="Spanish"> 
		<meta name="DC.language" scheme="ISO639-1" content="es" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">

		<link href="<?=base_url() ?>static/favicon.ico" rel="shortcut icon">
		<link href="<?=base_url() ?>static/images/icons/apple-touch-icon-iphone-57px.png" rel="apple-touch-icon" sizes="57x57">
		<link href="<?=base_url() ?>static/images/icons/apple-touch-icon-ipad-72px.png" rel="apple-touch-icon" sizes="72x72">
		<link href="<?=base_url() ?>static/images/icons/apple-touch-icon-iphoneretina-114px.png" rel="apple-touch-icon" sizes="114x114">
		<link href="<?=base_url() ?>static/images/icons/apple-touch-icon-ipadretina-144px.png" rel="apple-touch-icon" sizes="144x144">
		<meta name="geo.placename" content="Lima, Perú" />
		<meta name="geo.position" content="-12.047816;-77.062203" />
		<meta name="geo.region" content="PE-Lima" />
		<meta name="ICBM" content="-12.047816, -77.062203" />
		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/css/style.css?v=10">
		<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url() ?>static/css/sequence.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/css/jquery.fancybox.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/pickadate/lib/themes/default.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/pickadate/lib/themes/default.date.css" media="screen" />
		
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/pickadate/lib/themes/classic.date.css" media="screen" />
		<?php if ($section == 'suscribete') : ?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
		<?php else: ?>
		<script src="<?=base_url() ?>static/js/jquery-min.js"></script>
		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script>
		 if (typeof jQuery == 'undefined'){
				document.write(unescape('%3Cscript src="<?=base_url() ?>static/js/jquery-min.js" %3E%3C/script%3E'));
		 }
		</script>-->
		<?php endif; ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>
		<script src="<?=base_url() ?>static/js/main.js?v=14"></script>
		<?php if ($section != 'suscribete' && $section != 'reservas'): ?>
		<script type="text/javascript" src="<?=base_url() ?>static/js/validacontacto.js"></script>
		<script src="<?=base_url() ?>static/js/jquery.sequence-min.js"></script>
		<script src="<?=base_url() ?>static/js/sequencejs.js"></script>
		<?php endif; ?>
		<script type="text/javascript" src="<?=base_url() ?>static/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="<?=base_url() ?>static/pickadate/lib/picker.js"></script>
		<script type="text/javascript" src="<?=base_url() ?>static/pickadate/lib/picker.date.js"></script>
		<!--[if lt IE 8 ]>
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/css/ie.css">
		<![endif]-->

		<!--[if IE 8 ]>
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/css/ie8.css">
		<![endif]-->

		<!--[if IE 9 ]>
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/css/ie9.css">
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" />
		<?php if (isset($category) && isset($category->convertion_facebook)) : ?>
		<?=$category->convertion_facebook ?>
		<?php endif; ?>
		<?php if (isset($data) && isset($data->convertion_facebook)) : ?>
		<?=$data->convertion_facebook ?>
		<?php endif; ?>

		<?php 
			$ci =&get_instance();
			$ci->load->model('seo_model'); 
		?>
		<?=$ci->seo_model->getAnalytics() ?>
	</head>
	<body>
		<?php if (isset($category) && isset($category->convertion_google)) : ?>
		<?=$category->convertion_google ?>
		<?php endif; ?>
		<?php if (isset($data) && isset($data->convertion_facebook)) : ?>
		<?=$data->convertion_google ?>
		<?php endif; ?>
		<header class="clearfix">
			<div class="cabecera">
				<div class="container">
					<div class="logo"><a href="<?=base_url() ?>" title="T.G.I. Fridays Lima">T.G.I. Fridays Lima</a></div>
					<div class="reservas">
						<div class="telefonos">
						  <p>Reservas Telefónicas</p>
						  <p class="tel"><a href="tel:016106969">Lima: 610-6969</a></p>
						  <p class="tel">Arequipa: (054) 613-300</p>
						  <p class="tel">Trujillo: (064) 613-300</p>
						</div>
						<a href="<?=base_url() ?>reservas" title="" class="btn-reservas">Reservas Online</a>
					</div>
					
					<nav>
						<?php if ($section == 'slider' || $section == 'conocenos' || $section == 'mapa-sitio' || $section == 'eventos' || $section == 'trabaja-nosotros' || $section == 'reservas' || $section == 'contactanos' || $section == 'suscribete' || $section == 'gift-cards') : ?>
						<div class="menu0">
							<h1>Escoge tu ciudad</h1>
							<ul>
								<?php foreach ($cities as $c) : ?>
								<li><a href="<?=base_url() ?>ciudad/<?=$c->slug ?>" title=""><?=$c->title ?></a></li>
								<?php endforeach; ?>
							</ul>
							<div class="btn-fb"><p>Síguenos en:</p><a href="https://www.facebook.com/TGIFridaysPeru" title="Siguenos en Facebook" class="fb-icon" target="_blank">Facebook</a><a href="https://www.instagram.com/fridaysperu/" title="Siguenos en Instagram" class="ig-icon" target="_blank">Instagram</a></div>
						</div>
						<?php else: ?>
						<div class="menu1">
							<ul>
								<?php if (isset($c_food) && count($c_food) > 0) : ?>
								<li><a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/comida/<?=$c_food[0]->slug ?>" title="">comidas</a></li>
								<?php endif; ?>
								<?php if (isset($c_bar) && count($c_bar) > 0) : ?>
								<li><a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/bar/<?=$c_bar[0]->slug ?>" title="">bar</a></li>
								<?php endif; ?>
								<li><a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/promociones" title="">promociones</a></li>
							</ul>
							<div class="btn-fb"><p>Síguenos en:</p><a href="https://www.facebook.com/TGIFridaysPeru" title="Siguenos en Facebook" class="fb-icon" target="_blank">Facebook</a><a href="https://www.instagram.com/fridaysperu/" title="Siguenos en Instagram" class="ig-icon" target="_blank">Instagram</a></div>
						</div>
						<?php endif; ?>
					</nav>
					<div class="clear"></div>
				</div>
			</div>
		</header>
		<section id="<?=$section ?>">
			<?php $this->load->view($content_view); ?>
		</section>
		<footer>
			<div class="clearfix">
				<div class="pie">
					<div class="container fdo-pie">
						<nav>
							<div class="menu2">
								<ul>
									<li><a href="<?=base_url() ?>conocenos" title="">con&oacute;cenos</a></li>
									<li><a href="<?=base_url() ?>contactanos" title="">cont&aacute;ctanos</a></li>
									<li><a href="<?=base_url() ?>suscribete" title="">suscr&iacute;bete</a></li>
								</ul>
							</div>
						</nav>
						<nav>
							<div class="menu3">
								<ul>
									<li><a href="<?=base_url() ?>giftcards" title="">Gift Cards</a></li>
									<li><a href="<?=base_url() ?>eventos" title="">eventos</a></li>
									<li><a href="http://fridaysperu.aptitus.com/" target="_blank" title="">trabaja con nosotros</a></li>
									<li><a href="<?=base_url() ?>mapasitio" title="">mapa de sitio</a></li>
								</ul>
							</div>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</footer>
		<?php if ($section == 'suscribete') : ?>
		<script type="text/javascript">
		var fnames = new Array();var ftypes = new Array();fnames[1]='NOMBRES';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';fnames[2]='APELLIDOS';ftypes[2]='text';fnames[3]='DNI';ftypes[3]='text';fnames[4]='CELULAR';ftypes[4]='text';fnames[5]='FECNAC';ftypes[5]='date';fnames[6]='DPTO';ftypes[6]='dropdown';
		try {
			var jqueryLoaded=jQuery;
			jqueryLoaded=true;
		} catch(err) {
			var jqueryLoaded=false;
		}
		var head= document.getElementsByTagName('head')[0];
		if (!jqueryLoaded) {
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
			head.appendChild(script);
			if (script.readyState && script.onload!==null){
				script.onreadystatechange= function () {
					if (this.readyState == 'complete') mce_preload_check();
				}    
			}
		}
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://downloads.mailchimp.com/js/jquery.form-n-validate.js';
		head.appendChild(script);
		var err_style = '';
		try{
			err_style = mc_custom_error_style;
		} catch(e){
			err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
		}
		var head= document.getElementsByTagName('head')[0];
		var style= document.createElement('style');
		style.type= 'text/css';
		if (style.styleSheet) {
			style.styleSheet.cssText = err_style;
		} else {
			style.appendChild(document.createTextNode(err_style));
		}
		head.appendChild(style);
		setTimeout('mce_preload_check();', 250);
		var mce_preload_checks = 0;
		function mce_preload_check(){
			if (mce_preload_checks>40) return;
			mce_preload_checks++;
			try {
				var jqueryLoaded=jQuery;
			} catch(err) {
				setTimeout('mce_preload_check();', 250);
				return;
			}
			try {
				var validatorLoaded=jQuery("#fake-form").validate({});
			} catch(err) {
				setTimeout('mce_preload_check();', 250);
				return;
			}
			mce_init_form();
		}
		function mce_init_form(){
			jQuery(document).ready( function($) {
			var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
			var mce_validator = $("#mc-embedded-subscribe-form").validate(options);
			$("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
			options = { url: 'http://fridaysperu.us5.list-manage.com/subscribe/post-json?u=1afc9d75153efb95fafb40f3a&id=5add08e3ff&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
				beforeSubmit: function(){
					console.log('asdfasd');
					console.log($('#mce-FECNAC-month').val());
					console.log($('#mce-FECNAC-day').val());
					console.log($('#mce-FECNAC-year').val());
					$('#mce_tmp_error_msg').remove();
					$('.datefield','#mc_embed_signup').each(function(){
						var txt = 'filled';
						var fields = new Array();
						var i = 0;
						$(':text', this).each(function(){
							fields[i] = this;
							i++;
						});
						$(':hidden', this).each(function(){
							var bday = false;
							if (fields.length == 2){
								bday = true;
								fields[2] = {'value':1970};//trick birthdays into having years
							}
							if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
								this.value = '';
							} else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
								this.value = '';
							} else {
								if (/\[day\]/.test(fields[0].name)){
									this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;
								} else {
									this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
								}
							}
						});
					});
					return mce_validator.form();
				}, 
				success: mce_success_cb
			};
			$('#mc-embedded-subscribe-form').ajaxForm(options);
 
			jQuery.extend(jQuery.validator.messages, {
				required: "Este campo es obligatorio.",
				remote: "Por favor, rellena este campo.",
				email: "Por favor, escribe una direcci&oacute;n de correo v&acute;lida",
				url: "Por favor, escribe una URL v&aacute;lida.",
				date: "Por favor, escribe una fecha v&aacute;lida.",
				dateISO: "Por favor, escribe una fecha (ISO) v&acute;lida.",
				number: "Por favor, escribe un n&uacute;mero entero v&aacute;lido.",
				digits: "Por favor, escribe s&oacute;lo d?gitos.",
				creditcard: "Por favor, escribe un n?mero de tarjeta v&aacute;lido.",
				equalTo: "Por favor, escribe el mismo valor de nuevo.",
				accept: "Por favor, escribe un valor con una extensi&oacute;n aceptada.",
				maxlength: jQuery.validator.format("Por favor, no escribas m&aacute;s de {0} caracteres."),
				minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
				rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
				range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
				max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
				min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
			});
		});
	}
	function mce_success_cb(resp){
		$('#mce-success-response').hide();
		$('#mce-error-response').hide();
		if (resp.result=="success"){
			$('#mce-'+resp.result+'-response').show();
			$('#mce-'+resp.result+'-response').html(resp.msg);
			$('#mc-embedded-subscribe-form').each(function(){
				this.reset();
			});
			window.location.href = "<?=base_url() ?>suscribete/gracias";
		} else {
			var index = -1;
			var msg;
			try {
				var parts = resp.msg.split(' - ',2);
				if (parts[1]==undefined){
					msg = resp.msg;
				} else {
					i = parseInt(parts[0]);
					if (i.toString() == parts[0]){
						index = parts[0];
						msg = parts[1];
					} else {
						index = -1;
						msg = resp.msg;
					}
				}
			} catch(e){
				index = -1;
				msg = resp.msg;
			}
			try{
				if (index== -1){
					$('#mce-'+resp.result+'-response').show();
					$('#mce-'+resp.result+'-response').html(msg);            
				} else {
					err_id = 'mce_tmp_error_msg';
					html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
					
					var input_id = '#mc_embed_signup';
					var f = $(input_id);
					if (ftypes[index]=='address'){
						input_id = '#mce-'+fnames[index]+'-addr1';
						f = $(input_id).parent().parent().get(0);
					} else if (ftypes[index]=='date'){
						input_id = '#mce-'+fnames[index]+'-month';
						f = $(input_id).parent().parent().get(0);
					} else {
						input_id = '#mce-'+fnames[index];
						f = $().parent(input_id).get(0);
					}
					if (f){
						$(f).append(html);
						$(input_id).focus();
					} else {
						$('#mce-'+resp.result+'-response').show();
						$('#mce-'+resp.result+'-response').html(msg);
					}
				}
			} catch(e){
				$('#mce-'+resp.result+'-response').show();
				$('#mce-'+resp.result+'-response').html(msg);
			}
		}
	}

	</script>

	<script type="text/javascript">
	function annconf() {
	 
	var mcedpto = document.getElementById('mce-DPTO').value;

	if (mcedpto == "Lima")
		{
			$(".mc-field-group-local").show(500);
			$('#mce-LOCAL').addClass('required');
		}
	else if (mcedpto == "Arequipa" || mcedpto == "Trujillo")
		{
			$(".mc-field-group-local").hide(500);
			$('#mce-LOCAL').removeClass('required');
			$("#mce-LOCAL").val("");
		}
	}
	</script>
	<?php endif; ?>
	<?php if ($section == 'reservas') : ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
	</script>
	<?php endif; ?>
	</body>
</html>
