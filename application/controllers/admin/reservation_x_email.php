<?php
class Reservation extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('reservation_model');
	}
	
	public function index()
	{
		$arrayStatus = array('pending' => 'Pendiente', 'confirm' => 'Confirmado', 'no-confirm' => 'No Confirmado', 'cancel' => 'Cancelado');
		$contents = $this->reservation_model->getAll(array('status' => 'active'));
		$views['arrayStatus'] = $arrayStatus;
		$views['contents'] = array_reverse($contents);
		$views['content_view'] = 'admin/reservation_list';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		if ($this->input->post()) {
			$this->reservation_model->update(
				array(
					'observation' => $this->input->post('observation'),
				),
				$id
			);
			redirect('/admin/reservation');
		}
		$dataReservation = $this->reservation_model->find($id);
		$views['content'] = $dataReservation;
		$views['content_view'] = 'admin/reservation_add';
		$this->load->view('admin/template', $views);
	}
	
	public function delete($id)
	{
		$this->reservation_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/reservation');
	}
	
	public function confirm($id)
	{
		$data['dataReservation'] = $this->reservation_model->find($id);
		$this->reservation_model->update(
			array(
				'status_reservation' => 'confirm',
			),
			$id
		);
		/*$this->load->config('mandrill');
		$this->load->library('mandrill');*/
		$this->load->library('email');
		$this->email->initialize(array('charset' => 'utf-8', 'mailtype' => 'html'));
		$msg = $this->load->view('admin/mailing_reservation_confirm', $data, true);

		// Mail para cliente
		$this->email->from('noreply@fridaysperu.com', 'Fridays Peru');
		$this->email->to($data['dataReservation']->email); 

		$this->email->subject('Confirmacion de Reserva');
		$this->email->message($msg);	

		$this->email->send();
		/*$mandrill_ready = NULL;
		try {
			$this->mandrill->init( $this->config->item('mandrill_api_key') );
			$mandrill_ready = TRUE;
		} catch(Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
			var_dump($e); exit;
		}
		if ($mandrill_ready) {
			$msg = $this->load->view('admin/mailing_reservation_confirm', $data, true);
			$email = array(
				'html' => $msg, 
				'subject' => 'Confirmacion de Reserva',
				'from_email' => 'noreply@fridaysperu.com',
				'from_name' => 'Fridays Peru',
				'to' => array(array('email' => $data['dataReservation']->email)),
			);
			$result = $this->mandrill->messages_send($email);
		}*/
		redirect('/admin/reservation');
	}
	
	public function noconfirm($id)
	{
		$data['dataReservation'] = $this->reservation_model->find($id);
		$this->reservation_model->update(
			array(
				'status_reservation' => 'no-confirm',
			),
			$id
		);
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;
		try {
			$this->mandrill->init( $this->config->item('mandrill_api_key') );
			$mandrill_ready = TRUE;
		} catch(Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
			var_dump($e); exit;
		}
		if ($mandrill_ready) {
			$msg = $this->load->view('admin/mailing_reservation_noconfirm', $data, true);
			$email = array(
				'html' => $msg, 
				'subject' => 'Reserva Cancelada',
				'from_email' => 'noreply@fridaysperu.com',
				'from_name' => 'Fridays Peru',
				'to' => array(array('email' => $data['dataReservation']->email)),
			);
			$result = $this->mandrill->messages_send($email);
		}
		redirect('/admin/reservation');
	}
	
	public function cancel($id)
	{
		$data['dataReservation'] = $this->reservation_model->find($id);
		$this->reservation_model->update(
			array(
				'status_reservation' => 'cancel',
			),
			$id
		);
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;
		try {
			$this->mandrill->init( $this->config->item('mandrill_api_key') );
			$mandrill_ready = TRUE;
		} catch(Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
			var_dump($e); exit;
		}
		if ($mandrill_ready) {
			$msg = $this->load->view('admin/mailing_reservation_cancel', $data, true);
			$email = array(
				'html' => $msg, 
				'subject' => 'Cancelacion de Reserva',
				'from_email' => 'noreply@fridaysperu.com',
				'from_name' => 'Fridays Peru',
				'to' => array(array('email' => $data['dataReservation']->email)),
			);
			$result = $this->mandrill->messages_send($email);
		}
		redirect('/admin/reservation');
	}
	
	public function export()
	{
		$arrayStatus = array('pending' => 'Pendiente', 'confirm' => 'Confirmado', 'no-confirm' => 'No Confirmado', 'cancel' => 'Cancelado');
		$reservations = $this->reservation_model->getAll(array('status' => 'active'));
		header('Content-Disposition: attachment; filename=reservations.xls');
		header('Content-type: application/force-download');
		header('Content-Transfer-Encoding: binary');
		header('Pragma: public');
		echo '<table>';
		echo '<tr>';
		echo '<th>Nombres</th>';
		echo '<th>Telefono</th>';
		echo '<th>Correo</th>';
		echo '<th>Lugar</th>';
		echo '<th>Fecha</th>';
		echo '<th>Hora</th>';
		echo '<th>Motivo</th>';
		echo '<th>Num. de Personas</th>';
		echo '<th>Promocion</th>';
		echo '<th>Observaciones</th>';
		echo '<th>Fecha de Registro</th>';
		echo '<th>Estado</th>';
		echo '</tr>';
		foreach ($reservations as $r) {
			echo '<tr>';
			echo '<td>'.$r->name.'</td>';
			echo '<td>'.$r->telephone.'</td>';
			echo '<td>'.$r->email.'</td>';
			echo '<td>'.$r->place.'</td>';
			echo '<td>'.$r->date.'</td>';
			echo '<td>'.$r->hour.'</td>';
			echo '<td>'.$r->num_person.'</td>';
			echo '<td>'.$r->reason.'</td>';
			echo '<td>'.($r->promo_receive == 'recibir' ? 'si' : 'no').'</td>';
			echo '<td>'.$r->observation.'</td>';
			echo '<td>'.date('d-m-Y', strtotime($r->date_register)).'</td>';
			echo '<td>'.$r->status_reservation.'</td>';
			echo '</tr>';
		}
	}
}
