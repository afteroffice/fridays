<div class="row">
	<p>
		<a href="<?=base_url() ?>admin/city/add" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i> Agregar</a>
	</p>
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header" data-original-title>
				<h2><i class="icon-user"></i><span class="break"></span>Ciudad</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
						  <th>Titulo</th>
						  <th>Acciones</th>
					  </tr>
				  </thead>   
				  <tbody>
					<?php foreach ($contents as $c) : ?>
					<tr>
						<td><?=$c->title ?></td>
						<td>
							<a class="btn btn-success" href="<?=base_url() ?>admin/food/index/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Comida" >
								<i class="fa fa-cutlery "></i>  
							</a>
							<a class="btn btn-success" href="<?=base_url() ?>admin/bar/index/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Bar" >
								<i class="fa fa-beer "></i>  
							</a>
							<a class="btn btn-success" href="<?=base_url() ?>admin/promo/index/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Promociones" >
								<i class="fa fa-star-o  "></i>  
							</a>
							<a class="btn btn-info" href="<?=base_url() ?>admin/city/edit/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Editar" >
								<i class="fa fa-edit "></i>  
							</a>
							<a class="btn btn-danger" href="<?=base_url() ?>admin/city/delete/<?=$c->id ?>" onclick="return confirm('Desea eliminar la ciudad?')" data-toggle="tooltip" data-placement="top" title="Eliminar" >
								<i class="fa fa-remove "></i> 
							</a>
						</td>
					</tr>
					<?php endforeach; ?>
				  </tbody>
			  </table>            
			</div>
		</div>
	</div>
</div>
