<!DOCtYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Fridays</title>

<style type="text/css">
body{
	background: #ededed; margin: 0; padding: 0; min-width: 100%!important;
}
td, p{
	margin: 0;
}
.content{
	width: 100%; max-width: 600px;
} 
</style>
</head>

<body>
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
	<table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table class="content" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0 auto; font-family: Helvetica, Arial, sans-serif; background:#FFFFFF;">
					<tr>
						<th>
							<?php if ($num_person < 20) : ?>
							<img src="<?=base_url()?>static/images/mailing/img/cabecera-enproceso.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
							<?php else: ?>
							<img src="<?=base_url()?>static/images/mailing/img/cabecera-noconfirmada.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
							<?php endif; ?>
						</th>
					</tr>

					<tr style="display:block; padding:20px 0 20px 0; ">
						<td>
							<?php if ($num_person < 20) : ?>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;">Gracias por enviar tu solicitud en breve recibirás un email indicando la disponibilidad de nuestro local. Estos son los datos de tu reserva:</p>
							<p>&nbsp;</p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Nombres y Apellidos:</span> <?=$nombres?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Teléfono:</span> <?=$telefono?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">E-mail:</span> <?=$email?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Local de reserva:</span> <?=$local?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Fecha de reserva:</span> <?=$fecha?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Hora de reserva:</span> <?=$hora?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Nª de personas:</span> <?=$num_person?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Motivo de la reserva:</span> <?=$motivo?></p>
							
							<!--p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Teléfono:</span> <?=$telefono?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">E-mail:</span> <?=$email?></p-->
							<?php else: ?>
							<p style="font-size:18px; color:#FF0000; margin:0 0 5px 0;">Para reservas de 20 personas o más, por favor escribir directamente a reservas@fridaysperu.com. Gracias</p>
							<p>&nbsp;</p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Titular de la reserva:</span> <?=$nombres?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Teléfono:</span> <?=$telefono?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">E-mail:</span> <?=$email?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Local:</span> <?=$local?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Fecha de reserva:</span> <?=$fecha?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Hora:</span> <?=$hora?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Nª de personas:</span> <?=$num_person?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Motivo de la reserva:</span> <?=$motivo?></p>
							
							<!--p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Teléfono:</span> <?=$telefono?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">E-mail:</span> <?=$email?></p-->
							<?php endif; ?>
							<br />
							<br />
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><a href="<?=base_url()?>reservas/cancel/<?=$token ?>" style="font-weight:bold; color:#000000;">Cancelar Reserva</a></p>
						</td>
					</tr>


					<tr>
						<td>
							<img src="<?=base_url()?>static/images/mailing/img/pie.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</body>
</html>
