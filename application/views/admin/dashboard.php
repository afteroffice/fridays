<h1>Dashboard</h1>
<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<?php if ($this->session->userdata('user')->username == 'admin') : ?>
			<div class="box-header">
				<h2><i class="icon-edit"></i>Analytics</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<fieldset class="col-sm-12">
						<div class="form-group <?=(form_error('analytics') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Código</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'analytics',
											'value'       => @field($analytics[0]->value, set_value('analytics')),
											'rows'        => '4',
											'class'       => 'form-control'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</fieldset>
				</form>   
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
