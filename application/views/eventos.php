<section>
	<div class="container">
    <div id="sequence" class="event">
			<img class="sequence-prev" src="<?=base_url() ?>static/images/bt-prev.png" alt="Anterior" />
			<img class="sequence-next" src="<?=base_url() ?>static/images/bt-next.png" alt="Siguiente" />
			<ul class="sequence-canvas">
				<li class="animate-in">
					<div class="eventos">
						<h2>Eventos sociales</h2>
						<p>¡El mejor ambiente para organizar tu evento est&aacute; en Fridays! Tenemos todo lo que necesitas para tu reuni&oacute;n de amigos, el reencuentro con tus compa&ntilde;eros del colegio, la universidad o de tu ex-trabajo.</p>
						<p>T&uacute; lo organizas y nosotros nos encargamos de todo.</p>
						<p>Esperamos tu consulta</p>
						<p>610-6900 anexo 6 / 982 775 498</p>
						<p>marketing@fridaysperu.com</p>
					</div>
					<img class="sl-1" src="<?=base_url() ?>static/images/slides/bg-eventos-sociales.jpg" alt="Eventos sociales" />
				</li>
				<li>
					<div class="eventos">
						<h2>Eventos corporativos</h2>
						<p>Organiza las mejores conferencias, eventos de fin de a&ntilde;o, premiaciones, despedidas, o cualquier otra reuni&oacute;n de tu empresa con nosotros.</p>
            <ul>
            	<li>Ambiente privado</li>
              <li>Data show / ecran</li>
              <li>Opciones de paquetes de acuerdo a tus necesidades</li>
            </ul>
            <p>Esperamos tu consulta</p>
			<p>610-6900 anexo 6 / 982 775 498</p>
			<p>marketing@fridaysperu.com</p>
			
					</div>
					<img class="sl-1" src="<?=base_url() ?>static/images/slides/bg-eventos-corporativos.jpg" alt="Eventos coroprativos" />
				</li>
				<li>
					<div class="eventos">
						<h2>Cumplea&ntilde;os de ni&ntilde;os</h2>
						<p>Porque ellos merecen lo mejor, preparamos un Kids Menu con alternativas distintas para los chicos, adem&aacute;s de tarjetas de invitaci&oacute;n y el mejor ambiente para que ese momento sea inolvidable.</p>
					</div>
					<img class="sl-1" src="<?=base_url() ?>static/images/slides/bg-eventos-cumpleanos.jpg" alt="Cumpleaños de niños" />
				</li>
			</ul>
    </div>
  </div>
</section>
