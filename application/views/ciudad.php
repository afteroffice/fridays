<section>
	<div class="container">
		<div id="sequence">
			<img class="sequence-prev" src="<?=base_url() ?>static/images/bt-prev.png" alt="Anterior" />
			<img class="sequence-next" src="<?=base_url() ?>static/images/bt-next.png" alt="Siguiente" />
			<ul class="sequence-canvas">
				<?php foreach ($data as $i => $d) : ?>
				<li class="<?=($i== 0) ? 'animate-in' : '' ?>"><div class="info"><h2><?=$d->title ?></h2><h3><?=$d->subtitle ?></h3><?=$d->content ?></div><img class="sl-1" src="<?=base_url() ?>uploads/images/<?=$d->image ?>" alt="T.G.I. Friday's" />
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>
