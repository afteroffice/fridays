<section>
	<div class="container bg-cert-regalo clearfix">
    <div class="cert-regalo">
    	<div>
            <h1>Gift Cards</h1>
            <h2>El regalo perfecto para cualquier ocasi&oacute;n.</h2>
            <p>Sorprende con este divertido detalle.</p>
			<p>Tenemos Gift Cards de: S/.25, S/.50 ó S/.100</p><br><br>
			<p>Pide hoy la Gift Card de tu elección en cualquiera de nuestros locales. 
			</p><br>
			
			<p><b>Contáctanos:</b></p>
			<p>marketing@fridaysperu.com</p>
			<p>981 225 888</p>
			<p>(01) 610 - 6906</p><br>
			
			<p>¡REGALA DIVERSIÓN, REGALA FRIDAYS!</p>
        </div>
      	<div class="dlegal">
        	<p class="legal">S&oacute;lo v&aacute;lido para uso en los restaurantes de TGI Fridays Per&uacute;. Esta tarjeta es redimible por el monto total indicado a cambio de servicios y productos vendidos en TGI Fridays. Incluye impuestos y otros recargos. No genera vuelto en efectivo u otros medios de pago. No es v&aacute;lido para propinas. V&aacute;lida por 1 año desde el d&iacute;a de la compra.</p>
        </div>
    </div>
  </div>
</section>

