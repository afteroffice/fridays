$(document).ready( function(){
	$(".inline").colorbox({inline:true, width:"auto"});
	$.validate({
		modules : 'security'
	});
	$(".onlyNumber").keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			(e.keyCode == 65 && e.ctrlKey === true) || 
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				 return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	$(".onlyLetter").keypress(function(event){
		var inputValue = event.which;
		if((inputValue > 47 && inputValue < 58) && (inputValue != 32)){
			event.preventDefault();
		}
	});
	
	$('#r-depa').change(function() {
		$('#selectPlace').show();
		if ($("#r-depa option:selected").text() != "Lima") {
			$('#selectPlace').hide();
		}
	});
});
