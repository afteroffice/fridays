<?php
class Reservation_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "reservation";
	}
	
	public function getDataByToken($token)
	{
		$this->db->select();
		$this->db->where('token', $token);
		$this->db->where('status', 'active');
		$query = $this->db->get($this->_table);
		$result = $query->result();
		if (isset($result[0])) {
			return $result[0];
		}
		return false;
	}
}
