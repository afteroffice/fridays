
function trim(cadena)
{
	cadena = cadena.replace("'", "").replace("\"", "");
	for(i=0; i<cadena.length; )
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(i+1, cadena.length);
		else
			break;
	}

	for(i=cadena.length-1; i>=0; i=cadena.length-1)
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(0,i);
		else
			break;
	}
	
	return cadena;
}


function ValidaForm(){
		var tmpError = false;
		document.getElementById("divAlerta").style.display="none";
		var elNombre = trim(document.getElementById("strNombre").value);
		if (elNombre.length < 5)
		{
			//alert ("Error en los nombres");
			document.getElementById("strNombre").focus();
			tmpError=true;
			//return false;	
		}
				
		var x=document.getElementById("strEmail").value;
		if (trim(x).length > 0)
		{
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
			{
			  //alert("e-mail no válido");
			  document.getElementById("strEmail").focus();
			  tmpError=true;
			  //return false;
			}
		}
		
	
		if (tmpError)
		{
			document.getElementById("divAlerta").style.display="block";
		} 
		else
		{
			
		}
}

function BorrarForm(){
	var frm_elements = document.getElementById("postContacto").elements;
	for(i=0; i<frm_elements.length; i++)
	{
		field_type = frm_elements[i].type.toLowerCase();
		//alert("field_type: " + field_type);
		if (field_type == "text" || field_type == "email" || field_type == "tel" || field_type == "textarea")
			frm_elements[i].value="";
		//alert("indexOf: " + field_type.indexOf("select"));
		if (field_type.indexOf("select") == 0)
			frm_elements[i].selectedIndex = 0;
	}
	
}
