var canSubmit = false;

var nombres;
var paterno;
var materno;
var dni;
var sexo;
var departamento;
var distrito;
var direccion;
var email;
var fijo;
var celular;
var trabajo;
var local;
var horario;
var intereses;
var situacion;
var frecuencia;


		
$(document).ready
(
	function()
	{
		//prevent form submit on enter
		var form = document.getElementById("postSuscripcion");
		form.onsubmit = function()
		{
			validateSuscripcion()
			return canSubmit;
		}
		
		
		
		
		nombres = document.getElementById("NOMB");
		paterno = document.getElementById("APAT");
		materno = document.getElementById("AMAT");
		dni = document.getElementById("DNI");
		sexo = document.getElementsByName("SEXO");
		departamento = document.getElementById("DPTO");
		distrito = document.getElementById("DSTO");
		//direccion = document.getElementById("DIRE");
		email = document.getElementById("EMAIL");
		fijo = document.getElementById("TELE");
		//celular = document.getElementById("CELU");
		//trabajo = document.getElementById("TRAB");
		local = document.getElementById("LVIS");
		//horario = document.getElementsByName("horario[]");
		//intereses = document.getElementById("INTE")
		interes1 = document.getElementById("group_1");
		interes2 = document.getElementById("group_2");
		interes3 = document.getElementById("group_4");
		interes4 = document.getElementById("group_8");
		//informacion = document.getElementsByName("informacion[]");
		//situacion = document.getElementsByName("situacion");
		//frecuencia = document.getElementsByName("frecuencia");
		
	}
);

function departamentoChanged()
{
	if(departamento.value == "Arequipa")
	{
		distrito.disabled = true;
		//trabajo.disabled = true;
		local.disabled = true;
		
		distrito.options[0].selected = true;
		//trabajo.options[0].selected = true;
		local.options[0].selected = true;
	}
	else
	{
		distrito.disabled = false;
		//trabajo.disabled = false;
		local.disabled = false;
	}
}


function resetSuscripcion()
{
	var checkboxes = $("input[type='checkbox']");
	var radios = $("input[type='radio']");
	var selects = $("select");
	
	$.each(checkboxes, function(index,value)
	{
		value.checked = false;
	})
	$.each(radios, function(index,value)
	{
		value.checked = false;
	})
	
	$.each(selects, function(index,value)
	{
		value.disabled = false;
		value.options[0].selected = true;
	})
	
	
	//var inputs = [nombres , paterno, materno, dni , email, fijo]
	var inputs = [nombres , paterno, materno, dni , email]
	for (i = inputs.length-1; i >= 0 ; i--) 
	{
		inputs[i].value = "";
		inputs[i].className = "cF_Input";
	}
}

function validateSuscripcion()
{
	canSubmit = true;
	
	//validate checkboxs & radios
	//---------------------------
	
	var sexoChecked = false;
	for(var i = 0; i<sexo.length; i++)
	{
		if( sexo[i].checked) sexoChecked = true;
	}

	var departamentoChecked = false;
	if(departamento.value != -1) departamentoChecked = true;
	
	var distritoChecked = false;
	if(distrito.value != -1) distritoChecked = true;
	if(distrito.disabled) distritoChecked = true;

	var localChecked = false;
	if(local.value != -1) localChecked = true;
	if(local.disabled) localChecked = true;
	
	

	var interesesChecked = false;
	if(interes1.checked == true || interes2.checked == true || interes3.checked == true || interes4.checked == true) interesesChecked = true;
	
	if(!(sexoChecked && departamentoChecked && distritoChecked && localChecked && interesesChecked)) canSubmit = false;
	
	//var inputs = [nombres , paterno, materno, dni , email, fijo]
	var inputs = [nombres , paterno, materno, dni , email]

	for (i = inputs.length-1; i >= 0 ; i--) 
	{
		inputs[i].className = "cF_Input";
		
		if(deleteBlankSpaces(inputs[i].value) == "")
		{
			canSubmit = false;
			inputs[i].value="";
			inputs[i].focus();
			inputs[i].className = "cF_InputWrong";
		}
	}
	
	if( deleteBlankSpaces(email.value) == "" || deleteBlankSpaces(email.value).indexOf("@") == -1 || deleteBlankSpaces(email.value).indexOf(".") == -1)
	{
		canSubmit = false;
		email.focus();
		email.className = "cF_InputWrong";
	}
	
	if( isNumeric(dni.value) == false)
	{
		canSubmit = false;
		dni.focus();
		dni.className = "cF_InputWrong";
	}
	
	
	if(canSubmit)
	{
		sendContactForm();
		//suscribe();
	}
	else
	{
		alert("Debes de completar o llenar adecuadamente los campos marcados");
	}
	
}

function sendContactForm()
{
	document.postSuscripcion.submit();
}


function deleteBlankSpaces(str) 
{
	return str.replace(/^\s+|\s+$/g,"");
}

function isNumeric(_str)
{
	var strValidChars = "0123456789.-()*# ";

	if (_str.length == 0) return false;
	for (i = 0; i < _str.length; i++)
	{
		var strChar = _str.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) return false;
	}
	return true;
}

