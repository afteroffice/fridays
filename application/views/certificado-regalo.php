<section>
	<div class="container bg-cert-regalo clearfix">
    <div class="cert-regalo">
    	<div>
            <h1>Certificado de regalo</h1>
            <h2>Perfecto para cualquier ocasi&oacute;n</h2>
            <p>&iquest;Qu&eacute; tan grande quieres que sea tu regalo?</p>
            <p>Elige entre cualquiera de nuestros certificados disponibles<br><span>S/. 25, S/. 50 &oacute; S/. 100</span>,<br>y sorprende a esa persona con un
    delicioso momento. Puedes obtenerlo en cualquiera de nuestros establecimientos.</p>
        </div>
      	<div class="dlegal">
        	<p class="legal">S&oacute;lo v&aacute;lido en Per&uacute;. Este certificado es redimible por el monto indicado arriba a cambio de los servicios y productos vendidos en T.G.I. Friday&rsquo;s. Incluye impuestos. No genera vuelto en efectivo u otros medios de pago. No es v&aacute;lido para propinas. V&aacute;lido por 180 d&iacute;as desde su fecha de adquisici&oacute;n.</p>
        </div>
    </div>
  </div>
</section>
