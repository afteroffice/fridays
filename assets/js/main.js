$(document).ready(function(){
	$('.cleditor').cleditor();
	$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
	$('.date-picker').datepicker();
	$('.sort').sortable({
		cursor: 'move',
		axis:   'y',
		update: function(e, ui) {
			var href = $(this).attr('sort-url');
			$(this).sortable("refresh");
			var sorted = $(this).find("tr");
			var data = "";
			$.each(sorted, function(index, item) {
				data+="id[]="+$(item).attr("sort-id")+"&";
			});
			$.ajax({
				type:   'POST',
				url:    href,
				data:   data,
				success: function(msg) {
					$(ui.item).fadeOut("fast", function(){
						$(ui.item).fadeIn("fast");
					});
				}
			});
		}
	});
});
