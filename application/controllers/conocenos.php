<?php
class Conocenos extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'conocenos';
		$views['section'] = 'conocenos';
		$this->load->view('template', $views);
	}
}
