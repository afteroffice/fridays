<section>
	<div class="container bg-transp clearfix">
    <div class="info-hist">
    	<h1>Historia</h1>
      <p>Somos considerada una de las compa&ntilde;&iacute;as l&iacute;deres en la industria de la comida casual a nivel mundial. Empezamos en 1965 en la ciudad de New York, donde Alan Stillman, propietario y fundador, combin&oacute; el sabor de la comida con diversas bebidas en un nuevo concepto de restaurante-bar.</p>
      <p>Somos los creadores de los deliciosos Frozens, cuyo sabor ha conquistado todos los paladares del mundo, y de las hoy famosas Potato Skins. Tambi&eacute;n destacan nuestras incomparables hamburguesas y Buffalo Wings. Adem&aacute;s, somos reconocidos por el excelente servicio personalizado y el trato amigable que garantizan comodidad y satisfacci&oacute;n de nuestros clientes.</p>
      <p>Operamos en 60 pa&iacute;ses y tenemos m&aacute;s de 920 restaurantes en todo el mundo, 578 s&oacute;lo en los Estados Unidos y m&aacute;s de 355 a nivel internacional, de los cuales 8 est&aacute;n en Per&uacute;.</p>
      <p>El primer Fridays en Lima lo inauguramos en 1997 en el &Oacute;valo Guti&eacute;rrez en el distrito de Miraflores, y gracias a la enorme acogida expandimos nuestros locales al Jockey Plaza, La Marina Open Plaza, Larcomar y el Boulevard de Asia. En el 2011 nos descentralizamos e inauguramos nuestro primer local en Arequipa y en 2013 en Trujillo. El 2014 el gusto por Fridays sigui&oacute; creciendo e inauguramos nuestro octavo local en el Real Plaza Salaverry en el distrito de Jes&uacute;s Mar&iacute;a.</p>
      <p>Nuestra variada propuesta nos ha convertido en el lugar favorito de aquellos que tienen un esp&iacute;ritu divertido y que disfrutan de excelentes comidas, bebidas y un servicio inigualable.</p>
      <div class="locales-hist">
      	<select id="selLocal" onchange="changeMap();">
          <option selected value="7">&Oacute;valo Guti&eacute;rrez - Miraflores</option>
          <option value="8">C.C. Larcomar - Miraflores</option>
          <option value="9">C.C. Jockey Plaza - Surco</option>
          <option value="11">C.C. Boulevard de Asia - Ca&ntilde;ete</option>
          <option value="12">C.C. La Marina Open Plaza - San Miguel</option>
          <option value="13">C.C. Real Plaza Cayma - Arequipa</option>
		  <option value="18">C.C. Real Plaza - Trujillo</option>
		  <option value="19">C.C. Real Plaza Salaverry - Jes&uacute;s Mar&iacute;a</option>
		  <option value="20">Primavera -  San Borja</option>
        </select>
      </div>
    </div>
  	<div class="mapa-hist">
    	<img src="<?=base_url() ?>static/images/conocenos/bg-iphone1.png" alt="Conócenos">
    	<img src="<?=base_url() ?>static/images/conocenos/bg-iphone2.png" alt="Conócenos">
    	<img src="<?=base_url() ?>static/images/conocenos/bg-iphone3.png" alt="Conócenos">
    	<img src="<?=base_url() ?>static/images/conocenos/bg-iphone4.png" alt="Conócenos">
      <div class="gmap">
        <iframe id="frameMaps" width="272" height="465" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=fridays+lima&sll=40.396764,-3.713379&sspn=13.111272,28.54248&ie=UTF8&hq=fridays&hnear=Lima,+Per%C3%BA&cid=18376977518283762948&ll=-12.110662,-77.037334&spn=0.014686,0.018239&z=16&output=embed"></iframe><br />
      </div>
    </div>
  </div>
</section>
<script language="javascript" type="text/javascript">
<!--
var sourceMaps = new Array();
sourceMaps[7]="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=fridays+lima&sll=40.396764,-3.713379&sspn=13.111272,28.54248&ie=UTF8&hq=fridays&hnear=Lima,+Per%C3%BA&cid=18376977518283762948&ll=-12.110662,-77.037334&spn=0.014686,0.018239&z=15&output=embed";
sourceMaps[8]="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=fridays+lima&sll=40.396764,-3.713379&sspn=13.111272,28.54248&ie=UTF8&hq=fridays&hnear=Lima,+Per%C3%BA&ll=-12.131893,-77.030554&spn=0.009524,0.019054&z=15&iwloc=A&output=embed";
sourceMaps[9]="http://maps.google.com/maps?f=q&source=s_q&hl=es&geocode=&q=T.G.I.+Friday's%E2%80%8E&sll=-12.12946,-77.026198&sspn=0.015692,0.027874&ie=UTF8&hq=T.G.I.+Friday's%E2%80%8E&hnear=&cid=9229549178457507907&ll=-12.085989,-76.976051&spn=0.019052,0.038109&z=14&output=embed";
sourceMaps[11]="http://maps.google.es/maps?q=friday's+lima+peru+asia&hl=es&cd=1&ei=gx5jTMCwLIOYzAS426Q1&ie=UTF8&view=map&cid=3426872180844730735&ved=0CBQQpQY&hq=friday's+lima+peru+asia&hnear=&ll=-12.762585,-76.603031&spn=0.014649,0.018239&z=14&output=embed";
sourceMaps[12]="http://maps.google.es/maps?q=friday's+lima+peru+asia&hl=es&cd=1&ei=gx5jTMCwLIOYzAS426Q1&ie=UTF8&view=map&cid=12231284632117257834&ved=0CBQQpQY&hq=friday's+lima+peru+asia&hnear=&ll=-12.078897,-77.08776&spn=0.009526,0.019054&z=15&output=embed";
sourceMaps[13]="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=T.G.I.+Friday's,+Arequipa,+Per%C3%BA&aq=0&oq=fridays+arequi&sll=-12.761057,-76.602441&sspn=0.058514,0.104628&ie=UTF8&hq=tgi+friday's&hnear=Arequipa,+Per%C3%BA&t=m&z=14&iwloc=B&cid=2448109021367677578&ll=-16.389304,-71.549422&output=embed";
sourceMaps[18]="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=Real+Plaza,+Trujillo,+La+Libertad,+Per%C3%BA&aq=4&oq=real+plaza+&sll=40.396764,-3.713379&sspn=11.689818,26.784668&ie=UTF8&hq=Real+Plaza,&hnear=Trujillo,+La+Libertad,+Per%C3%BA&ll=-8.132181,-79.031767&spn=0.007424,0.013078&t=m&z=14&iwloc=A&cid=4700059015012980296&output=embed";
sourceMaps[19]="http://maps.google.es/maps?f=q&source=s_q&hl=es&geocode=&q=Real+Plaza,+Trujillo,+La+Libertad,+Per%C3%BA&aq=4&oq=real+plaza+&sll=40.396764,-3.713379&sspn=11.689818,26.784668&ie=UTF8&hq=Real+Plaza,&hnear=Trujillo,+La+Libertad,+Per%C3%BA&ll=-8.132181,-79.031767&spn=0.007424,0.013078&t=m&z=14&iwloc=A&cid=4700059015012980296&output=embed";
sourceMaps[20]="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3901.0236009038617!2d-76.9873009!3d-12.1105368!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c794902845d5%3A0x3082b298106158d6!2sAv.+Primavera+737%2C+Lima+15037!5e0!3m2!1ses-419!2spe!4v1453462878647";

function changeMap()
{
	var elSel = $("#selLocal").val();
	//alert("elSel.value: " + elSel);
	var url = sourceMaps[elSel];
	//alert ("url:" + url);
	$('#frameMaps').attr('src', url);
    $('#frameMaps').reload();
}

-->
</script>
