<div class="row">
	<p>
		<a href="<?=base_url() ?>admin/food/index/<?=$category->city?>" class="btn btn-lg btn-danger"><i class="fa fa-chevron-left"></i> Regresar</a>
		<a href="<?=base_url() ?>admin/food/additem/<?=$category->id ?>" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i> Agregar</a>
	</p>
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header" data-original-title>
				<h2><i class="icon-user"></i><span class="break"></span>Platos de la categoria "<?=$category->title?>"</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
						  <th>Titulo</th>
						  <th>Acciones</th>
					  </tr>
				  </thead>   
				  <tbody class="sort" sort-url="<?=base_url() ?>/admin/food/orderitem">
					<?php foreach ($contents as $c) : ?>
					<tr sort-id="<?=$c->id ?>" class="<?=($c->status == 'inactive') ? 'danger' : '' ?>">
						<td><?=$c->title ?></td>
						<td>
							<a class="btn btn-success" href="<?=base_url() ?>admin/food/duplicateitem/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Duplicar" >
								<i class="fa fa-repeat "></i>  
							</a>
							<a class="btn btn-info" href="<?=base_url() ?>admin/food/edititem/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Editar" >
								<i class="fa fa-edit "></i>  
							</a>
							<?php if ($c->status == 'active') : ?>
							<a class="btn btn-info" href="<?=base_url() ?>admin/food/disableitem/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Deshabilitar" >
								<i class="fa fa-toggle-off "></i>  
							</a>
							<?php else : ?>
							<a class="btn btn-info" href="<?=base_url() ?>admin/food/enableitem/<?=$c->id ?>" data-toggle="tooltip" data-placement="top" title="Habilitar" >
								<i class="fa fa-toggle-on "></i>  
							</a>
							<?php endif; ?>
							<a class="btn btn-danger" href="<?=base_url() ?>admin/food/deleteitem/<?=$c->id ?>" onclick="return confirm('Desea eliminar el plato?')" data-toggle="tooltip" data-placement="top" title="Eliminar" >
								<i class="fa fa-remove "></i> 
							</a>
						</td>
					</tr>
					<?php endforeach; ?>
				  </tbody>
			  </table>            
			</div>
		</div>
	</div>
</div>
