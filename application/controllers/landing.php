<?php
class Landing extends CI_Controller {

	public function burger($placeUrl)
	{
		if ($this->input->post()) {
			$this->load->model('landingburger_model');
			$this->load->library('fpdf');
			$this->load->config('mandrill');
			$this->load->library('mandrill');
			$place = $this->input->post('place');
			if ($this->input->post('city') != "Lima") {
				$place = '';
			}
			$id = $this->landingburger_model->insert(
				array(
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'dni' => $this->input->post('dni'),
					'date_birth' => $this->input->post('year_bith').'-'.$this->input->post('month_bith').'-'.$this->input->post('day_bith'),
					'city' => $this->input->post('city'),
					'place' => $place,
					'promo' => $this->input->post('promo'),
					'date_register' => date('Y-m-d'),
					'user_agent' => $_SERVER['HTTP_USER_AGENT']
				)
			);
			$code = str_pad($id, 5, "0", STR_PAD_LEFT);
			$this->landingburger_model->update(array('code' => $code), $id);

			$this->fpdf = new FPDF('L', 'mm', array(264.58, 141.02));
			$this->fpdf->SetMargins(0, 0 , 0); 
			$this->fpdf->Image($this->config->item('base_url').'assets/img/background_cupon.jpg');
			$this->fpdf->SetAutoPageBreak(0);
			$this->fpdf->SetFont('Arial','B',14);
			$this->fpdf->SetTextColor(255, 255, 255);
			$this->fpdf->SetXY(120, 5);
			$this->fpdf->Cell(50, 10, $this->input->post('name'));
			$this->fpdf->SetXY(120, 12);
			$this->fpdf->Cell(50, 10, 'DNI: '.$this->input->post('dni'));
			$this->fpdf->SetTextColor(254, 0, 0);
			$this->fpdf->SetFont('Arial','',14);
			$this->fpdf->SetXY(-20, -40);
			$this->fpdf->Cell(0, 0, $code);
			$this->fpdf->SetTextColor(255, 255, 255);
			$this->fpdf->SetFont('Arial','',7);
			$this->fpdf->SetXY(120, 105);
			$this->fpdf->Cell(0, 2, 'Descuento máximo de S/.100 por mesa.');
			$this->fpdf->SetXY(120, 109);
			$this->fpdf->Cell(0, 2, 'Es indispensable presentar este cupón para poder acceder al descuento de 25% en Stackhouse Burgers & Shakes.');
			$this->fpdf->SetXY(120, 113);
			$this->fpdf->Cell(0, 2, 'Descuento válido únicamente para las Burgers: Triple Bacon Stackhouse Burger, Italian Stackhouse Burger, Buffalo Chicken,');
			$this->fpdf->SetXY(120, 117);
			$this->fpdf->Cell(0, 2, 'Stackhouse Sandwich y los Shakes: Banana Cream Pie Shake, Grasshopper Pie Shake y Chocolate Silk Jack Daniels Shake');
			$this->fpdf->SetXY(120, 121);
			$this->fpdf->Cell(0, 2, 'El descuento es válido en las tiendas de Lima, Arequipa y Trujillo.');
			$this->fpdf->SetXY(120, 125);
			$this->fpdf->Cell(0, 2, 'Descuento no válido para otros platos de la carta.');
			$this->fpdf->SetXY(120, 129);
			$this->fpdf->Cell(0, 2, 'Válido solo un cupón de descuento por inscripción.');
			$this->fpdf->SetXY(120, 133);
			$this->fpdf->Cell(0, 2, 'Promoción no acumulable con otras promociones y descuentos.');
			$this->fpdf->SetXY(120, 137);
			$this->fpdf->Cell(0, 2, 'Válido del 20/05/2015 al 22/06/2015.');
			$this->fpdf->Output('./uploads/pdf/cupon_'.$code.'.pdf', 'F');

			$mandrill_ready = NULL;
			try {
				$this->mandrill->init( $this->config->item('mandrill_api_key') );
				$mandrill_ready = TRUE;
			} catch(Mandrill_Exception $e) {
				$mandrill_ready = FALSE;
				var_dump($e); exit;
			}
			if ($mandrill_ready) {
				
				$msg = "Ven a Fridays y disfruta este exclusivo descuento en nuestras Burgers & Shakes. ¡Te esperamos!";
				$email = array(
					'html' => $msg, 
					'subject' => 'Cupón Burgers & Shakes',
					'from_email' => 'noreply@fridaysperu.com',
					'from_name' => 'Fridays Peru',
					'to' => array(array('email' => $this->input->post('email'))),
					'attachments' => array (
						array(
                			'type' => 'application/pdf',
                			'name' => 'cupon.pdf',
                			'content' => base64_encode(file_get_contents(FCPATH.'uploads/pdf/cupon_'.$code.'.pdf'))
                		)
            		)
				);
				$result = $this->mandrill->messages_send($email);
			}

			redirect('/landing/burgersuccess/'.$placeUrl);
		}
		$views['placeUrl'] = $placeUrl;
		$this->load->view('landing_burger', $views);
	}

	public function pdf()
	{
		$this->load->library('fpdf');
		$this->fpdf = new FPDF('L', 'mm', array(264.58, 141.02));
		$this->fpdf->SetMargins(0, 0 , 0); 
		$this->fpdf->Image($this->config->item('base_url').'assets/img/background_cupon.jpg');
		$this->fpdf->SetFont('Arial','B',14);
		$this->fpdf->SetTextColor(255, 255, 255);
		$this->fpdf->SetXY(120, 5);
		$this->fpdf->Cell(50, 10, "Name");
		$this->fpdf->SetXY(120, 12);
		$this->fpdf->Cell(50, 10, 'DNI: ');
		$this->fpdf->SetTextColor(254, 0, 0);
		$this->fpdf->SetFont('Arial','',14);
		$this->fpdf->SetXY(-20, -40);
		$this->fpdf->Cell(0, 0, "00005");
		$this->fpdf->SetTextColor(255, 255, 255);
		$this->fpdf->SetFont('Arial','',7);
		$this->fpdf->SetXY(120, 105);
		$this->fpdf->Cell(0, 2, 'Descuento máximo de S/.100 por mesa.');
		$this->fpdf->SetXY(120, 109);
		$this->fpdf->Cell(0, 2, 'Es indispensable presentar este cupón para poder acceder al descuento de 25% en Stackhouse Burgers & Shakes.');
		$this->fpdf->SetXY(120, 113);
		$this->fpdf->Cell(0, 2, 'Descuento válido únicamente para las Burgers: Triple Bacon Stackhouse Burger, Italian Stackhouse Burger, Buffalo Chicken,');
		$this->fpdf->SetXY(120, 117);
		$this->fpdf->Cell(0, 2, 'Stackhouse Sandwich y los Shakes: Banana Cream Pie Shake, Grasshopper Pie Shake y Chocolate Silk Jack Daniels Shake');
		$this->fpdf->SetXY(120, 121);
		$this->fpdf->Cell(0, 2, 'El descuento es válido en las tiendas de Lima, Arequipa y Trujillo.');
		$this->fpdf->SetXY(120, 125);
		$this->fpdf->Cell(0, 2, 'Descuento no válido para otros platos de la carta.');
		$this->fpdf->SetXY(120, 129);
		$this->fpdf->Cell(0, 2, 'Válido solo un cupón de descuento por inscripción.');
		$this->fpdf->SetXY(120, 133);
		$this->fpdf->Cell(0, 2, 'Promoción no acumulable con otras promociones y descuentos.');
		$this->fpdf->SetXY(120, 137);
		$this->fpdf->Cell(0, 2, 'Válido del 20/05/2015 al 22/06/2015.');
		$this->fpdf->Output();
	}

	public function burgersuccess($place)
	{
		$views['place'] = $place;
		$views['placeUrl'] = $place;
		$this->load->view('landing_burger_success', $views);
	}

	public function validatorburger()
	{
		$field = $_GET['field'];
		$value = $_POST[$field];
		$this->load->model('landingburger_model');
		$userLanding = $this->landingburger_model->findByField($field, $value);
		if ($userLanding) {
			$result = array('valid' => false, 'message' => 'Ya registrado');
		} else {
			$result = array('valid' => true);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function promo()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		
		$email = $this->input->get_post('email', TRUE);
		if ($email) {
			$this->load->model('promo_email_model');
			$evalueUser = $this->promo_email_model->getDataByEmail($email);
			if (!$evalueUser) {
				$this->promo_email_model->insert(
					array(
						'email' => $email, 
						'confirm' => 'si', 
						'user_agent' => $_SERVER['HTTP_USER_AGENT'], 
						'date_register' => date('Y-m-d H:i:s')
					)
				);
			}
		}
		
		$views['content_view'] = 'landing_promo';
		$views['section'] = 'reservas';
		$this->load->view('template', $views);
	}
}
