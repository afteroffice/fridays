<?php
class Login extends CI_Controller {
	
	public function index()
	{
		if ($this->session->userdata('user') !== false) {
			redirect('admin/dashboard', 'refresh');
		}
		if ($this->input->post()) {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_login');
			if ($this->form_validation->run() == true) {
				redirect('admin/dashboard', 'refresh');
			}

		}
		$this->load->view('admin/login');
	}
	
	public function logout()
	{
		$this->session->set_userdata('user', false);
		//$this->session->unset_userdata('user');
		//session_destroy();
		redirect('admin/login', 'refresh');
	}

	
	public function check_login($password)
	{
		$username = $this->input->post('email');
		$result = $this->user->login($username, $password);
		if ($result) {
			$this->session->set_userdata('user', $result);
			return true;
		} else {
			$this->form_validation->set_message('check_login', 'Email o password invalido');
			return false;
		}
	}
}
