<section>
	<div class="container bg-transp clearfix">
    <div class="cont-suscribete">
      <div class="susc-promo">
	<span>Suscríbete y entérate de las últimas promociones y novedades de Fridays</span>
      </div>
      <div class="susc-form">

<!-- Begin MailChimp Signup Form -->
<link href="http://cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
/*
	#mc_embed_signup{background:transparent; clear:left; font:14px Helvetica,Arial,sans-serif; }
	#mc-field-group, #mc_embed_signup .mc-field-group{padding-bottom: 1%;}
	.mc-field-group{
		display: inline-block;
		width: 48%;
		margin: 10px 0;
	}
	#mc_embed_signup {float: none; padding: 0 20%; width: 60%; display: inline-block;}
	#mc_embed_signup form{padding: 10px 0}
	#textContent{width:900px; height:auto; border:0; outline:none;}
	#content.textContent{background-position: bottom}
	#mc_embed_signup .mc-field-group .asterisk{right: -16px}
	#mc_embed_signup .size1of2{width: 30%}
	#mc_embed_signup .mc-field-group select{padding: 5px;}
	#mc_embed_signup .button{background-color: #C40417;}
	#mc_embed_signup .button:hover{background-color: #E1061C;}
	#mc_embed_signup .indicates-required{float: right;}
	#mc_embed_signup .indicates-required .asterisk{position: relative; bottom: -8px;}
	h1#titleContent{margin-bottom: 0}
	#privacy{padding-top: 12px }
	#mc_embed_signup input.mce_inline_error{border-color: #E1061C}
	#mc_embed_signup div.mce_inline_error{background-color: #E1061C}
	#mc_embed_signup div.response{width: 95%}

	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	   
<style type="text/css">
/* MailChimp Form Embed Code - Classic - 08/17/2011 */
#mc_embed_signup form {display:block; position:relative; text-align:left; padding:10px 0 10px 3%}
#mc_embed_signup h2 {font-weight:bold; padding:0; margin:15px 0; font-size:1.4em;}
#mc_embed_signup input {border:none; -webkit-appearance:none;}
#mc_embed_signup input[type=checkbox]{-webkit-appearance:checkbox;}
#mc_embed_signup input[type=radio]{-webkit-appearance:radio;}
#mc_embed_signup input:focus {border-color:#333;}
#mc_embed_signup .button {
	
	/*clear:both; background-color: #aaa; border: 0 none; border-radius:4px; color: #FFFFFF; cursor: pointer; display: inline-block; font-size:15px; font-weight: bold; height: 32px; line-height: 32px; margin: 0 5px 10px 0; padding: 0 22px; text-align: center; text-decoration: none; vertical-align: top; white-space: nowrap; width: auto;
	*/
	float:right;
	font-family:csm-font;
	height:45px;
	line-height:33px;
	text-transform:uppercase;
	margin:20px 0 0 20px;
	padding:5px 50px;
	font-size:18px;
	text-align:center;
	color:#FFF;
	background:#CF0907;
	border-radius:0;
	
	}
#mc_embed_signup .button:hover {/*background-color:#777;*/}
#mc_embed_signup .small-meta {font-size: 11px;}
#mc_embed_signup .nowrap {white-space:nowrap;}

#mc_embed_signup .mc-field-group {clear:left; position:relative; width:96%; padding-bottom:3%; min-height:50px;}
#mc_embed_signup .size1of2 {clear:none; float:left; display:inline-block; width:46%; margin-right:4%;}
* html #mc_embed_signup .size1of2 {margin-right:2%; /* Fix for IE6 double margins. */}
#mc_embed_signup .mc-field-group label {display:block; margin-bottom:3px;}
#mc_embed_signup .mc-field-group input {display:block; width:100%; padding:8px 0; text-indent:2%;}
#mc_embed_signup .mc-field-group select {-webkit-appearance: none; -moz-appearance: none; appearance: none;display:inline-block; width:99%; padding:5px 0; margin-bottom:2px; background:rgba(230,0,4,0.2) url(../images/bg-select.png) center right no-repeat; border:none; color:#FFF; font-size:16px;}
#mc_embed_signup .mc-other {color:black}

#mc_embed_signup .datefield, #mc_embed_signup .phonefield-us{padding:5px 0;}
#mc_embed_signup .datefield input, #mc_embed_signup .phonefield-us input{display:inline; width:60px; margin:0 2px; letter-spacing:1px; text-align:center; padding:5px 0 2px 0;}
#mc_embed_signup .phonefield-us .phonearea input, #mc_embed_signup .phonefield-us .phonedetail1 input{width:40px;}
#mc_embed_signup .datefield .monthfield input, #mc_embed_signup .datefield .dayfield input{width:30px;}
#mc_embed_signup .datefield label, #mc_embed_signup .phonefield-us label{display:none;}

#mc_embed_signup .indicates-required {text-align:right; font-size:11px; margin-right:4%;}
#mc_embed_signup .asterisk {position:relative; color:#c60; font-size:200%; top:8px;}
#mc_embed_signup .mc-field-group .asterisk {position:absolute; top:40px; right:-20px;}        
#mc_embed_signup .clear {clear:both;}

#mc_embed_signup .mc-field-group.input-group ul {margin:0; padding:5px 0; list-style:none;}
#mc_embed_signup .mc-field-group.input-group ul li {display:block; float:left; padding:3px 8px; margin:0;}
#mc_embed_signup .mc-field-group.input-group label {display:inline;}
#mc_embed_signup .mc-field-group.input-group input {display:inline; width:auto; border:none;}

#mc_embed_signup div#mce-responses {float:left; top:-1.4em; padding:0em .5em 0em .5em; overflow:hidden; width:100%; clear: both;}
#mc_embed_signup div.response {margin:1em 0; padding:1em .5em .5em 0; font-weight:bold; float:left; top:-1.5em; z-index:1; width:80%;}
#mc_embed_signup #mce-error-response {display:none;}
#mc_embed_signup #mce-error-response a {color:white;}
#mc_embed_signup #mce-success-response {color:#529214; display:none;}
#mc_embed_signup label.error {display:block; float:none; width:auto; margin-left:1.05em; text-align:left; padding:.5em 0;}

#mc-embedded-subscribe {clear:both; width:auto; display:block; margin:1em 0 1em 5%;}
#mc_embed_signup #num-subscribers {font-size:1.1em;}
#mc_embed_signup #num-subscribers span {padding:.5em; border:1px solid #ccc; margin-right:.5em; font-weight:bold;}

	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	   
@media only screen and (max-width: 479px) { /*320*/
	#mc_embed_signup .size1of2 {width:100%;} 
}
</style>
<div id="mc_embed_signup">
<form action="http://fridaysperu.us5.list-manage.com/subscribe/post?u=1afc9d75153efb95fafb40f3a&amp;id=5add08e3ff" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">

<div class="indicates-required"><span class="asterisk">*</span> dato requerido</div>
<div class="mc-field-group size1of2">
	<label for="mce-NOMBRES">Nombres <span class="asterisk">*</span></label>
	<input type="text" value="" name="NOMBRES" class="required" id="mce-NOMBRES">
</div>
<div class="mc-field-group size1of2">
	<label for="mce-APELLIDOS">Apellidos  <span class="asterisk">*</span></label>
	<input type="text" value="" name="APELLIDOS" class="required" id="mce-APELLIDOS">
</div>
<div class="mc-field-group size1of2">
	<label for="mce-EMAIL">Email  <span class="asterisk">*</span></label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group size1of2">
	<label for="mce-DNI">DNI  <span class="asterisk">*</span></label>
	<input type="text" value="" name="DNI" class="required onlyNumber" id="mce-DNI" maxlength="8">
</div>
<div class="mc-field-group size1of2">
	<label for="mce-CELULAR">Celular <span class="asterisk">*</span></label>
	<input type="text" name="CELULAR" class="required onlyNumber" value="" id="mce-CELULAR" maxlength="9">
</div>
<div class="mc-field-group size1of2">
	<label for="mce-FECNAC-month">Fecha de Nacimiento  <span class="asterisk">*</span></label>
	<div class="datefield">
		<span class="subfield dayfield"><input type="text" onFocus="if(this.value == 'DD') { this.value = ''; }"  pattern="[0-9]*" value="DD" size="2" maxlength="2" name="FECNAC[day]" id="mce-FECNAC-day" class="date-input onlyNumber"></span>
		<span class="subfield monthfield"><input type="text" onFocus="if(this.value == 'MM') { this.value = ''; }"  pattern="[0-9]*" value="MM" size="2" maxlength="2" name="FECNAC[month]" id="mce-FECNAC-month" class="date-input onlyNumber"></span>
		<span class="subfield yearfield"><input type="text" onFocus="if(this.value == 'AAAA') { this.value = ''; }"  pattern="[0-9]*" value="AAAA" size="4" maxlength="4" name="FECNAC[year]" id="mce-FECNAC-year" class="date-input onlyNumber"></span>
		<div class="fake-date"><input type="hidden" name="FECNAC" class="" id="FECNAC-fake-date" value=""></div>
	</div>
</div>
<div class="mc-field-group">
	<label for="mce-DPTO">Departamento / Ciudad  <span class="asterisk">*</span></label>
	<select name="DPTO" class="required" id="mce-DPTO" onChange="annconf();">
    	<option class="mc-other" value=""></option>
    	<option class="mc-other" value="Lima">Lima</option>
        <option class="mc-other" value="Arequipa">Arequipa</option>
        <option class="mc-other" value="Trujillo">Trujillo</option>
	</select>
</div>
<div class="mc-field-group input-group">
    <strong>Quiero recibir información de: </strong>
    <ul><li><input type="checkbox" value="1" name="group[1][1]" id="mce-group[1]-1-0"><label for="mce-group[1]-1-0">Deportes</label></li>
		<li><input type="checkbox" value="2" name="group[1][2]" id="mce-group[1]-1-1"><label for="mce-group[1]-1-1">Promociones</label></li>
		<li><input type="checkbox" value="4" name="group[1][4]" id="mce-group[1]-1-2"><label for="mce-group[1]-1-2">Lanzamientos</label></li>
		<li><input type="checkbox" value="8" name="group[1][8]" id="mce-group[1]-1-3"><label for="mce-group[1]-1-3">Eventos</label></li>
	</ul>
</div>
<div class="clear"></div>
<div id="mce-responses">
	<div class="response" id="mce-error-response" style="display:none"></div>
	<div class="response" id="mce-success-response" style="display:none"></div>
</div>	
<div class="clear"></div>
<input type="submit" value="Suscribirme" name="subscribe" id="mc-embedded-subscribe" class="button">
   
</form>
</div>

      </div>
      <div class="clear"></div>
      <p>Pol&iacute;tica de privacidad</p>
      <p>Fridays valora y respeta tu privacidad, por eso te garantizamos que la informaci&oacute;n que nos proporciones ser&aacute; estrictamente confidencial. Es nuestra pol&iacute;tica no vender, alquilar, distribuir o proporcionar tu informaci&oacute;n personal a terceros. La informaci&oacute;n que proporciones ser&aacute; utilizada para conocer tus preferencias, servirte mejor y hacerte llegar novedades sobre nuestro men&uacute;, promociones especiales y servicios.</p>
    </div>
  </div>
</section>
