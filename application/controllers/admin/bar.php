<?php
class Bar extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('barcategory_model');
		$this->load->model('baritem_model');
		$this->load->library('upload');
	}
	
	public function index($city)
	{
		$contents = $this->barcategory_model->getAll(array('status != ' => 'delete', 'city' => $city), true);
		$views['city'] = $city;
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/bar_list';
		$this->load->view('admin/template', $views);
	}
	
	public function add($city)
	{
		if ($this->input->post() && $this->_validate_submit_category()) {
			$idCategoryBar = $this->barcategory_model->insert(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'city' => $city,
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords'),
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->barcategory_model->update(
						array(
							'image' => $newName
						),
						$idCategoryBar
					);
				}
			}
			redirect('/admin/bar/index/'.$city);
		}
		$views['city'] = $city;
		$views['content_view'] = 'admin/bar_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		$dataBarCategory = $this->barcategory_model->find($id);
		if ($this->input->post() && $this->_validate_submit_category()) {
			$this->barcategory_model->update(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords')
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->barcategory_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/bar/index/'.$dataBarCategory->city);
		}
		$views['city'] = $dataBarCategory->city;
		$views['content'] = $dataBarCategory;
		$views['content_view'] = 'admin/bar_add';
		$this->load->view('admin/template', $views);
	}
	
	public function delete($id)
	{
		$dataBarCategory = $this->barcategory_model->find($id);
		$this->barcategory_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/bar/index/'.$dataBarCategory->city);
	}

	public function enable($id)
	{
		$dataBarCategory = $this->barcategory_model->find($id);
		$this->barcategory_model->update(
			array(
				'status' => 'active',
			),
			$id
		);
		redirect('/admin/bar/index/'.$dataBarCategory->city);
	}
	
	public function disable($id)
	{
		$dataBarCategory = $this->barcategory_model->find($id);
		$this->barcategory_model->update(
			array(
				'status' => 'inactive',
			),
			$id
		);
		redirect('/admin/bar/index/'.$dataBarCategory->city);
	}
	
	public function duplicate($id)
	{
		$dataBarCategory = $this->barcategory_model->find($id);
		$idCategory = $this->barcategory_model->insert(
			array(
				'title' => $dataBarCategory->title,
				'subtitle' => $dataBarCategory->subtitle,
				'content' => $dataBarCategory->content,
				'slug' => '',
				'city' => $dataBarCategory->city,
				'date_register' => date('Y-m-d H:i:s')
			)
		);
		redirect('/admin/bar/index/'.$dataBarCategory->city);
	}

	public function order()
	{
		$ids = $this->input->post('id');
		foreach ($ids as $k => $id) {
			$this->barcategory_model->update(
				array('order' => $k),
				$id
			);
		}
	}
	
	public function item($category)
	{
		$contents = $this->baritem_model->getAll(array('status != ' => 'delete', 'category' => $category), true);
		$dataBarCategory = $this->barcategory_model->find($category);
		$views['category'] = $dataBarCategory;
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/bar_item_list';
		$this->load->view('admin/template', $views);
	}
	
	public function additem($category)
	{
		if ($this->input->post() && $this->_validate_submit_item()) {
			$idItemBar = $this->baritem_model->insert(
				array(
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'iframe' => $this->input->post('iframe'),
					'category' => $category,
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->baritem_model->update(
						array(
							'image' => $newName
						),
						$idItemBar
					);
				}
			}
			if ($_FILES['image_2']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_2')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image_2' => $newName
						),
						$idItemBar
					);
				}
			}
			redirect('/admin/bar/item/'.$category);
		}
		$dataBarCategory = $this->barcategory_model->find($category);
		$views['category'] = $dataBarCategory;
		$views['content_view'] = 'admin/bar_item_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edititem($id)
	{
		$dataBarItem = $this->baritem_model->find($id);
		if ($this->input->post() && $this->_validate_submit_item()) {
			$this->baritem_model->update(
				array(
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'iframe' => $this->input->post('iframe')
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->baritem_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			if ($_FILES['image_2']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_2')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->baritem_model->update(
						array(
							'image_2' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/bar/item/'.$dataBarItem->category);
		}
		$dataBarCategory = $this->barcategory_model->find($dataBarItem->category);
		$views['category'] = $dataBarCategory;
		$views['content'] = $dataBarItem;
		$views['content_view'] = 'admin/bar_item_add';
		$this->load->view('admin/template', $views);
	}
	
	public function deleteitem($id)
	{
		$dataBarItem = $this->baritem_model->find($id);
		$this->baritem_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/bar/item/'.$dataBarItem->category);
	}

	public function enableitem($id)
	{
		$dataBarItem = $this->baritem_model->find($id);
		$this->baritem_model->update(
			array(
				'status' => 'active',
			),
			$id
		);
		redirect('/admin/bar/item/'.$dataBarItem->category);
	}
	
	public function disableitem($id)
	{
		$dataBarItem = $this->baritem_model->find($id);
		$this->baritem_model->update(
			array(
				'status' => 'inactive',
			),
			$id
		);
		redirect('/admin/bar/item/'.$dataBarItem->category);
	}
	
	public function duplicateitem($id)
	{
		$dataBarItem = $this->baritem_model->find($id);
		$idItem = $this->baritem_model->insert(
			array(
				'title' => $dataBarItem->title,
				'content' => $dataBarItem->content,
				'iframe' => $dataBarItem->iframe,
				'category' => $dataBarItem->category,
				'date_register' => date('Y-m-d H:i:s')
			)
		);
		redirect('/admin/bar/item/'.$dataBarItem->category);
	}

	public function orderitem()
	{
		$ids = $this->input->post('id');
		foreach ($ids as $k => $id) {
			$this->baritem_model->update(
				array('order' => $k),
				$id
			);
		}
	}
	
	private function _validate_submit_category()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		$this->form_validation->set_rules('slug', 'Slug','trim|required');
		return $this->form_validation->run();
	}
	
	private function _validate_submit_item()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		return $this->form_validation->run();
	}
}
