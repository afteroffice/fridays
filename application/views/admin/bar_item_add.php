<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h2><i class="icon-edit"></i>Tragos de la Categoria "<?=$category->title?>"</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<fieldset class="col-sm-12">
						<div class="form-group <?=(form_error('title') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Titulo</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'title',
											'id'          => 'title',
											'value'       => @field($content->title, set_value('title')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('content') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Contenido</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'content',
											'value'       => @field($content->content, set_value('content')),
											'rows'        => '30',
											'class'       => 'form-control cleditor'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<?php if (isset($content->image) && $content->image != "") : ?>
						<div class="form-group">
							<a href="<?=base_url() ?>uploads/images/<?=$content->image ?>" target="_blank">
								<img src="<?=base_url() ?>uploads/images/<?=$content->image ?>" width="240px" />
							</a>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label class="control-label" >Imagen (210px x 329px)</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'image',
											'id'          => 'image',
										);
										echo form_upload($data);
									?>
								</div>
							</div>
						</div>
						<?php if (isset($content->image_2) && $content->image_2 != "") : ?>
						<div class="form-group">
							<a href="<?=base_url() ?>uploads/images/<?=$content->image_2 ?>" target="_blank">
								<img src="<?=base_url() ?>uploads/images/<?=$content->image_2 ?>" width="240px" />
							</a>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label class="control-label" >Imagen (210px x 120px)</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'image_2',
											'id'          => 'image_2',
										);
										echo form_upload($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('iframe') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Iframe Like</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'iframe',
											'value'       => @field($content->iframe, set_value('iframe')),
											'rows'        => '4',
											'class'       => 'form-control'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Guardar</button>
							<a href="<?=base_url() ?>admin/bar/item/<?=$category->id ?>" type="submit" class="btn btn-danger">Regresar</a>
						</div>
					</fieldset>
				</form>   
			</div>
		</div>
	</div>
</div>
