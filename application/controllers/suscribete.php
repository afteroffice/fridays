<?php
class Suscribete extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'suscribete';
		$views['section'] = 'suscribete';
		$this->load->view('template', $views);
	}
	
	public function gracias()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'suscribete_gracias';
		$views['section'] = 'suscribete';
		$this->load->view('template', $views);
	}
	
	public function confirmacion()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'suscribete_confirmacion';
		$views['section'] = 'suscribete';
		$this->load->view('template', $views);
	}
	public function confirmar()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'suscribete_confirmar';
		$views['section'] = 'suscribete';
		$this->load->view('template', $views);
	}
	public function confirmado()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'suscribete_confirmado';
		$views['section'] = 'suscribete';
		$this->load->view('template', $views);
	}
}
