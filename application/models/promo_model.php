<?php
class Promo_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "promo";
	}
	
	public function getDataBySlug($slug, $city)
	{
		$this->db->select();
		$this->db->where('slug', $slug);
		$this->db->where('city', $city);
		$this->db->where('status', 'active');
		$query = $this->db->get($this->_table);
		$result = $query->result();
		if (isset($result[0])) {
			return $result[0];
		}
		return false;
	}
}
