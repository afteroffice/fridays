<!DOCTYPE html>
<html>
	<head>
		<title>Fridays - Burgers & Shakes</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/reset.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/estilos.css?v=20">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/desktop.css?v=13">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/tablet.css?v=13">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/smartphone.css?v=2">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>static/landing-burguer/css/colorbox.css">
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script type="text/javascript" src="js/respond.src.js"></script>
		<![endif]-->
		<?php 
			$ci =&get_instance();
			$ci->load->model('seo_model'); 
		?>
		<?=$ci->seo_model->getAnalytics() ?>
		<?php if ($placeUrl == 'lima') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Lima -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022703925552', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022703925552&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php elseif ($placeUrl == 'arequipa') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Arequipa -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022704127552', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022704127552&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php elseif ($placeUrl == 'trujillo') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Trujillo -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022704031152', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022704031152&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php endif; ?>
	</head>
	<body>
		<div id="foto">
			<div class="titulo">
				<h1><span class="stackhouse">Stackhouse</span>BURGERS<br> <span class="shakes">& SHAKES</h1>
				<p>Tenemos<br> lo que buscas</p>
			</div>
		</div>


		<form id="fdescuento" method="post">
			<h2>LLENA EL FORMULARIO Y OBTÉN <span class="descuento">25% DE DESCUENTO</span> <span class="burgers">EN BURGERS & SHAKES</span></h2>

			<div class="fila">
				<label>Nombres y Apellidos</label>
				<input type="text" name="name" id="r-nombres" data-validation="required" class="onlyLetter">
			</div>

			<div class="fila">
				<label>Email</label>
				<input type="email" name="email" id="r-correo" data-validation="email server" data-validation-url="/landing/validatorburger?field=email">
			</div>

			<div class="fila">
				<label>DNI</label>
				<input type="text" name="dni" id="r-dni" data-validation="required server" data-validation-url="/landing/validatorburger?field=dni" class="onlyNumber" >
			</div>

			<div class="fila">
				<label>Fecha de Nacimiento</label>
				<input type="text" name="day_bith" id="r-dia" placeholder="DD" class="fecha onlyNumber" maxlength="2" data-validation="number" data-validation-allowing="range[1;31]">
				
				<input type="text" name="month_bith" id="r-mes" placeholder="MM" class="fecha onlyNumber" maxlength="2" data-validation="number" data-validation-allowing="range[1;12]">

				<input type="text" name="year_bith" id="r-anio" placeholder="AAAA" class="fecha onlyNumber" maxlength="4" data-validation="number length" data-validation-length="4-4">
			</div>

			<div class="fila">
				<label>Departamento</label>
				<select id="r-depa" name="city">
					<option>Lima</option>
					<option>Arequipa</option>
					<option>Trujillo</option>
				</select>
			</div>

			<div id="selectPlace" class="fila">
				<label>Local más visitado</label>
				<select id="r-local" name="place">
					<option value="Asia">Asia</option>
					<option value="Jockey Plaza">Jockey Plaza</option>
					<option value="Larcomar">Larcomar</option>
					<option value="Óvalo Gutiérrez">Óvalo Gutiérrez</option>
					<option value="San Miguel">San Miguel</option>
					<option value="Salaverry">Salaverry</option>
				</select>
			</div>

			<div class="fila">
				<label for="recibir-check" id="recibir">Quiero recibir promociones de Fridays™</label>
				<input type="checkbox" id="recibir-check" name="promo" checked="checked">
			</div>

			<input type="submit" name="r-enviar" id="r-enviar" value="Enviar">

			<a href="#ventana" class="terminos-condiciones inline">TÉRMINOS Y CONDICIONES</a>
		</form>

		<!-- VENTANA TERMINOS -->
		<div style='display:none'>
			<div id="ventana">
				<h2>TÉRMINOS Y <br>CONDICIONES</h2>

				<ul class="lista-terminos">
					<li>Descuento máximo de S/.100 por mesa.</li>
					<li>Es indispensable presentar el cupón para poder acceder al descuento de 25% en Stackhouse Burgers & Shakes. </li>
					<li>Descuento válido únicamente para las Burgers: Triple Bacon Stackhouse Burger, Italian Stackhouse Burger, Buffalo Chicken   Stackhouse Sandwich y los Shakes: Banana Cream Pie Shake, Grasshopper Pie Shake y Chocolate Silk Jack Daniel’s® Shake. </li>
					<li>El descuento es válido en las tiendas de Lima, Arequipa y Trujillo.</li>
					<li>Descuento no válido para otros platos de la carta.</li>
					<li>Válido solo un cupón de descuento por inscripción.</li>
					<li>Promoción no acumulable con otras promociones y descuentos.</li>
					<li>Válido del 20/05/2015 al 22/06/2015.</li>
				</ul>
			</div><!--ventana-->
		</div><!--displaynone-->


		<!--*******-->
		<!--SCRIPTS-->
		<!--*******-->
		<script type="text/javascript" src="<?=base_url() ?>static/landing-burguer/js/modernizr.custom.13945"></script>
		<script src="<?=base_url() ?>static/landing-burguer/js/jquery-1.10.2.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>
		<script src="<?=base_url() ?>static/landing-burguer/js/jquery.colorbox.js"></script>
		<script src="<?=base_url() ?>static/landing-burguer/js/burgers.js?v=3"></script>
	</body>
</html>
