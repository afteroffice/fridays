<section>
	<div class="container bg-transp clearfix">
    <div class="cont-contacto">
    	<h1>Cont&aacute;ctanos</h1>
      <p>Agradecemos tus comentarios, sugerencias e inter&eacute;s en T.G.I. Fridays.</p>
      <p>Por favor llena el siguiente formulario y un representante de T.G.I. Fridays responder&aacute; tus preguntas o comentarios en el horario de lunes a viernes de 9 a.m. a 6 p.m.</p>
      <div id="divAlerta" class="alerta" style="display:none;">
      	<h2>Hemos detectado alg&uacute;n error, por favor revisa los datos que has introducido</h2>
        <p>Los campos <span>"Nombres"</span>, <span>"Apellidos"</span> y <span>"Escribe tus comentarios"</span> son obligatorios.<br>Para poder contactar contigo, debes ingresar un num&eacute;ro de tel&eacute;fono o una direcci&oacute;n de correo electr&oacute;nico valida.</p>
      </div>
      <form name="postContacto" id="postContacto" method="post" >
      <div class="cont-datos">
      	<h2>Ingresa tus Datos Personales</h2>
        <ul>
        	<li>Nombres:</li>
          <li><input name="strNombres" id="strNombres" type="text"></li>
        	<li>Apellidos:</li>
          <li><input name="strApellidos" id="strApellidos" type="text"></li>
        	<li>E-mail:</li>
          <li><input name="strEmail" id="strEmail" type="email"></li>
        	<li>Tel&eacute;fono:</li>
          <li><input name="strPhone" id="strPhone" type="tel"></li>
        	<li>Tipo de comentario:</li>
          <li>
          	<div class="cont-despl">
              <select name="selTipo" id="selTipo">
                    <option selected value="Otro">Seleccionar</option>
                    <option value="Consulta">Consulta</option>
                    <option value="Agradecimiento">Agradecimiento</option>
                    <option value="Queja">Queja</option>
                    <option value="Sugerencia">Sugerencia</option>
                    <option value="Otro">Otro</option>
              </select>
            </div>
          </li>
        </ul> 
      </div>
      <div class="cont-coment">
      	<h2>Escribe tus comentarios:</h2>
        <ul>
        	<li><textarea name="txtComentarios" id="txtComentarios"></textarea></li>
          <li>
            <h5>Garant&iacute;a de confidencialidad</h5>
            <p>Garant&iacute;a de confidencialidad Fridays valora y respeta tu privacidad, por eso te garantizamos que cualquier informaci&oacute;n que nos proporciones ser&aacute; estrictamente confidencial. Es nuestra pol&iacute;tica no vender, alquilar, distribuir o proporcionar cualquier informaci&oacute;n personal a terceros.</p>
          </li>
        </ul>
        <a href="javascript:BorrarForm();" title="Borrar los campos del formulario">Borrar</a>
        <a href="javascript:ValidaForm();" title="Enviar el formulario con la información de contacto">Enviar</a>
      </div>
      </form>
      <div class="clear"></div>
    </div>
  </div>
</section>
