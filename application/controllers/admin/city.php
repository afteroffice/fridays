<?php
class City extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('city_model');
		$this->load->library('upload');
	}
	
	public function index()
	{
		$contents = $this->city_model->getAll(array('status' => 'active'));
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/city_list';
		$this->load->view('admin/template', $views);
	}
	
	public function add()
	{
		if ($this->input->post() && $this->_validate_submit()) {
			$idSlider = $this->city_model->insert(
				array(
					'title' => $this->input->post('title'),
					'slug' => $this->input->post('slug'),
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			redirect('/admin/city');
		}
		$views['content_view'] = 'admin/city_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		if ($this->input->post() && $this->_validate_submit()) {
			$this->city_model->update(
				array(
					'title' => $this->input->post('title'),
					'slug' => $this->input->post('slug'),
				),
				$id
			);
			redirect('/admin/city');
		}
		$dataCity = $this->city_model->find($id);
		$views['content'] = $dataCity;
		$views['content_view'] = 'admin/city_add';
		$this->load->view('admin/template', $views);
	}
	
	public function delete($id)
	{
		$this->city_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/city');
	}
	
	private function _validate_submit()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		$this->form_validation->set_rules('slug', 'Slug','trim|required');
		return $this->form_validation->run();
	}
}
