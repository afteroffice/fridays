<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h2><i class="icon-edit"></i>Categoria de Comida</h2>
			</div>
			<div class="box-content">
				<form class="form-horizontal" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<fieldset class="col-sm-12">
						<div class="form-group <?=(form_error('title') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Titulo</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'title',
											'id'          => 'title',
											'value'       => @field($content->title, set_value('title')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('subtitle') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Sub Titulo</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'subtitle',
											'id'          => 'subtitle',
											'value'       => @field($content->subtitle, set_value('subtitle')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('content') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Contenido</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'content',
											'value'       => @field($content->content, set_value('content')),
											'rows'        => '30',
											'class'       => 'form-control cleditor'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<?php if (isset($content->image) && $content->image != "") : ?>
						<div class="form-group">
							<a href="<?=base_url() ?>uploads/images/<?=$content->image ?>" target="_blank">
								<img src="<?=base_url() ?>uploads/images/<?=$content->image ?>" width="240px" />
							</a>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label class="control-label" >Imagen (1100px x 344px)</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'image',
											'id'          => 'image',
										);
										echo form_upload($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('slug') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Slug</label>
							<div class="controls row">
								<div class="input-group col-sm-4">
									<?php
										$data = array(
											'name'        => 'slug',
											'id'          => 'slug',
											'value'       => @field($content->slug, set_value('slug')),
											'class'       => 'form-control',
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('convertion_google') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Code Convertion Google</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'convertion_google',
											'value'       => @field($content->convertion_google, set_value('convertion_google')),
											'rows'        => '4',
											'class'       => 'form-control'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('convertion_facebook') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Code Convertion Facebook</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'convertion_facebook',
											'value'       => @field($content->convertion_facebook, set_value('convertion_facebook')),
											'rows'        => '4',
											'class'       => 'form-control'
										);
										echo form_textarea($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('tag_title') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Tag Title</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'tag_title',
											'value'       => @field($content->tag_title, set_value('tag_title')),
											'class'       => 'form-control'
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('tag_description') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Tag Description</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'tag_description',
											'value'       => @field($content->tag_description, set_value('tag_description')),
											'class'       => 'form-control'
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-group <?=(form_error('tag_keywords') != '') ? "has-error" : "" ?>">
							<label class="control-label" >Tag Keywords</label>
							<div class="controls">
								<div class="input-group col-sm-12">
									<?php
										$data = array(
											'name'        => 'tag_keywords',
											'value'       => @field($content->tag_keywords, set_value('tag_keywords')),
											'class'       => 'form-control'
										);
										echo form_input($data);
									?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Guardar</button>
							<a href="<?=base_url() ?>admin/food/index/<?=$city ?>" type="submit" class="btn btn-danger">Regresar</a>
						</div>
					</fieldset>
				</form>   
			</div>
		</div>
	</div>
</div>
