<!DOCtYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Fridays</title>

<style type="text/css">
body{
	background: #ffffff; margin: 0; padding: 0; min-width: 100%!important;
}
td, p{
	margin: 0;
}
.content{
	width: 100%; max-width: 600px;
} 
</style>
</head>

<body>
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
	<table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table class="content" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0 auto; font-family: Helvetica, Arial, sans-serif; background:#FFFFFF;">
					<tr>
						<th>
							<img src="<?=base_url()?>static/images/mailing/img/cabecera-nodisponible.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
						</th>
					</tr>

					<tr style="display:block; padding:20px 0 20px 0; ">
						<td>
							<p style="font-weight:bold; color:#000000;"><span>Por el momento no contamos con disponibilidad en el local seleccionado.</span></p>
							<p style="font-weight:bold; color:#000000;"><span>Prueba eligiendo otro de nuestros locales.</span></p>
						</td>
					</tr>

					<tr>
						<td>
							<a href="<?=base_url()?>reservas" style="width:180px; height:50px; display:block; margin:0 auto; "><img src="<?=base_url()?>static/images/mailing/img/btn-reserva.jpg" alt="reservas" style="widht:100%; display:block; margin:0 auto; border:0;"></a>
						</td>
					</tr>


					<tr>
						<td>
							<img src="<?=base_url()?>static/images/mailing/img/pie-reserva-nodisponible.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</body>
</html>
