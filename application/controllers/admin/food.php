<?php
class Food extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('foodcategory_model');
		$this->load->model('fooditem_model');
		$this->load->library('upload');
	}
	
	public function index($city)
	{
		$contents = $this->foodcategory_model->getAll(array('status !=' => 'delete', 'city' => $city), true);
		$views['city'] = $city;
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/food_list';
		$this->load->view('admin/template', $views);
	}
	
	public function add($city)
	{
		if ($this->input->post() && $this->_validate_submit_category()) {
			$idCategoryFood = $this->foodcategory_model->insert(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'city' => $city,
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords'),
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->foodcategory_model->update(
						array(
							'image' => $newName
						),
						$idCategoryFood
					);
				}
			}
			redirect('/admin/food/index/'.$city);
		}
		$views['city'] = $city;
		$views['content_view'] = 'admin/food_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		$dataFoodCategory = $this->foodcategory_model->find($id);
		if ($this->input->post() && $this->_validate_submit_category()) {
			$this->foodcategory_model->update(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
					'slug' => $this->input->post('slug'),
					'convertion_google' => $this->input->post('convertion_google'),
					'convertion_facebook' => $this->input->post('convertion_facebook'),
					'tag_title' => $this->input->post('tag_title'),
					'tag_description' => $this->input->post('tag_description'),
					'tag_keywords' => $this->input->post('tag_keywords')
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->foodcategory_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/food/index/'.$dataFoodCategory->city);
		}
		$views['city'] = $dataFoodCategory->city;
		$views['content'] = $dataFoodCategory;
		$views['content_view'] = 'admin/food_add';
		$this->load->view('admin/template', $views);
	}
	
	public function delete($id)
	{
		$dataFoodCategory = $this->foodcategory_model->find($id);
		$this->foodcategory_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/food/index/'.$dataFoodCategory->city);
	}

	public function enable($id)
	{
		$dataFoodCategory = $this->foodcategory_model->find($id);
		$this->foodcategory_model->update(
			array(
				'status' => 'active',
			),
			$id
		);
		redirect('/admin/food/index/'.$dataFoodCategory->city);
	}
	
	public function disable($id)
	{
		$dataFoodCategory = $this->foodcategory_model->find($id);
		$this->foodcategory_model->update(
			array(
				'status' => 'inactive',
			),
			$id
		);
		redirect('/admin/food/index/'.$dataFoodCategory->city);
	}
	
	public function duplicate($id)
	{
		$dataFoodCategory = $this->foodcategory_model->find($id);
		$idCategory = $this->foodcategory_model->insert(
			array(
				'title' => $dataFoodCategory->title,
				'subtitle' => $dataFoodCategory->subtitle,
				'content' => $dataFoodCategory->content,
				'slug' => "",
				'city' => $dataFoodCategory->city,
				'order' => 100,
				'date_register' => date('Y-m-d H:i:s')
			)
		);
		redirect('/admin/food/index/'.$dataFoodCategory->city);
	}

	public function order()
	{
		$ids = $this->input->post('id');
		foreach ($ids as $k => $id) {
			$this->foodcategory_model->update(
				array('order' => $k),
				$id
			);
		}
	}
	
	public function item($category)
	{
		$contents = $this->fooditem_model->getAll(array('status != ' => 'delete', 'category' => $category), true);
		$dataFoodCategory = $this->foodcategory_model->find($category);
		$views['category'] = $dataFoodCategory;
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/food_item_list';
		$this->load->view('admin/template', $views);
	}
	
	public function additem($category)
	{
		if ($this->input->post() && $this->_validate_submit_item()) {
			$idItemFood = $this->fooditem_model->insert(
				array(
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'iframe' => $this->input->post('iframe'),
					'category' => $category,
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image' => $newName
						),
						$idItemFood
					);
				}
			}
			if ($_FILES['image_2']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_2')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image_2' => $newName
						),
						$idItemFood
					);
				}
			}
			if ($_FILES['image_3']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_3')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image_3' => $newName
						),
						$idItemFood
					);
				}
			}
			redirect('/admin/food/item/'.$category);
		}
		$dataFoodCategory = $this->foodcategory_model->find($category);
		$views['category'] = $dataFoodCategory;
		$views['content_view'] = 'admin/food_item_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edititem($id)
	{
		$dataFoodItem = $this->fooditem_model->find($id);
		if ($this->input->post() && $this->_validate_submit_item()) {
			$this->fooditem_model->update(
				array(
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'iframe' => $this->input->post('iframe')
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			if ($_FILES['image_2']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_2')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image_2' => $newName
						),
						$id
					);
				}
			}
			if ($_FILES['image_3']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_3')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->fooditem_model->update(
						array(
							'image_3' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/food/item/'.$dataFoodItem->category);
		}
		$dataFoodCategory = $this->foodcategory_model->find($dataFoodItem->category);
		$views['category'] = $dataFoodCategory;
		$views['content'] = $dataFoodItem;
		$views['content_view'] = 'admin/food_item_add';
		$this->load->view('admin/template', $views);
	}
	
	public function deleteitem($id)
	{
		$dataFoodItem = $this->fooditem_model->find($id);
		$this->fooditem_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/food/item/'.$dataFoodItem->category);
	}

	public function enableitem($id)
	{
		$dataFoodItem = $this->fooditem_model->find($id);
		$this->fooditem_model->update(
			array(
				'status' => 'active',
			),
			$id
		);
		redirect('/admin/food/item/'.$dataFoodItem->category);
	}
	
	public function disableitem($id)
	{
		$dataFoodItem = $this->fooditem_model->find($id);
		$this->fooditem_model->update(
			array(
				'status' => 'inactive',
			),
			$id
		);
		redirect('/admin/food/item/'.$dataFoodItem->category);
	}
	
	public function duplicateitem($id)
	{
		$dataFoodItem = $this->fooditem_model->find($id);
		$idItem = $this->fooditem_model->insert(
			array(
				'title' => $dataFoodItem->title,
				'content' => $dataFoodItem->content,
				'iframe' => $dataFoodItem->iframe,
				'category' => $dataFoodItem->category,
				'order' => 100,
				'date_register' => date('Y-m-d H:i:s')
			)
		);
		redirect('/admin/food/item/'.$dataFoodItem->category);
	}

	public function orderitem()
	{
		$ids = $this->input->post('id');
		foreach ($ids as $k => $id) {
			$this->fooditem_model->update(
				array('order' => $k),
				$id
			);
		}
	}
	
	private function _validate_submit_category()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		$this->form_validation->set_rules('slug', 'Slug','trim|required');
		return $this->form_validation->run();
	}
	
	private function _validate_submit_item()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		return $this->form_validation->run();
	}
}
