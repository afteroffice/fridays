<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- start: Meta -->
		<meta charset="utf-8">
		<title>Dashboard - Ferreyros</title>
		<!-- end: Meta -->
		
		<!-- start: Mobile Specific -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- end: Mobile Specific -->
		
		<!-- start: CSS -->
		<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/style.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/retina.min.css" rel="stylesheet">
		<link href="<?=base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>
		<!-- end: CSS -->
		

		<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?=base_url() ?>assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div id="content" class="col-sm-12 full">
					<div class="row">
						<div class="login-box">
							<div class="header">
								Ingresar al Panel
							</div>
							<form class="form-horizontal login" action="" method="post">
								<fieldset class="col-sm-12">
									<div class="form-group">
										<div class="controls row">
											<div class="input-group col-sm-12">	
												<input type="text" class="form-control" id="email" name="email" placeholder="Email"/>
												<span class="input-group-addon"><i class="icon-user"></i></span>
											</div>	
										</div>
									</div>							
									<div class="form-group">
										<div class="controls row">
											<div class="input-group col-sm-12">	
												<input type="password" class="form-control" id="password" name="password" placeholder="Password"/>
												<span class="input-group-addon"><i class="icon-key"></i></span>
											</div>	
										</div>
									</div>
									<div class="row">
										<button type="submit" class="btn btn-lg btn-primary col-xs-12">Ingresar</button>
									</div>
								</fieldset>	
							</form>
							<div class="clearfix"></div>				
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- start: JavaScript-->
		<!--[if !IE]>-->
			<script src="assets/js/jquery-2.0.3.min.js"></script>
		<!--<![endif]-->

		<!--[if IE]>
			<script src="<?=base_url() ?>assets/js/jquery-1.10.2.min.js"></script>
		<![endif]-->

		<!--[if !IE]>-->
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<?=base_url() ?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
		<!--<![endif]-->

		<!--[if IE]>
			<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			
		<![endif]-->
		<script src="<?=base_url() ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?=base_url() ?>assets/js/bootstrap.min.js"></script>
		<!-- page scripts -->
		<script src="<?=base_url() ?>assets/js/jquery.icheck.min.js"></script>
		
		<!-- theme scripts -->
		<script src="<?=base_url() ?>assets/js/custom.min.js"></script>
		<script src="<?=base_url() ?>assets/js/core.min.js"></script>
		
		<!-- inline scripts related to this page -->
		<script src="<?=base_url() ?>assets/js/pages/login.js"></script>
		
		<!-- end: JavaScript-->
		
	</body>
</html>
