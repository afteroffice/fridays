
<?php

  include_once("funciones-sesiones.php");
  include_once("../../configRD.php");



function cargaInicialArequipa(){

  CompruebaArequipa();
  
  cabeceras();

}

function cargaInicialLima(){

  CompruebaLima();
  
  cabeceras();

}

function cargaInicialTrujillo(){

  CompruebaTrujillo();
  
  cabeceras();

}




function cabeceras(){

	echo '<!DOCTYPE html>'."\n";
	echo '<!--[if IE 7 ]><html class="ie ie7" lang="es"> <![endif]-->'."\n";
	echo '<!--[if IE 8 ]><html class="ie ie8" lang="es"> <![endif]-->'."\n";
	echo '<!--[if (gte IE 9)|!(IE)]><!--><html lang="es"> <!--<![endif]-->'."\n";
	
	echo '<head>'."\n";
	echo '<meta charset="windows-1252">'."\n";
	$ciudadActual = $_SESSION['nCiudad'];
  //obtener el nombre de la p�gina
  $nombrePaginaActual = $_SERVER['SCRIPT_NAME'];
  
  if ( strpos($nombrePaginaActual, '/') !== FALSE ){
      $nombrePaginaActual = array_pop(explode('/', $nombrePaginaActual));
  }

  $filaCategoria = consultar_Categoria_Pagina_Ciudad($ciudadActual, $nombrePaginaActual);

  $row = mysql_fetch_array($filaCategoria );

  $title=$row["Title"];
  $description=$row["Description"];
  $keywords=$row["Keywords"];

	echo '<title>'.$title.'</title>'."\n";
	echo '<meta name="keywords" content="'.$description.'"/>'."\n";
	echo '<meta name="description" content="'.$keywords.'"/>'."\n";

	include_once("../../includes/etiquetas-comunes.php"); 
	if($ciudadActual=="Arequipa"){
	include_once("../../includes/etiquetas-arequipa.php"); 
	}
	if($ciudadActual=="Lima"){
	include_once("../../includes/etiquetas-lima.php"); 
	}
	if($ciudadActual=="Trujillo"){
	include_once("../../includes/etiquetas-trujillo.php"); 
	}

}


function cabeceras2(){

	echo '<!-- Styles -->'."\n";
	echo '<link rel="stylesheet" type="text/css" href="../../css/style.css">'."\n";
	
	include("../../includes/estilos-ie.php"); 
	include_once("../../includes/analytics.php"); 
	
	echo '</head>'."\n";
	echo '<body>'."\n";
	include("../../includes/header.php"); 
}


function PintaCuerpo(){

  $ciudadActual = $_SESSION['nCiudad'];
  
  //obtener el nombre de la p�gina
  $nombrePaginaActual = $_SERVER['SCRIPT_NAME'];
  
  if ( strpos($nombrePaginaActual, '/') !== FALSE ){
      $nombrePaginaActual = array_pop(explode('/', $nombrePaginaActual));
  }

  $filaCategoria = consultar_Categoria_Pagina_Ciudad($ciudadActual, $nombrePaginaActual);

  $row = mysql_fetch_array($filaCategoria );

  $idCategoria=$row["idCategoria"];
  $nombreCategoria=$row["nombreCategoria"];
  $paginaCategoria=$row["paginaCategoria"];
  $titleCategoria=$row["titleCategoria"];
  $srcImgCategoria=$row["srcImgCategoria"];
  $H1Banner=$row["H1Banner"];
  $H1Banner=htmlspecialchars_decode($H1Banner, ENT_NOQUOTES);
  $H2Banner=$row["H2Banner"];
  $H2Banner=htmlspecialchars_decode($H2Banner, ENT_NOQUOTES);
  $frasePromocion=$row["frasePromocion"];
  $activo=$row["activo"];
  $orden=$row["orden"];
  
    //echo "<p>htmlentities($srcImgCategoria)</p>";
	echo '<section class="clearfix">'."\n";
	echo '<div class="container clearfix">'."\n";
	$tmp ='<div class="com-tit" style="background:url('.$srcImgCategoria;
	//$tmp.=htmlentities($srcImgCategoria);
	$tmp.=');" >'."\n";
	echo $tmp;
	//echo '<div class="com-tit" style="background:url("'.$srcImgCategoria.'")" >'."\n";
	echo '<div class="com-tit1" >'."\n";
	echo '<h1>'.$H1Banner.'</h1>'."\n";
	echo '<h2>'.$H2Banner.'</h2>'."\n";
	echo '</div>'."\n";
	echo '</div>'."\n";
	echo '</div>'."\n";
	echo '<div class="container com-promo">'."\n";
	echo '<h2>'.$frasePromocion.'</h2>'."\n";
	echo '</div>'."\n";
	echo '<div class="container">'."\n";
	echo '<div class="menu-comidas">'."\n";
	echo '<ul>'."\n";
        
		if($ciudadActual=="Arequipa"){
		    $ciudad="arequipa";
		}
		if($ciudadActual=="Lima"){
			$ciudad="lima";
		}
		if($ciudadActual=="Trujillo"){
			$ciudad="trujillo";
		}
		
        $filasCategorias = consulta_Categorias_Activas_Ciudad($ciudadActual);
        $filas = mysql_num_rows($filasCategorias);
        if($filas==0){  

        echo '<li><a href="#" >Sin Categorias.</a></li>'."\n"; 

        }else{
           while($rows = mysql_fetch_array($filasCategorias)){
              
			  $nombreCat=$rows["nombreCategoria"];
              $paginaCat=$rows["paginaCategoria"];
              $titleCat=$rows["titleCategoria"];
              
			  $extens = extension($paginaCat);
			  
             if($extens=="php"){
				  echo '<li><a href="/'.$ciudad.'/comidas/'.$paginaCat.'" title="'.$titleCat.'" ';
					  if($paginaCat==$nombrePaginaActual){ echo 'class="activo" '; } 
				  echo '>'.$nombreCat.'</a></li>'."\n"; 
			  }
            }
        }

		 echo '</ul>'."\n";
		 echo '<div class="com-select">'."\n";
		 echo '<select onchange="location.href = this.options[this.selectedIndex].value;">'."\n";

            $filasCategorias = consulta_Categorias_Activas_Ciudad($ciudadActual);
            $filas = mysql_num_rows($filasCategorias);
            if($filas==0){  
  
 		       echo '<option value="#" selected>SIN COMIDAS...</option>'."\n";
      
            }else{
    
               echo '<option value="#" selected>MENU COMIDAS...</option>'."\n";
  
               while($rows = mysql_fetch_array($filasCategorias)){
                  $nombreCat=$rows["nombreCategoria"];
                  $paginaCat=$rows["paginaCategoria"];
                   $extens = extension($paginaCat);
			  
                   if($extens=="php"){
        		       echo '<option value="'.$paginaCat.'">- '.$nombreCat.'</option>'."\n";
					   }

              }
          }

    echo '</select>'."\n";
    echo '</div>'."\n";
    echo '</div>'."\n";
    echo '<div class="comidas">'."\n";

 
              $filasDetalles = consulta_Items_Activos_Ciudad($idCategoria, $ciudadActual);
              $filasDetallessinImagen = consulta_Items_Activos_sinImagen_Ciudad($idCategoria, $ciudadActual);

              $filas = mysql_num_rows($filasDetalles);
              $filassinImagen = mysql_num_rows($filasDetallessinImagen);

              $totalfilas= $filas + $filassinImagen;
              if($totalfilas==0){

            	echo '<p>No hay comidas para mostrar.</p>'."\n";

             }else{

                echo '<ul class="com1 clearfix">'."\n";
 
				while ($row = mysql_fetch_array($filasDetalles)){
	
						$tmpImgsrc= $row["srcImg"];
						$tmpImgsrc369_170= $row["srcImg369_170"];
						$tmpImgsrc568_200= $row["srcImg568_200"];
						$tmpImgalt= $row["altImg"];
						$tmpImgclass= $row["classImg"];
						$tmpImgclass369_170= $row["classImg369_170"];
						$tmpImgclass568_200= $row["classImg568_200"];
						$tmpH2= $row["textoH2"];
						$tmpH2=htmlspecialchars_decode($tmpH2, ENT_NOQUOTES);
						$tmpP= $row["parrafo"];
						//$tmpP=str_replace("&lt;br /&gt;", "<br />", $tmpP);
						$tmpP=htmlspecialchars_decode($tmpP, ENT_NOQUOTES);
						//$tmpP=str_replace("&amp;nbsp;", "", $tmpP);
						$tmpMegusta=$row["iFrame_megusta"];

						echo '<li>'."\n"; 
						if($tmpMegusta!="" && strlen($tmpMegusta)>5){
						     echo '<div class="me-gusta1">'."\n";
						     echo htmlspecialchars_decode($tmpMegusta)."\n";
						     echo '</div>'."\n";
						  }
						echo '<div class="img-comidas">'."\n";
						echo '<img src="'.$tmpImgsrc.'" alt="'.$tmpImgalt.'" class="'.$tmpImgclass.'">'."\n";
						echo '<img src="'.$tmpImgsrc369_170.'" alt="'.$tmpImgalt.'" class="'.$tmpImgclass369_170.'">'."\n";
						echo '<img src="'.$tmpImgsrc568_200.'" alt="'.$tmpImgalt.'" class="'.$tmpImgclass568_200.'">'."\n";
						echo '</div>'."\n";
						echo '<div class="info-comidas">'."\n";
						echo '<h2>'.$tmpH2.'</h2>'."\n";
						echo '<p>'.$tmpP.'</p>'."\n";
						echo '</div>'."\n";
						echo '</li>'."\n";
                }
			 
				 while ($row = mysql_fetch_array($filasDetallessinImagen)){
	
						$tmpH2= $row["textoH2"];
						$tmpH2=htmlspecialchars_decode($tmpH2, ENT_NOQUOTES);
						$tmpP= $row["parrafo"];
						$tmpP=htmlspecialchars_decode($tmpP, ENT_NOQUOTES);
	                    $tmpMegusta=$row["iFrame_megusta"];
						
						  echo '<li>'."\n";
						  echo '<div class="info-comidas sin-img">'."\n";
						  echo '<h2>'.$tmpH2.'</h2>'."\n";
						  if($tmpMegusta!="" && strlen($tmpMegusta)>5){
						     echo '<div class="me-gusta2">'."\n";
						     echo htmlspecialchars_decode($tmpMegusta)."\n";
						     echo '</div>'."\n";
						  }
						  echo '<p>'.$tmpP.'</p>'."\n";
						  echo '</div>'."\n";
						  echo '</li>'."\n";   
				}
				
                echo '</ul>'."\n";  
             }
   echo '</div>'."\n"; 
   echo '</div>'."\n"; 
   echo '</section>'."\n"; 
   
   
include("../../includes/footer.php");

echo '</body>'."\n"; 
echo '</html>'."\n"; 
}



//funcion para obtener una categoria por el nombre de su pagian y su ciudad
function consultar_Categoria_Pagina_Ciudad($ciudadActual, $nombrePaginaActual){
    
     $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM comidasCategorias$ciudadActual WHERE paginaCategoria='$nombrePaginaActual'";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function consulta_Categorias_Activas_Ciudad($ciudadActual){
   
   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM comidasCategorias$ciudadActual WHERE activo=1  AND LENGTH(paginaCategoria) >=5";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function consulta_Items_Activos_Ciudad($idCategoria, $ciudadActual){

   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM itemComidas$ciudadActual WHERE idCategoria='$idCategoria' AND activo='1' AND LENGTH(srcImg) >=5 ORDER BY orden ASC ";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}


function consulta_Items_Activos_sinImagen_Ciudad($idCategoria, $ciudadActual){

   $conexion = mysql_connect(RD_MYSQL_SERVER,RD_USUARIO_BD,RD_CONTRA_BD);

	if(!$conexion) {
		   die('Error al intentar conectar: '.mysql_error());
	}
	//seleccionar la base de datos para trabajar
	mysql_select_db(RD_BD);
    //sentencia sql para consultar los datos de los sliders
    $querysql = "SELECT * FROM itemComidas$ciudadActual WHERE idCategoria='$idCategoria' AND activo='1' AND LENGTH(srcImg) < 5 ORDER BY orden ASC ";
	
	$result=mysql_query($querysql,$conexion);
    
	return $result;

}

function extension($str) {
		return end(explode(".", $str));
	}
?>