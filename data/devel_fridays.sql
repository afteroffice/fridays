# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: carbono.us (MySQL 5.5.40-0ubuntu0.14.04.1)
# Base de datos: devel_fridays
# Tiempo de Generación: 2014-11-07 05:38:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla bar_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bar_category`;

CREATE TABLE `bar_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `city` int(11) NOT NULL,
  `convertion_google` text,
  `convertion_facebook` text,
  `tag_title` varchar(200) DEFAULT NULL,
  `tag_description` varchar(200) DEFAULT NULL,
  `tag_keywords` varchar(200) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bar_category` WRITE;
/*!40000 ALTER TABLE `bar_category` DISABLE KEYS */;

INSERT INTO `bar_category` (`id`, `title`, `subtitle`, `content`, `image`, `slug`, `city`, `convertion_google`, `convertion_facebook`, `tag_title`, `tag_description`, `tag_keywords`, `order`, `status`, `date_register`)
VALUES
	(1,'Ultimates','Ultimates','Piensa en grande, disfruta en grande','image_content_141360341696.jpg','ultimates',1,NULL,NULL,NULL,NULL,NULL,2,'active','2014-10-17 22:36:56'),
	(2,'Ultimates 2','Ultimates 2','Piensa en grande, disfruta en grande','','ultimates-2',1,'google bar','facebook bar','title','description ultimate 2','keywords ultimate 2',0,'active','2014-11-03 12:20:55'),
	(3,'Ultimates','Ultimates','Piensa en grande, disfruta en grande','','',1,NULL,NULL,NULL,NULL,NULL,1,'active','2014-11-04 10:58:31');

/*!40000 ALTER TABLE `bar_category` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla bar_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bar_item`;

CREATE TABLE `bar_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `image_2` varchar(200) NOT NULL,
  `iframe` text NOT NULL,
  `category` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bar_item` WRITE;
/*!40000 ALTER TABLE `bar_item` DISABLE KEYS */;

INSERT INTO `bar_item` (`id`, `title`, `content`, `image`, `image_2`, `iframe`, `category`, `order`, `status`, `date_register`)
VALUES
	(1,'Ultimate Long Island Iced Tea','Nuestra versión premium del ya famoso Long Island Iced Tea: vodka, Ron Cartavio Selecto<sup>®</sup>, gin, triple sec, sweet &amp; sour, brandy y un toque de Coca-Cola<sup>®</sup>. ¡Te hará volar! S/.24','image_content_14136038732.jpg','image_content_141511613069.jpg','<iframe scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\" src=\"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.fridaysperu.com%2F&layout=button_count&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=21\"></iframe>',1,0,'active','2014-10-17 22:44:33'),
	(2,'Ultimate Long Island Iced Tea 2','Nuestra versión premium del ya famoso Long Island Iced Tea: vodka, Ron Cartavio Selecto<sup>®</sup>, gin, triple sec, sweet &amp; sour, brandy y un toque de Coca-Cola<sup>®</sup>. ¡Te hará volar! S/.24','','','<iframe scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\" src=\"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.fridaysperu.com%2F&layout=button_count&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=21\"></iframe>',1,1,'active','2014-11-04 11:09:52');

/*!40000 ALTER TABLE `bar_item` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla city
# ------------------------------------------------------------

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;

INSERT INTO `city` (`id`, `title`, `slug`, `order`, `status`, `date_register`)
VALUES
	(1,'Lima','lima',0,'active','2014-10-17 21:07:21'),
	(2,'Arequipa','arequipa',0,'active','2014-10-17 21:10:45'),
	(3,'Trujillo','trujillo',0,'active','2014-10-17 21:10:52');

/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `lastname` varchar(500) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `comment` text,
  `date_register` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;

INSERT INTO `contact` (`id`, `name`, `lastname`, `email`, `phone`, `type`, `comment`, `date_register`)
VALUES
	(1,'Jesus','Fabian','jfabian.pe@gmail.com','9938487121','Consulta','Prueba del envio de consultas por el formulario de contactanos','2014-10-20 01:49:49');

/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla food_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `food_category`;

CREATE TABLE `food_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `city` int(11) NOT NULL,
  `convertion_google` text,
  `convertion_facebook` text,
  `tag_title` varchar(200) DEFAULT NULL,
  `tag_description` varchar(200) DEFAULT NULL,
  `tag_keywords` varchar(200) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `food_category` WRITE;
/*!40000 ALTER TABLE `food_category` DISABLE KEYS */;

INSERT INTO `food_category` (`id`, `title`, `subtitle`, `content`, `image`, `slug`, `city`, `convertion_google`, `convertion_facebook`, `tag_title`, `tag_description`, `tag_keywords`, `order`, `status`, `date_register`)
VALUES
	(1,'Wine & Beer','Siempre cae bien una buena compañí­a.','Solo basta con probarlos para que se vuelvan tus favoritos.	','image_content_141360117365.jpg','wine-beer',1,'google','facebook',NULL,NULL,NULL,0,'active','2014-10-17 21:59:32'),
	(2,'Starters','Para un gran comienzo.','Solo basta con probarlos para que se vuelvan tus favoritos.','image_content_141360221968.jpg','starters',1,NULL,NULL,NULL,NULL,NULL,2,'active','2014-10-17 22:16:59'),
	(3,'Wine & Beer','','','','wine-beer',2,NULL,NULL,NULL,NULL,NULL,0,'active','2014-10-30 15:14:50'),
	(4,'Wine & Beer 2','Siempre cae bien una buena compañí­a.','Solo basta con probarlos para que se vuelvan tus favoritos.	','','wine-beer-2',1,NULL,NULL,NULL,NULL,NULL,1,'active','2014-11-04 11:33:49');

/*!40000 ALTER TABLE `food_category` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla food_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `food_item`;

CREATE TABLE `food_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `image_2` varchar(200) NOT NULL,
  `image_3` varchar(200) NOT NULL,
  `iframe` text NOT NULL,
  `category` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `food_item` WRITE;
/*!40000 ALTER TABLE `food_item` DISABLE KEYS */;

INSERT INTO `food_item` (`id`, `title`, `content`, `image`, `image_2`, `image_3`, `iframe`, `category`, `order`, `status`, `date_register`)
VALUES
	(1,'Vino Blanco','<br>Copa: S/.16        Botella: S/.63','image_content_141360270326.jpg','image_content_141511610484.jpg','image_content_141511610459.jpg','<iframe scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\" src=\"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.fridaysperu.com%2F&layout=button_count&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=21\"></iframe>',1,0,'active','2014-10-17 22:25:03'),
	(2,'Vino Blanco 2','<br>Copa: S/.16        Botella: S/.63','','','','<iframe scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\" src=\"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.fridaysperu.com%2F&layout=button_count&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=21\"></iframe>',1,1,'active','2014-11-04 11:41:54');

/*!40000 ALTER TABLE `food_item` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla promo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promo`;

CREATE TABLE `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_icon` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image_icon` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `disclamer` text NOT NULL,
  `convertion_google` text,
  `convertion_facebook` text,
  `tag_title` varchar(200) DEFAULT NULL,
  `tag_description` varchar(200) DEFAULT NULL,
  `tag_keywords` varchar(200) DEFAULT NULL,
  `city` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `promo` WRITE;
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;

INSERT INTO `promo` (`id`, `title_icon`, `title`, `content`, `image_icon`, `image`, `slug`, `disclamer`, `convertion_google`, `convertion_facebook`, `tag_title`, `tag_description`, `tag_keywords`, `city`, `order`, `status`, `date_register`)
VALUES
	(1,'Always Happy','DISFRUTA SIEMPRE','<p>Los tragos clásicos de nuetra carta para que celebres el doble cuando quieras.</p>\n<ul class=\"lista-descripcion\">\n 				<li>\n 					<h3>Hurricane</h3>\n 					<p>¡Ron Cartavio®, jugo de maracuyá y naranja, granadina y Sweet &amp; Sour.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Mojito</h3>\n 					<p>Ron Cartavio®, menta fresca y soda.</p>\n 				</li>\n 				<li>\n 					<h3>Cuba Libre</h3>\n 					<p>El clásico de siempre, Ron Cartavio® y Coca-Cola®.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Chilcano</h3>\n 					<p>Pisco Tabernero®, Ginger Ale Schweppes®, limón y amargo de angostura.</p>\n 				</li>\n 				<li>\n 					<h3>Screwdriver</h3>\n 					<p>Refrescante combinación de Vodka Smirnoff®y jugo de naranja.</p>\n 				</li>\n 				<li>\n 					<h3>Electric Lemonade</h3>\n 					<p>Vodka Smirnoff®, Blue Curaçao, Sweet &amp; Sour y Sprite®.</p>\n 				</li>\n 			</ul>','image_content_141361171067.png','image_content_141361144039.jpg','always-happy','Promoción individual. Solo en bebidas seleccionadas. No aplican descuentos sobre esta promoción. Ambas bebidas serán del mismo sabor. Válido en todos los locales de Fridays.','google','facebook',NULL,NULL,NULL,1,0,'active','2014-10-17 22:56:16'),
	(2,'Always Happy','DISFRUTA SIEMPRE','<p>Los tragos clásicos de nuetra carta para que celebres el doble cuando quieras.</p>\n<ul class=\"lista-descripcion\">\n 				<li>\n 					<h3>Hurricane</h3>\n 					<p>¡Ron Cartavio®, jugo de maracuyá y naranja, granadina y Sweet &amp; Sour.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Mojito</h3>\n 					<p>Ron Cartavio®, menta fresca y soda.</p>\n 				</li>\n 				<li>\n 					<h3>Cuba Libre</h3>\n 					<p>El clásico de siempre, Ron Cartavio® y Coca-Cola®.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Chilcano</h3>\n 					<p>Pisco Tabernero®, Ginger Ale Schweppes®, limón y amargo de angostura.</p>\n 				</li>\n 				<li>\n 					<h3>Screwdriver</h3>\n 					<p>Refrescante combinación de Vodka Smirnoff®y jugo de naranja.</p>\n 				</li>\n 				<li>\n 					<h3>Electric Lemonade</h3>\n 					<p>Vodka Smirnoff®, Blue Curaçao, Sweet &amp; Sour y Sprite®.</p>\n 				</li>\n 			</ul>','image_content_141410139116.png','image_content_141410139188.jpg','always-happy','Promoción individual. Solo en bebidas seleccionadas. No aplican descuentos sobre esta promoción. Ambas bebidas serán del mismo sabor. Válido en todos los locales de Fridays.',NULL,NULL,NULL,NULL,NULL,2,0,'active','2014-10-23 17:56:31');

/*!40000 ALTER TABLE `promo` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla promo_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promo_item`;

CREATE TABLE `promo_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `category` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `promo_item` WRITE;
/*!40000 ALTER TABLE `promo_item` DISABLE KEYS */;

INSERT INTO `promo_item` (`id`, `title`, `content`, `image`, `category`, `order`, `status`, `date_register`)
VALUES
	(1,'DISFRUTA SIEMPRE','<p>Los tragos clásicos de nuetra carta para que celebres el doble cuando quieras.</p>\n<ul class=\"lista-descripcion\">\n 				<li>\n 					<h3>Hurricane</h3>\n 					<p>¡Ron Cartavio®, jugo de maracuyá y naranja, granadina y Sweet &amp; Sour.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Mojito</h3>\n 					<p>Ron Cartavio®, menta fresca y soda.</p>\n 				</li>\n 				<li>\n 					<h3>Cuba Libre</h3>\n 					<p>El clásico de siempre, Ron Cartavio® y Coca-Cola®.</p>\n 				</li>\n 				<li>\n 					<h3>Signature Chilcano</h3>\n 					<p>Pisco Tabernero®, Ginger Ale Schweppes®, limón y amargo de angostura.</p>\n 				</li>\n 				<li>\n 					<h3>Screwdriver</h3>\n 					<p>Refrescante combinación de Vodka Smirnoff®y jugo de naranja.</p>\n 				</li>\n 				<li>\n 					<h3>Electric Lemonade</h3>\n 					<p>Vodka Smirnoff®, Blue Curaçao, Sweet &amp; Sour y Sprite®.</p>\n 				</li>\n 			</ul>','image_content_141360525253.jpg',1,0,'active','2014-10-17 23:07:32');

/*!40000 ALTER TABLE `promo_item` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla seo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `seo`;

CREATE TABLE `seo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla slider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `date_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;

INSERT INTO `slider` (`id`, `title`, `subtitle`, `content`, `image`, `order`, `status`, `date_register`)
VALUES
	(1,'IN HERE,','IT\'S ALWAYS FRIDAY','','image_content_141358972885.jpg',0,'active','2014-10-17 18:48:48'),
	(2,'IN HERE,','IT\'S ALWAYS FRIDAY','<p>EL MEJOR AMBIENTE</p>','image_content_141359364739.jpg',0,'active','2014-10-17 19:54:06');

/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`)
VALUES
	(1,'admin','8b971bd986c8f63d5eba66e759940dd8');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
