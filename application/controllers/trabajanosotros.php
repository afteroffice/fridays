<?php
class Trabajanosotros extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'trabaja-nosotros';
		$views['section'] = 'trabaja-nosotros';
		$this->load->view('template', $views);
	}
}
