<section>
	<div class="container bg-transp clearfix">
  	<h1 class="mapa-sitio">Mapa del sitio</h1>
    <div class="clear"></div>
    <div class="mapa">

      <h2>Generales</h2>
      <ul>
        <li><a href="/index.html" title="Ir a la página de ">Inicio</a></li>
        <li><a href="/reservas.html" title="Realiza tu reserva">Reservas</a></li>
        <li><a href="/concocenos.html" title="Conoce más sobre T.G.I. Fridays Perú">Con&oacute;cenos</a></li>
        <li><a href="/contactanos.html" title="Contácta con T.G.I. Fridays">Cont&aacute;ctanos</a></li>
        <li><a href="/suscribete.html" title="Suscribete a nuestro boletín">Suscr&iacute;bete</a></li>
      </ul>
      <ul>
        <li><a href="/certificado-regalo.html" title="Pide tu Certificado de regalo">Certificado de regalo</a></li>
        <li><a href="/eventos.html" title="Eventos en Fridays">Eventos</a></li>
        <li><a href="http://stc.laborum.pe/fridays/index.html" title="Trabaja en el T.G.I. Fridays de tu ciudad">Trabaja con nosotros</a></li>
        <li><a href="/mapa-sitio.html" title="Todas las páginas de nuestra web">Mapa del sitio</a></li>
      </ul>
		</div>
    <div class="clear"></div>
    <div class="fl-der">
    <div class="mapa">
      <h2>Lima</h2>
      <ul>
        <li><a href="/lima/index.html" title="">Principal</a></li>
        <li><a href="/lima/comidas/starters.html" title="">Comidas</a></li>
        <li><a href="/lima/bar/ultimates.html" title="">Bar</a></li>
        <li><a href="/lima/promociones.html" title="">Promociones</a></li>
      </ul>
		</div>
    <div class="mapa">
      <h2>Arequipa</h2>
      <ul>
        <li><a href="/arequipa/index.html" title="">Principal</a></li>
        <li><a href="/arequipa/comidas/starters.html" title="">Comidas</a></li>
        <li><a href="/arequipa/bar/ultimates.html" title="">Bar</a></li>
        <li><a href="/arequipa/promociones.html" title="">Promociones</a></li>
      </ul>
		</div>
    <div class="mapa last">
      <h2>Trujillo</h2>
      <ul>
        <li><a href="/trujillo/index.html" title="">Principal</a></li>
        <li><a href="/trujillo/comidas/starters.html" title="">Comidas</a></li>
        <li><a href="/trujillo/bar/ultimates.html" title="">Bar</a></li>
        <li><a href="/trujillo/promociones.html" title="">Promociones</a></li>
      </ul>
    </div>
    </div>
  </div>
</section>
