<?php
class Dashboard extends App_Controller {
	
	public function index()
	{
		$this->load->model('seo_model');
		if ($this->input->post() && $this->_validate_submit()) {
			$keys = $this->seo_model->getAll(array('key' => 'analytics'));
			foreach ($keys as $k) {
				$this->seo_model->delete($k->id);
			}
			$this->seo_model->insert(array('key' => 'analytics', 'value' => $this->input->post('analytics')));
		}
		$analytics = $this->seo_model->getAll(array('key' => 'analytics'));
		$views['analytics'] = $analytics;
		$views['content_view'] = 'admin/dashboard';
		$this->load->view('admin/template', $views);
	}

	private function _validate_submit()
	{
		$this->form_validation->set_rules('analytics', 'Google Analytics','trim|required');
		return $this->form_validation->run();
	}
}
