<?php
class Reservas extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		//$this->load->config('mandrill');
		$this->load->config('admin_email');
		$this->load->library('email');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		if ($this->input->post()) {
			if ($this->input->post('r-nombre') != '' && $this->input->post('r-correo') != '') {
				$this->load->model('reservation_model');
				$token = bin2hex(openssl_random_pseudo_bytes(32));
				$data['nombres'] = $this->input->post('r-nombre');
				$data['telefono'] = $this->input->post('r-telefono');
				$data['email'] = $this->input->post('r-correo');
				$data['local'] = $this->input->post('r-local');
				$data['fecha'] = $this->input->post('r-fecha'); //$this->input->post('r-dia').'/'.$this->input->post('r-mes');
				$data['hora'] = $this->input->post('r-hora-h').':'.$this->input->post('r-hora-m');
				$data['num_person'] = $this->input->post('r-personas');
				$data['motivo'] = $this->input->post('r-motivo');
				$data['token'] = $token;
				$this->reservation_model->insert(
					array(
						'name' => $this->input->post('r-nombre'),
						'telephone' => $this->input->post('r-telefono'),
						'email' => $this->input->post('r-correo'),
						'place' => $this->input->post('r-local'),
						'date' => $this->input->post('r-fecha'), //$this->input->post('r-dia').'/'.$this->input->post('r-mes'),
						'hour' => $this->input->post('r-hora-h').':'.$this->input->post('r-hora-m'),
						'num_person' => $this->input->post('r-personas'),
						'reason' => $this->input->post('r-motivo'),
						'promo_receive' => $this->input->post('f-recibir'),
						'token' => $token,
						'date_register' => date('Y-m-d H:i:s')
					)
				);
				$this->email->initialize(array('charset' => 'utf-8', 'mailtype' => 'html'));
				$msg = $this->load->view('mailing_reserva', $data, true);

				// Mail para cliente
				$this->email->from('noreply@fridaysperu.com', 'Fridays Peru');
				$this->email->to($this->input->post('r-correo')); 

				$this->email->subject(($this->input->post('r-personas') <= 15) ? 'Reserva Fridays' : 'Solicitud de Reserva');
				$this->email->message($msg);	

				$this->email->send();
				/*$mandrill_ready = NULL;
				try {
					$this->mandrill->init( $this->config->item('mandrill_api_key') );
					$mandrill_ready = TRUE;
				} catch(Mandrill_Exception $e) {
					$mandrill_ready = FALSE;
					var_dump($e); exit;
				}
				if ($mandrill_ready) {
					// Mail para cliente
					$msg = $this->load->view('mailing_reserva', $data, true);
					$email = array(
						'html' => $msg, 
						'subject' => ($this->input->post('r-personas') <= 15) ? 'Reserva Fridays' : 'Solicitud de Reserva',
						'from_email' => 'noreply@fridaysperu.com',
						'from_name' => 'Fridays Peru',
						'to' => array(array('email' => $this->input->post('r-correo'))),
					);
					$result = $this->mandrill->messages_send($email);
					
					// Mail para operario
					$msg = $this->load->view('mailing_operario_reserva', $data, true);
					foreach ($this->config->item('operario_email') as $ae) {
						$to[] = array('email' => $ae);
					}
					$email = array(
						'html' => $msg, 
						'subject' => 'Reserva Fridays', 
						'from_email' => 'noreply@fridaysperu.com',
						'from_name' => 'Fridays Peru',
						'to' => $to 
					);
					$result = $this->mandrill->messages_send($email);
				}*/
				redirect('/reservas/success');
			}
		}
		$views['cities'] = $cities;
		$views['content_view'] = 'reservas';
		$views['section'] = 'reservas';
		$this->load->view('template', $views);
	}
	
	public function success()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'reservas_success';
		$views['section'] = 'reservas';
		$this->load->view('template', $views);
	}
	
	public function terminoscondiciones()
	{
		$this->load->view('terminos-condiciones');
	}
	
	public function cancel($token)
	{
		$this->load->model('city_model');
		$this->load->model('reservation_model');
		$dataReservation = $this->reservation_model->getDataByToken($token);
		if ($dataReservation == false) {
		}
		$data['dataReservation'] = $dataReservation;
		$this->reservation_model->update(
			array(
				'status_reservation' => 'cancel',
				'token' => ''
			),
			$dataReservation->id
		);
		
		$data['nombres'] = $dataReservation->name; 
		$data['telefono'] = $dataReservation->telephone; 
		$data['email'] = $dataReservation->email; 
		$data['local'] = $dataReservation->place; 
		$data['fecha'] = $dataReservation->date; 
		$data['hora'] = $dataReservation->hour; 
		$data['num_person'] = $dataReservation->num_person; 
		$data['motivo'] = $dataReservation->reason; 
		
		$this->load->config('admin_email');
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;
		try {
			$this->mandrill->init( $this->config->item('mandrill_api_key') );
			$mandrill_ready = TRUE;
		} catch(Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
			var_dump($e); exit;
		}
		if ($mandrill_ready) {
			/* Mail para cliente */
			$msg = $this->load->view('admin/mailing_reservation_cancel', $data, true);
			$email = array(
				'html' => $msg, 
				'subject' => 'Cancelación de Reserva',
				'from_email' => 'noreply@fridaysperu.com',
				'from_name' => 'Fridays Peru',
				'to' => array(array('email' => $data['dataReservation']->email)),
			);
			$result = $this->mandrill->messages_send($email);
			
			/* Mail para operario */
			$msg = $this->load->view('mailing_operario_reserva_cancel', $data, true);
			foreach ($this->config->item('operario_email') as $ae) {
				$to[] = array('email' => $ae);
			}
			$email = array(
				'html' => $msg, 
				'subject' => 'Cancelación reserva Fridays', 
				'from_email' => 'noreply@fridaysperu.com',
				'from_name' => 'Fridays Peru',
				'to' => $to,
			);
			$result = $this->mandrill->messages_send($email);
		}
		
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'reservas_cancel';
		$views['section'] = 'reservas';
		$this->load->view('template', $views);
	}
}
