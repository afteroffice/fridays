<?php
class City_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "city";
	}
	
	public function getCityBySlug($slug)
	{
		$this->db->select();
		$this->db->where('slug', $slug);
		$this->db->where('status', 'active');
		$query = $this->db->get($this->_table);
		$result = $query->result();
		return $result[0];
	}
	
	public function getCategoriesFoods($city)
	{
		$this->db->select();
		$this->db->where('city', $city);
		$this->db->where('status', 'active');
		$this->db->order_by("order", "asc");
		$query = $this->db->get('food_category');
		return $query->result();
	}
	
	public function getCategoriesBars($city)
	{
		$this->db->select();
		$this->db->where('city', $city);
		$this->db->where('status', 'active');
		$this->db->order_by("order", "asc");
		$query = $this->db->get('bar_category');
		return $query->result();
	}
}
