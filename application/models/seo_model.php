<?php
class Seo_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "seo";
	}

	public function getAnalytics()
	{
		$this->db->select();
		$this->db->where('key', 'analytics');
		$query = $this->db->get($this->_table);
		$result = $query->result();
		if (isset($result[0])) {
			return $result[0]->value;
		}
		return '';
	}
}