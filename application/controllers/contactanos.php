<?php
class Contactanos extends CI_Controller {
	
	public function index()
	{
		$this->load->model('city_model');
		$this->load->model('contact_model');
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		if ($this->input->post()) {
			$this->contact_model->insert(
				array(
					'name' => $this->input->post('strNombres'),
					'lastname' => $this->input->post('strApellidos'),
					'email' => $this->input->post('strEmail'),
					'phone' => $this->input->post('strPhone'),
					'type' => $this->input->post('selTipo'),
					'comment' => $this->input->post('txtComentarios'),
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			$mandrill_ready = NULL;
			try {
				$this->mandrill->init( $this->config->item('mandrill_api_key') );
				$mandrill_ready = TRUE;
			} catch(Mandrill_Exception $e) {
				$mandrill_ready = FALSE;
				var_dump($e); exit;
			}
			if ($mandrill_ready) {
				
				$msg = "";
				$msg .= "<div style='text-align:justify; width:500; padding:20px 90px 50px 70px; color:black'>";
				$msg .= "Su mensaje ha sido recibido. En breve nos estaremos comunicando con usted.<br/>";
				$msg .= "Muchas gracias.<br/><br/>";
				$msg .= "<b>T.G.I Friday's Perú</b><br/><br/>";
				$msg .= "<small>Garantía de confidencialidad <br/>";
				$msg .= "Friday's valora y respeta tu privacidad, por eso te garantizamos que cualquier información ";
				$msg .= "que nos proporciones será estrictamente confidencial. Es nuestra polí­tica no vender, ";
				$msg .= "alquilar, distribuir o proporcionar cualquier información personal a terceros.</small></div>";
				$email = array(
					'html' => $msg, 
					'subject' => 'Mensaje de Contacto',
					'from_email' => 'noreply@fridaysperu.com',
					'from_name' => 'Fridays Peru',
					'to' => array(array('email' => $this->input->post('strEmail'))) 
				);
				$result = $this->mandrill->messages_send($email);
				
				$msg = '';
				$msg .= '<p>Nombre: '.$this->input->post('strNombres').'</p>';
				$msg .= '<p>Apellido: '.$this->input->post('strApellidos').'</p>';
				$msg .= '<p>Correo: '.$this->input->post('strEmail').'</p>';
				$msg .= '<p>Telefono: '.$this->input->post('strPhone').'</p>';
				$msg .= '<p>Tipo de Comentario: '.$this->input->post('selTipo').'</p>';
				$msg .= '<p>Mensaje: '.$this->input->post('txtComentarios').'</p>';
				$email = array(
					'html' => $msg, 
					'subject' => 'Contacto Fridays',
					'from_email' => 'noreply@fridaysperu.com',
					'from_name' => 'Contacto Fridays',
					'to' => array(array('email' => 'marketing@fridaysperu.com'))
				);
				$result = $this->mandrill->messages_send($email);
				redirect('/contactanos/gracias');
			}
		}
		$views['content_view'] = 'contactanos';
		$views['section'] = 'contactanos';
		$this->load->view('template', $views);
	}
	
	public function gracias()
	{
		$this->load->model('city_model');
		$cities = $this->city_model->getAll(array('status' => 'active'));
		$views['cities'] = $cities;
		$views['content_view'] = 'contactanos_gracias';
		$views['section'] = 'contactanos';
		$this->load->view('template', $views);
	}
}
