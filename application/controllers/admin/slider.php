<?php
class Slider extends App_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('slider_model');
		$this->load->library('upload');
	}
	
	public function index()
	{
		$contents = $this->slider_model->getAll(array('status' => 'active'));
		$views['contents'] = $contents;
		$views['content_view'] = 'admin/slider_list';
		$this->load->view('admin/template', $views);
	}
	
	public function add()
	{
		if ($this->input->post() && $this->_validate_submit()) {
			$idSlider = $this->slider_model->insert(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
					'date_register' => date('Y-m-d H:i:s')
				)
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->slider_model->update(
						array(
							'image' => $newName
						),
						$idSlider
					);
				}
			}
			redirect('/admin/slider');
		}
		$views['content_view'] = 'admin/slider_add';
		$this->load->view('admin/template', $views);
	}
	
	public function edit($id)
	{
		if ($this->input->post() && $this->_validate_submit()) {
			$this->slider_model->update(
				array(
					'title' => $this->input->post('title'),
					'subtitle' => $this->input->post('subtitle'),
					'content' => $this->input->post('content'),
				),
				$id
			);
			if ($_FILES['image']['name'] != '') {
				$config['upload_path'] = './uploads/images';
				$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
				$config['max_size'] = '10000KB';
				$this->upload->initialize($config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$newName = 'image_content_'.time().rand(1,99).$data['file_ext'];
					rename($data['full_path'], $data['file_path'].$newName);
					$this->slider_model->update(
						array(
							'image' => $newName
						),
						$id
					);
				}
			}
			redirect('/admin/slider');
		}
		$dataSlider = $this->slider_model->find($id);
		$views['content'] = $dataSlider;
		$views['content_view'] = 'admin/slider_add';
		$this->load->view('admin/template', $views);
	}
	
	public function delete($id)
	{
		$this->slider_model->update(
			array(
				'status' => 'delete',
			),
			$id
		);
		redirect('/admin/slider');
	}
	
	private function _validate_submit()
	{
		$this->form_validation->set_rules('title', 'Titulo','trim|required');
		return $this->form_validation->run();
	}
}
