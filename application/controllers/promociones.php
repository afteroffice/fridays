<?php
class Promociones extends CI_Controller {
	
	public function index($slug)
	{
		$this->load->model('slider_model');
		$this->load->model('city_model');
		$this->load->model('promo_model');
		$city = $this->city_model->getCityBySlug($slug);
		$data = $this->promo_model->getAll(array('status' => 'active', 'city' => $city->id), true);
		$views['data'] = $data;
		$views['city'] = $city;
		$views['c_food'] = $this->city_model->getCategoriesFoods($city->id);
		$views['c_bar'] = $this->city_model->getCategoriesBars($city->id);
		$views['content_view'] = 'promociones';
		$views['section'] = 'promociones';
		$this->load->view('template', $views);
	}
	
	public function detail($slugCity, $slug)
	{
		$this->load->model('slider_model');
		$this->load->model('city_model');
		$this->load->model('promo_model');
		$city = $this->city_model->getCityBySlug($slugCity);
		$promoData = $this->promo_model->getDataBySlug($slug, $city->id);
		if (!$promoData) {
			redirect('/');
		}
		$views['data'] = $promoData;
		$views['city'] = $city;
		$views['c_food'] = $this->city_model->getCategoriesFoods($city->id);
		$views['c_bar'] = $this->city_model->getCategoriesBars($city->id);
		$views['content_view'] = 'promociones_detalle';
		$views['section'] = 'detalle-promocion';
		$this->load->view('template', $views);
	}
}
