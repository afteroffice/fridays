<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('field')) {
    function field($validation, $database = NULL, $last = '') {
        $value = (isset($validation) && trim($validation) != "") ? $validation : ( (isset($database)) ? $database : $last);
        return $value;
    }
}


?>
