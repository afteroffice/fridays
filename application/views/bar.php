<div class="container">
	<div class="menu-bar">
		<ul>
			<?php foreach ($categories as $c) : ?>
			<li><a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/bar/<?=$c->slug ?>" title="" class="<?=($c->id == $category->id) ? 'activo' : '' ?>" ><?=$c->title ?></a></li>
			<?php endforeach; ?>
		</ul>
		<select onChange="location = this.options[this.selectedIndex].value;">
			<option value="#" selected>MENU BAR...</option>
			<?php foreach ($categories as $c) : ?>
			<option value="<?=$c->slug ?>">- <?=$c->title ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="bar-tit" style="background:url(<?=base_url() ?>uploads/images/<?=$category->image ?>);" >
		<h1><?=$category->title ?></h1>
		<h2><?=$category->subtitle ?></h2>
	</div>
	<div class="bebidas">
		<ul class="beb1 clearfix">
			<?php foreach ($items as $i) : ?>
			<li>
				<?php if (isset($i->image) && $i->image != "") : ?>
				<div class="me-gusta3">
					<?=$i->iframe ?>
				</div>
				<?php endif; ?>
				<?php if ($i->image != "") : ?>
				<div class="img-comidas">
					<img src="<?=base_url() ?>uploads/images/<?=$i->image ?>" alt="<?=$i->title ?>" class="img-beb1">
					<img src="<?=base_url() ?>uploads/images/<?=$i->image_2 ?>" alt="<?=$i->title ?>" class="img-beb2">
				</div>
				<?php endif; ?>
				<div class="info-bebidas <?=($i->image == "") ? 'sin-img' : ''  ?>">
					<h2><?=$i->title ?></h2>
					<p><?=$i->content ?></p>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
