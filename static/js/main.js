$(document).ready( function(){
	$.formUtils.addValidator({
		name : 'date_reserva',
		validatorFunction : function(value, $el, config, language, $form) {
			if ($('#r-dia').val() > 31) {
				return false;
			}
			var d = new Date();
			var month = $('#r-mes').val();
			month--;
			var hour = parseInt($('#r-hora-h').val()) + 12;
			evalDate = new Date(d.getFullYear(), month, $('#r-dia').val(), hour, $('#r-hora-m').val());
			if (d.getTime() >= evalDate.getTime()) {
				return false
			}
			return true
		
		},
		errorMessage : 'You have to answer with an even number',
		errorMessageKey: 'badEvenNumber'
	});

	$.validate({
		onError : function($form) {
			$('#msg-error').show();
		},
		modules : 'date'
	});
	$(".onlyNumber").keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			(e.keyCode == 65 && e.ctrlKey === true) || 
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				 return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	$(".onlyLetter").keypress(function(event){
		var inputValue = event.which;
		var allowedChars = new RegExp("^[a-zA-Z \-]+$");
		var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (allowedChars.test(str)) {
			return true;
		}
		event 	.preventDefault();
		return false;
	});
	
	/*$('#r-enviar').click(function() {
		return false;
	});*/
	$('#freservas').submit(function(e) {
		var d = new Date();
		var month = $('#r-mes').val();
		month--;
		var hour = parseInt($('#r-hora-h').val()) + 14;
		evalDate = new Date(d.getFullYear(), month, $('#r-dia').val(), hour, $('#r-hora-m').val());
		if (d.getTime() >= evalDate.getTime()) {
			alert('Fecha de reserva invalida');
			e.preventDefault();
		}
		if (!$('#f-tyc').is(':checked')) {
			alert('Debe aceptar los terminos y condiciones')
			e.preventDefault();
		}
	});
	
	$('#r-dia').keyup(function() {
		if ($(this).val().length == 2) {
			$('#r-mes').focus();
		}
	});
	
	params = {
		minDate: "+0D", 
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'], 
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dec'], 
		dateFormat: 'dd-mm-yy', 
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		onSelect: function(select, e) {
			var value = 12;
			$('#r-hora-h option').show();
			var d = new Date();
			var yyyy = d.getFullYear().toString();
			var mm = (d.getMonth()+1).toString();
			var dd  = d.getDate().toString();
			date_string = (dd[1]?dd:"0"+dd[0]) + '-' +  (mm[1]?mm:"0"+mm[0]) + '-' + yyyy;
			if (select == date_string) {
				hour_current = d.getHours();
				hour_current++;
				$('#r-hora-h option').each(function(index) {
					if ($(this).attr('rel') <= hour_current) {
						$(this).hide();
						var s = index + 1;
						value = $('#r-hora-h option:eq(' + s + ')').val()
					}
				});
			}
			$('#r-hora-h').val(value);
		}
	}
	var now = new Date();
	if (now.getHours() > 20) {
		params.minDate = "+1D";
	}
	if (now.getDay() == 7) {
		params.beforeShowDay = function(date) {
			var day = date.getDate();
			return [day != now.getDate(),''];
		}
	}
	$(".datepicker").datepicker(params);
	
	$('#r-hora-h').change(function() {
		$("#r-hora-m > option").each(function() {
			$(this).show();
		});
		if ($(this).val() == '11') {
			$("#r-hora-m > option").each(function() {
				if (this.value != '00') {
					$(this).hide();
				}
			});
		}
	});
	
	$('#fake-FECNAC').change(function() {
		var date = $('#fake-FECNAC').val().split("/");
		$('#mce-FECNAC').val(date[1] + '/' + date[0] + '/' + date[2]);
	});
	$('.date-input').change(function() {
		$('#FECNAC-fake-date').val($('#mce-FECNAC-month').val() + '/' + $('#mce-FECNAC-day').val() + '/' + $('#mce-FECNAC-year').val());
	});
});
