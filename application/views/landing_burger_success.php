<!DOCTYPE html>
<html>
	<head>
		<title>Fridays - Burgers & Shakes</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/reset.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/estilos.css?v=20">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/desktop.css?v=13">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/tablet.css?v=13">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/smartphone.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url() ?>/static/landing-burguer/css/colorbox.css">
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script type="text/javascript" src="js/respond.src.js"></script>
		<![endif]-->
		<?php 
			$ci =&get_instance();
			$ci->load->model('seo_model'); 
		?>
		<?=$ci->seo_model->getAnalytics() ?>
		<?php if ($placeUrl == 'lima') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Lima -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022703925552', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022703925552&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php elseif ($placeUrl == 'arequipa') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Arequipa -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022704127552', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022704127552&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php elseif ($placeUrl == 'trujillo') : ?>
		<!-- Facebook Conversion Code for FRIDAYS - Burgers &amp; Shakes - Trujillo -->
		<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6022704031152', {'value':'0.00','currency':'USD'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022704031152&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
		<?php endif; ?>
	</head>

<body>
    <div class="gracias">
        <img src="<?=base_url() ?>/static/landing-burguer/img/logo-fridays.jpg" alt="Logo Fridays" class="logo">

        <h2>GRACIAS POR TU PARTICIPACIÓN</h2>

        <p>Revisa tu correo electrónico, imprime el cupón y disfruta de las Burgers & Shakes.</p>

        <div class="descubre">
            <p>DESCUBRE MÁS PROMOS</p><a href="<?=base_url() ?>ciudad/<?=$place?>/promociones" class="btn-descubre">aquí</a>
        </div>
    </div>

<!--*******-->
<!--SCRIPTS-->
<!--*******-->
<script type="text/javascript" src="<?=base_url() ?>/static/landing-burguer/js/modernizr.custom.13945"></script>
</body>
</html>
