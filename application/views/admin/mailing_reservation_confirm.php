<!DOCtYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Fridays</title>

<style type="text/css">
body{
	background: #ffffff; margin: 0; padding: 0; min-width: 100%!important;
}
td, p{
	margin: 0;
}
.content{
	width: 100%; max-width: 600px;
} 
</style>
</head>

<body>
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
	<table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table class="content" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0 auto; font-family: Helvetica, Arial, sans-serif; background:#FFFFFF;">
					<tr>
						<th>
							<img src="<?=base_url()?>static/images/mailing/img/cabecera-confirmacion.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
						</th>
					</tr>

					<tr style="display:block; padding:20px 0 20px 0; ">
						<td>
							<p style="font-weight:bold; color:#000000;"><span><?=$dataReservation->name ?>,</span></p>
							<p style="font-weight:bold; color:#000000;"><span>Tu reserva ha sido confirmada</span></p>
							<p style="font-weight:bold; color:#000000;"><span>Te esperamos para que disfrutes una gran experiencia</span></p>
							<p style="font-weight:bold; color:#000000;"><span>&nbsp;</span></p>
							<p style="font-weight:bold; color:#000000;"><span>Estos son los datos de la confirmación de tu reserva</span></p>
						</td>
					</tr>

					<tr style="display:block; padding:0 0 20px 0; ">
						<td>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Nombres y Apellidos:</span> <?=$dataReservation->name ?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Local:</span> <?=$dataReservation->place ?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Fecha:</span> <?=$dataReservation->date ?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Hora:</span> <?=$dataReservation->hour ?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Nª de personas:</span> <?=$dataReservation->num_person ?></p>
							<p style="font-size:16px; color:#505050; margin:0 0 5px 0;"><span style="font-weight:bold; color:#000000;">Motivo de la reserva:</span> <?=$dataReservation->reason ?></p>
						</td>
					</tr>


					<tr>
						<td>
							<img src="<?=base_url()?>static/images/mailing/img/pie-confirmacion-reserva.jpg" alt="" style="width:100%; display:block; margin:0; border:0;">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</body>
</html>
