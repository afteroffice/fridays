<?php
class Landingburger_model extends App_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->_table = "landing_burger";
	}

	public function findByField($field, $value)
	{
		$this->db->select();
		$this->db->where($field, $value);
		$query = $this->db->get($this->_table);
		$result = $query->result();
		if (isset($result[0])) {
			return $result[0];
		}
		return false;
	}
}