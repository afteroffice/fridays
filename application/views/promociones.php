<h2>Elige tu promo favorita</h2>
<ul class="lista-promociones">
	<?php foreach ($data as $d) : ?>
	<li>
		<a href="<?=base_url() ?>ciudad/<?=$city->slug ?>/promociones/<?=$d->slug ?>">
			<img src="<?=base_url() ?>uploads/images/<?=$d->image_icon ?>" alt="<?=$d->title_icon ?>" height="60px">
			<h3><?=$d->title_icon ?></h3>
		</a>
	</li>
	<?php endforeach; ?>
</ul>
